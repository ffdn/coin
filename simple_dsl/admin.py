# -*- coding: utf-8 -*-

from django.contrib import admin

from coin.configuration.admin import ChildConfigurationAdmin
from simple_dsl.models import SimpleDSL


class SimpleDSLInline(admin.StackedInline):
    model = SimpleDSL


class SimpleDSLAdmin(ChildConfigurationAdmin):
    base_model = SimpleDSL
    # Used for form inclusion (when browsing a subscription object in the
    # admin, SimpleDSLInline will be displayed)
    inline = SimpleDSLInline
    # Since we don't provide a view, don't display a "view on site" link
    # in the admin.
    view_on_site = False

admin.site.register(SimpleDSL, SimpleDSLAdmin)
