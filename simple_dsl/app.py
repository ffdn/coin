# -*- coding: utf-8 -*-

from django.apps import AppConfig


class SimpleDSLConfig(AppConfig):
    name = 'simple_dsl'
    verbose_name = 'xDSl'
