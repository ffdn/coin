Permissions (sur l'interface d'administration)
==============================================

Par défaut, un membre n'a pas accès à l'interface d'administration.

Organisation
------------

Les permissions d'un membre se changent dans sa fiche. Seuls les
super-utilisateurs peuvent modifier les permissions.

### Statut équipe

Il permet d'autoriser un membre à se connecter à l'interface
d'administration. Un bouton *« Administration »* apparaîtra alors dans son
menu. En l'absence d'appartenance à un [groupe](#groupes) ou
du [statut super-utilisateur](#statut-super-utilisateur), le statut équipe
donne accès à une interface d'administration vide.

### Statut super-utilisateur

Un membre avec le *statut super-utilisateur* peut lire et modifier toutes les
informations gérées par coin. C'est typiquement un statut à réserver aux
membres du bureau.

### Groupes

Les *groupes* permettent simplement de réunir les membres par niveau
d'accès. Un *groupe* inclut donc un ou plusieurs *membres* et se voit attribuer
une ou plusieurs [permissions](#permissions).

Un membre peut appartenir à plusieurs groupes.

### Permissions

Les permissions permettent de choisir précisément à quelles données peuvent
accéder les membres d'un [groupe](#groupe).

#### Permissions par opération

On peut gérer les permissions d'accès pour chaque opération réalisable dans
coin. Une opération est la combinaison d'un *type d'opération* et d'un *type de
donnée*.

- Les **types d'opérations** sont : *création*, *suppression* *modification*.
- Les **types de données** principaux sont : membre, abonnement, offre… La
liste complète est affichée aux super-utilisateurs sur la page d'accueil de
l'administration.


**NB**: Le droit de *lecture* est accordé avec le droit de *modification*. Le droit
de *lecture seule* n'existe donc pas.

Les permissions sur les *abonnements*, les *offres* et les *membres* sont de plus
restreintes par les [permissions fines](#permissions-fines-par-offre).

#### Permissions fines (par offre)

Ce sont des permissions qui permettent de n'autoriser l'accès qu'à une partie
des données en fonction de leur contenu. Ces permissions ne se substituent pas
aux [permissions par opération](#permissions-par-operation), elles en limitent
le champ d'application.

Les *types de données* dont l'accès est limité par les *permissions fines* sont :

- offres
- abonnements
- membre

Les *permissions fines* permettent ce genre de logique :

- Les membres du groupe « Admins VPN » n'ont accès qu'à ce qui concerne les
  abonnés et abonnements VPN.
- Les membres du groupe « Wifi Machin » n'ont accès qu'à ce qui concerne les
  abonnements wifi du quartier machin
- etc…

Le critère sur lequel une donnée est accessible ou non est donc l'offre
souscrite.


Exemples
--------

## Exemple pour un groupe gérant le matériel et les emprunts

1. Créer un **groupe** « Matos » (dans la section *Auth*) avec toutes les
   permissions mentionnant l'application « hardware_provisioning ».
2. Pour chaque *membre* qui va gérer le matos, aller sur sa fiche, et dans la
   rubrique *Permissions* :

  - activer son *Statut équipe*
  - l'ajouter au groupe  « Matos »

**NB:** Quand un membre de notre groupe « Matos » déclare un nouvel emprunt, il
devra tapper au moins 3 caractères du nom du membre qui emprunte, de cette façon
un utilisateur qui n'est pas super-utilisateur n'a pas accès facilement à la
liste de tous les membres.



## Exemple pour un groupe gérant les abonnements ADSL

1. **Pour chaque offre ADSL, créer une Row Level Permission** (dans la section
   *Members*) correspondante (c'est pénible mais on est obligé de faire une
   permission par *offre*). Par exemple, si on a deux offres ADSL :

   | Champ          | Valeur                            	|
   |----------------|---------------------------------------|
   | Nom          	| Permission ADSL Marque blanche    	|
   | Content Type 	| abonnement                        	|
   | Nom de code  	| perm-adsl-marque-blanche          	|
   | Offre        	| Marque blanche FDN - 32 € / mois  	|

 et

   | Champ          | Valeur                            	            |
   |----------------|---------------------------------------------------|
   | Nom          	| Permission ADSL Marque blanche (préférentiel)    	|
   | Content Type 	| abonnement                        	            |
   | Nom de code  	| perm-adsl-marque-blanche-pref        	            |
   | Offre        	| Marque blanche FDN - 32 € / mois  	            |

2. **Créer un Groupe** (dans la section *Auth*) nommé « ADSL » avec les
   permissions suivantes :
  - `membres | membre | Can add membre` pour que les *membres* du groupe
    puissent créer de nouvelles fiches membre
  - `membres | membre | Can change membre` pour qu'ils puissent voir et éditer
    les infos des membres, ils n'auront accès qu'aux membres qui ont souscrit à
    un abonnement ADSL
  - `offers | abonnement | Can add abonnement` pour qu'ils puissent une
    souscription d'abonnement
  - `offers | abonnement | Can change abonnement` pour qu'ils puissent modifier
    une souscription abonnement
  - `offers | abonnement | Can delete abonnement` si l'on veut qu'ils puissent
    supprimer des abonnements (à réfléchir, peut être souhaitable ou non)
  - `offers | abonnement | perm-adsl-marque-blanche` pour qu'ils puissent avoir
    accès aux membres qui ont souscrit à l'offre correspondante (permission
    qu'on vient de créer au 1.)
  - `offers | abonnement | perm-adsl-marque-blanche-pref` (idem)

3. **Pour chaque membre** qui va gérer l'ADSL, aller sur sa fiche et dans la
   rubrique *Permissions* :
  - lui ajouter le *Statut équipe* (afin qu'il puisse se connecter à l'interface d'admin)
  - l'ajouter au groupe « ADSL »

Les membres du groupe peuvent maintenant ajouter / modifier des membres et
des abonnements.

**Attention :** pour respecter la vie privée, les membres du groupe n'ont accès
qu'aux membres qui ont un abonnement ADSL. Donc s'ils veulent enregistrer un
nouveau membre il faut renseigner son abonnement *au moment de la création de
la fiche membre* (en bas du formulaire membre) ; sinon la fiche du nouveau
membre va être créée mais sera invisible (erreur 404, sauf pour le bureau).
