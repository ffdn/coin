# -*- coding: utf-8 -*-

from django.contrib import admin

from coin.reverse_dns.models import NameServer, ReverseDNSEntry

admin.site.register(NameServer,)
admin.site.register(ReverseDNSEntry,)
