# -*- coding: utf-8 -*-

from django.apps import AppConfig


class ReverseDNSConfig(AppConfig):
    name = 'coin.reverse_dns'
    verbose_name = "Reverse DNS"

    admin_menu_addons = {
        'infra': [
            ("Serveur de nom", "reverse_dns/nameserver"),
            ("Reverse DNS", "reverse_dns/reversednsentry"),
        ]
    }
