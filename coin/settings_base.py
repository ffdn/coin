# -*- coding: utf-8 -*-

import os
#import ldap

# Django settings for coin project.

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
DEBUG = TEMPLATE_DEBUG = True

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# I (Aleks) don't understand why Django is complaining ...
# https://stackoverflow.com/questions/58502493/name-attributes-of-actions-defined-in-class-astromatchapp-report-admin-use/65578574#65578574
# (can maybe removed once we're past Django 2.2 idk ?)
SILENCED_SYSTEM_CHECKS = ['admin.E130']

# Email on which to send emails
SUBSCRIPTIONS_NOTIFICATION_EMAILS = []
REQUESTS_NOTIFICATION_EMAILS = []

MANAGERS = ADMINS

DATABASES = {
    # Database hosted on vagant test box
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'coin',
        'USER': 'coin',
        'PASSWORD': 'coin',
        'HOST': 'localhost',  # Empty for localhost through domain sockets
        'PORT': '15432',  # Empty for default
    },
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.7/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

# Country code for phone number format
PHONENUMBER_DB_FORMAT = "fr"

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Default URL for login and logout
LOGIN_URL = '/members/login'
LOGIN_REDIRECT_URL = '/members'
LOGOUT_URL = '/members/logout'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Location of private files. (Like invoices)
# In production, this location should not be publicly accessible through
# the web server
PRIVATE_FILES_ROOT = SENDFILE_ROOT = os.path.join(BASE_DIR, 'smedia/')

# Backend to use when sending private files to client
# In production, must be sendfile.backends.xsendfile with Apache xsend file mod
# Or failing xsendfile, use : sendfile.backends.simple
# https://github.com/johnsensible/django-sendfile
SENDFILE_BACKEND = 'sendfile.backends.development'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '!qy_)gao6q)57#mz1s-d$5^+dp1nt=lk1d19&9bb3co37vn)!3'

MIDDLEWARE = (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# Do not load jQuery from crappy CDN
DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': '/static/js/vendor/jquery.js'
}

# Used for debug toolbar
INTERNAL_IPS = ['127.0.0.1', '::1']

ROOT_URLCONF = 'coin.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'coin.wsgi.application'

TEMPLATE_DIRS = (
    # Only absolute paths, always forward slashes
    os.path.join(PROJECT_PATH, 'templates/'),
)

EXTRA_TEMPLATE_DIRS = tuple()

INSTALLED_APPS = (
    'debug_toolbar',  # always installed, but enabled only if DEBUG=True
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    #'ldapdb',  # LDAP as database backend
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'netfields',
    # Uncomment the next line to enable admin documentation:
    #'django.contrib.admindocs',
    'polymorphic',
    #'activelink',          # Detect if a link match actual page
    'compat',
    'hijack',
    'django_extensions',
    'crispy_forms',
    'coin',
    'coin.members',
    'coin.offers',
    'coin.billing',
    'coin.resources',
    'coin.reverse_dns',
    'coin.configuration',
    'coin.isp_database',
)

CRISPY_TEMPLATE_PACK = 'bootstrap4'

EXTRA_INSTALLED_APPS = tuple()

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        "coin.billing": {
            'handlers': ['console'],
            'level': 'INFO',
        },
        "coin.subnets": {
            'handlers': ['console'],
            'level': 'INFO',
        }
    }
}

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.template.context_processors.debug",
    "django.template.context_processors.i18n",
    "django.template.context_processors.media",
    "django.template.context_processors.static",
    "django.template.context_processors.tz",
    "django.template.context_processors.request",
    "coin.isp_database.context_processors.branding",
    "coin.context_processors.installed_apps",
    "django.contrib.messages.context_processors.messages")

AUTH_USER_MODEL = 'members.Member'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# Where admins are redirected to after hijacking a user
HIJACK_LOGIN_REDIRECT_URL = '/'

# Where admins are redirected to after releasing a user
HIJACK_LOGOUT_REDIRECT_URL = '/admin/members/member/'

# Needed for link in admin
HIJACK_ALLOW_GET_REQUESTS = True

GRAPHITE_SERVER = "http://localhost"

# Configuration for outgoing emails
#DEFAULT_FROM_EMAIL = "coin@example.com"
#EMAIL_USE_TLS = True
#EMAIL_HOST = "smtp.chezmoi.tld"

# Do we use LDAP or not
LDAP_ACTIVATE = False

# Not setting them results in NameError
LDAP_USER_BASE_DN = None
VPN_CONF_BASE_DN = None
DSL_CONF_BASE_DN = None

# Allow member to edit their vpn
MEMBER_CAN_EDIT_VPN_CONF = True

# Membership configuration
# Default cotisation in €, per year
DEFAULT_MEMBERSHIP_FEE = 20

# Link to a page with information on how to become a member or pay the
# membership fee
MEMBER_MEMBERSHIP_INFO_URL = ''

# Statutes / Terms validation
#MEMBER_TERMS = 'J\'ai lu les <a href="https://wiki.arn-fai.net/administratif:statuts" target="_BLANK">statuts de l\'association</a>'
MEMBER_TERMS = False

# When should we remind a member about its membership ?  List of deltas from
# the anniversary date, can be a combination of positive (after anniversary)
# and negative (before aniversary) af months, days and weeks.
MEMBERSHIP_FEE_REMINDER_DATES = [
    {'months': -3},  # 3 months before
    {'months': -2},  # 2 months before
    {'months': -1},  # 1 month before
    {'days': 0},     # the day of anniversary
    {'months': +1},  # 1 month after
]

# Customize template titles
SITE_TITLE = "COIN - SI"
SITE_HEADER = "COIN est un Outil pour un Internet Neutre"
SITE_LOGO_URL = ''

# Pattern used to display a unique reference for any subscription
# Helpful for bank wire transfer identification
SUBSCRIPTION_REFERENCE = 'REF-{subscription.offer.reference}-{subscription.pk}'

# Template string to display the label the member should indicates for the bank
# transfer
MEMBERSHIP_REFERENCE = "ADH-{user.pk}"

# Payment delay in days
PAYMENT_DELAY = 30

# Reset session if cookie older than 2h.
SESSION_COOKIE_AGE = 7200

# RSS/Atom feeds to display on dashboard
# feed name (used in template), url, max entries to display
# "isp" entry gets picked automatically in default index template
FEEDS = (
    ('ffdn', 'http://www.ffdn.org/fr/rss.xml', 3),
#    ('isp', 'http://isp.example.com/feed/', 3),
)

# Allow user to edit their VPS Info
MEMBER_CAN_EDIT_VPS_CONF = True

# Allow user to edit their VPN Info
MEMBER_CAN_EDIT_VPN_CONF = True

# Account registration
# Allow visitor to join the association by register on COIN
REGISTRATION_OPEN = False

# All account with unvalidated email will be deleted after X days
ACCOUNT_ACTIVATION_DAYS = 7

# Member can edit their own data
MEMBER_CAN_EDIT_PROFILE = False

# Allows to deactivate displays and calculations of balances.
HANDLE_BALANCE = False

# Add subscription comments in invoice items
INVOICES_INCLUDE_CONFIG_COMMENTS = True

# String template used for the IP allocation log (c.f. coin.subnet loggers
# This will get prefixed by [Allocating IP] or [Desallocating IP]
IP_ALLOCATION_MESSAGE = None

## maillist module
# Command that push mailling-list subscribers to the lists server
MAILLIST_SYNC_COMMAND = ''

# External account
# This label appears in member account if user can ask for a subscription
# on an external account
VERBOSE_NAME_EXTERNAL_ACCOUNT = 'Compte externe'
VERBOSE_NAME_PLURAL_EXTERNAL_ACCOUNT = 'Comptes externes'

# Backend for sendfile. In production should be replaced by a better backend
# for more details see: https://django-sendfile2.readthedocs.io/en/latest/backends.html
SENDFILE_BACKEND = 'django_sendfile.backends.simple'