from .settings_base import *

# settings for unit tests

EXTRA_INSTALLED_APPS = (
    'hardware_provisioning',
    'maillists',
    'vpn',
    'vps',
    'housing',
)

TEMPLATE_DIRS = EXTRA_TEMPLATE_DIRS + TEMPLATE_DIRS
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': TEMPLATE_DIRS,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': TEMPLATE_CONTEXT_PROCESSORS
        },
    },
]

# Backend for sendfile
SENDFILE_BACKEND = 'django_sendfile.backends.development'
