# -*- coding: utf-8  -*-

from .settings_base import *

DATABASES = {
    # Database hosted on vagant test box
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'coin',
        'USER': 'coin',
        'PASSWORD': 'kouaingkouaing',
        'HOST': 'db',
        'PORT': '5432',
    },
}


EXTRA_INSTALLED_APPS = (
    'hardware_provisioning',
    'maillists',
    'vpn',
    'vps',
    'housing',
    'internet_access',
    'external_account',
)

# Surcharge les paramètres en utilisant le fichier settings_local.py
try:
    from .settings_local import *
except ImportError:
    pass

TEMPLATE_DIRS = EXTRA_TEMPLATE_DIRS + TEMPLATE_DIRS
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': TEMPLATE_DIRS,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': TEMPLATE_CONTEXT_PROCESSORS
        },
    },
]
