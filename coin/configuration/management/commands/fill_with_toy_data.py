# -*- coding: utf-8 -*-

from argparse import RawTextHelpFormatter
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.hashers import make_password

from coin.resources.models import IPPool
from coin.offers.models import Offer, OfferSubscription, OfferIPPool
from coin.configuration.models import Configuration
from coin.members.models import Member

################################################################################


class Command(BaseCommand):

    help = __doc__

    def create_parser(self, *args, **kwargs):
        parser = super().create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def handle(self, *args, **options):

        Member.objects.all().delete()
        IPPool.objects.all().delete()
        Offer.objects.all().delete()
        OfferIPPool.objects.all().delete()

        admin = Member.objects.create_superuser(username="admin", first_name="Syssa", last_name="Mine", email="admin@yolo.test", password="Yunohost")
        member1 = Member.objects.create(username="sasha", first_name="Sasha", last_name="Yolo", email="sasha@yolo.test")
        member2 = Member.objects.create(username="camille", first_name="Camille", last_name="Yolo", email="camille@yolo.test")
        admin.save()
        member1.save()
        member2.save()

        pool1 = IPPool.objects.create(name="Pool d'IP pour VPS", default_subnetsize=32, inet="10.20.0.0/24")
        pool2 = IPPool.objects.create(name="Pool d'IP pour VPN", default_subnetsize=32, inet="10.30.0.0/24")
        pool1.save()
        pool2.save()

        vps_de_test = Offer.objects.create(name="VPS de test", reference="vps-test", configuration_type="VPS", period_fees=0, initial_fees=0, non_billable=True)
        vps_small = Offer.objects.create(name="VPS 1core 1G", reference="vps-1core-1G", configuration_type="VPS", period_fees=8, initial_fees=0, non_billable=False)
        vps_big = Offer.objects.create(name="VPS 2core 2G", reference="vps-2core-2G", configuration_type="VPS", period_fees=15, initial_fees=0, non_billable=False)
        vps_de_test.save()
        vps_small.save()
        vps_big.save()
        OfferIPPool.objects.create(ip_pool=pool1, to_assign=True, offer=vps_de_test).save()
        OfferIPPool.objects.create(ip_pool=pool1, to_assign=True, offer=vps_small).save()
        OfferIPPool.objects.create(ip_pool=pool1, to_assign=True, offer=vps_big).save()

        vpn_de_test = Offer.objects.create(name="VPN de test", reference="vpn-test", configuration_type="VPN", period_fees=0, initial_fees=0, non_billable=True)
        vpn_standard = Offer.objects.create(name="VPN standard", reference="vpn-standard", configuration_type="VPN", period_fees=4, initial_fees=0, non_billable=False)
        vpn_de_test.save()
        vpn_standard.save()
        OfferIPPool.objects.create(ip_pool=pool2, to_assign=True, offer=vpn_de_test).save()
        OfferIPPool.objects.create(ip_pool=pool2, to_assign=True, offer=vpn_standard).save()
