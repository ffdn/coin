# -*- coding: utf-8 -*-
"""
Fetch all states for configuration of a given type (e.g. VPS, ...) using the
FETCH_STATES hook define for this conf type using settings.HOOKS.

This command is meant to be called regularly from a cron job like :

    */10 * * * * root manage.py update_configuration_states VPS


The {conf_list} is a python list containing dicts like :
    [ {"id": 4, "offer_name": "VPS-de-test", "offer_ref": "vps-test", "username": "sam" },
      {"id": 6, "offer_name": "VPS-de-test", "offer_ref": "vps-test", "username": "saeed" },
      {"id": 7, "offer_name": "VPS-2G-500",  "offer_ref": "vps-2g",   "username": "sasha" }
    ]

The {conf_list_csv} contains the same kind of information, but in the form of a CSV
which is expected to be easier to interface with for bash hooks.

    4;VPS-de-test;vps-test;sam
    6;VPS-de-test;vps-test;seed
    7;VPS-2G-500;vps-2g;sasha

The hook is expected to return the following kind of output (loaded from json or yaml or ...)

    [{"id":4, "provisioned": "disabled", "status": "down", "status_color": "red"},
     {"id":6, "provisioned": "yes",      "status": "running", "status_color": "green"},
     {"id":7, "provisioned": "ongoing",  "status": "booting", "status_color": "orange"}
    ]
"""


from argparse import RawTextHelpFormatter
from django.core.management.base import BaseCommand, CommandError

from coin.configuration.models import Configuration
from coin.utils import get_descendant_classes

################################################################################


class Command(BaseCommand):

    help = __doc__

    def create_parser(self, *args, **kwargs):
        parser = super().create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def add_arguments(self, parser):

        parser.add_argument(
            'conf_type',
            type=str,
            help="Something like VPS, VPN, Housing, ..."
        )

    def handle(self, *args, **options):

        if not options["conf_type"] != "":
            raise CommandError("You must provide a configuration type")

        conf_classes = {c.url_namespace: c for c in get_descendant_classes(Configuration)}
        if options["conf_type"] not in conf_classes:
            raise CommandError("Unknown conf type %s ... known conf types are : %s" % (options["conf_type"], ', '.join(list(conf_classes.keys()))))

        conf_classes[options["conf_type"]].fetch_and_update_all_states()
