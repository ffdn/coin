# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0003_configuration_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuration',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_configuration.configuration_set+', editable=False, to='contenttypes.ContentType', null=True, on_delete=models.CASCADE),
        ),
    ]
