# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0005_auto_20200717_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuration',
            name='comment',
            field=models.CharField(help_text="Ce texte s'affiche dans l'interface de l'abonn\xe9\\u22c5e", max_length=512, verbose_name='commentaire', blank=True),
        ),
    ]
