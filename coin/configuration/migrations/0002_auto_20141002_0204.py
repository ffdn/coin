# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0001_initial'),
        ('contenttypes', '0001_initial'),
        ('configuration', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='offersubscription',
            field=models.OneToOneField(related_name='configuration', verbose_name='abonnement', to='offers.OfferSubscription', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='configuration',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_configuration.configuration_set', editable=False, to='contenttypes.ContentType', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
