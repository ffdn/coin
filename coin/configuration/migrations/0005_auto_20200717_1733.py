# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0004_auto_20161015_1837'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='last_status_update',
            field=models.DateTimeField(null=True, verbose_name="derni\xe8re mise \xe0 jour de l'\xe9tat", blank=True),
        ),
        migrations.AddField(
            model_name='configuration',
            name='provisioned',
            field=models.CharField(default='no', max_length=16, verbose_name='provisionn\xe9', choices=[('no', 'Non'), ('ongoing', 'En cours'), ('yes', 'Oui'), ('disabled', 'D\xe9sactiv\xe9 temporairement')]),
        ),
        migrations.AddField(
            model_name='configuration',
            name='status',
            field=models.CharField(default='N/A', max_length=32, verbose_name='status'),
        ),
        migrations.AddField(
            model_name='configuration',
            name='status_color',
            field=models.CharField(default='grey', max_length=16, blank=True, choices=[('green', 'green'), ('orange', 'orange'), ('red', 'red'), ('grey', 'grey')]),
        ),
    ]
