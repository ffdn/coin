# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0002_auto_20141002_0204'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='comment',
            field=models.CharField(default='', max_length=512, verbose_name='commentaire', blank=True),
            preserve_default=False,
        ),
    ]
