# -*- coding: utf-8 -*-
import datetime
import csv
from io import StringIO

import logging
from ipaddress import ip_address, ip_network

from netfields import InetAddressField, NetManager

from django.db import models
from polymorphic.models import PolymorphicModel
from coin.offers.models import OfferSubscription
from django.db.models.signals import post_save, post_delete
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from django.conf import settings
from django.utils.safestring import mark_safe
from django.core.exceptions import ValidationError

from coin.utils import get_descendant_classes
from coin.hooks import HookManager
from coin.resources.models import IPSubnet
from coin import validation

"""
Implementation note : Configuration is a PolymorphicModel.
The childs of Configuration are the differents models to store
technical informations of a subscription.

To add a new configuration backend, you have to create a new app with a model
which inherit from Configuration.
Your model can implement Meta verbose_name to have human readable name and a
url_namespace variable to specify the url namespace used by this model.
"""

class Configuration(PolymorphicModel):

    offersubscription = models.OneToOneField(OfferSubscription,
                                             related_name='configuration',
                                             verbose_name='abonnement',
                                             on_delete=models.CASCADE)
    comment = models.CharField(blank=True, max_length=512,
                               verbose_name="commentaire",
                               help_text="Ce texte s'affiche dans l'interface de l'abonné⋅e")

    # Is this service configured / served on the actual infrastructure ?
    # This can be 'no', 'yes', 'ongoing' (e.g. machine being configured) or 'temporary disabled'
    provisioned_choices = [("no", "Non"), ("ongoing", "En cours"), ("yes", "Oui"), ("disabled", "Désactivé temporairement")]
    provisioned = models.CharField(default="no", max_length=16, choices=provisioned_choices, blank=False, null=False, verbose_name='provisionné')

    # Subclasses can fill this list with stuff like "operating_system"
    # or "ssh_key" or "password" or whichever field is related to provisionning
    provisioning_infos = []

    # Is this service actually running and usable ?
    # E.g. a VPS can be provisionned but be powered down
    # The value is free, to be set manually by the admin, or (if state hook is enabled) via a hook.
    # The status_color can be used to signify if status is good / bad / other...
    status = models.CharField(default="N/A", blank=False, null=False, max_length=32, verbose_name="status")
    status_color_choices = ["green", "orange", "red", "grey"]
    status_color = models.CharField(default="grey", max_length=16, choices=[(c, c) for c in status_color_choices], blank=True, null=False)
    last_status_update = models.DateTimeField(null=True, blank=True,
                                              verbose_name="dernière mise à jour de l'état")

    @classmethod
    def provision_is_managed_via_hook(self):
        return HookManager.is_defined(self.url_namespace, "PROVISION")

    @classmethod
    def state_is_managed_via_hook(self):
        return HookManager.is_defined(self.url_namespace, "FETCH_ALL_STATES")

    @staticmethod
    def get_configurations_choices_list():
        """
        Génère automatiquement la liste de choix possibles de configurations
        en fonction des classes enfants de Configuration
        """
        return tuple([(c.__name__, c._meta.verbose_name) for c in get_descendant_classes(Configuration)])

    def convert_to_dict_for_hook(self):
        # FIXME : check assertions here
        return {"id": self.pk,
                "offer_name": self.offersubscription.offer.name.encode('utf-8'),
                "offer_ref": self.offersubscription.offer.reference.encode('utf-8'),
                "username": self.offersubscription.member.username.encode('utf-8'),
                }

    def provision(self):
        assert self.provisioned == "no", "Cette configuration est déjà provisionnée"
        success, out, err = HookManager.run(self.configuration_type_name(), "PROVISION", **(self.convert_to_dict_for_hook()))
        if not success:
            raise Exception("Le provisionnement a échoué : %s" % err)
        # If state is managed via hooks, this info will be updated via the hook.
        # So for now we just set it to 'ongoing' so that the button 'provision' is not showed while we wait for the state to be updated
        if self.state_is_managed_via_hook():
            self.provisioned = "ongoing"
        # Otherwise, just flag this as provisioned because no hook will update this info
        else:
            self.provisioned = "yes"
        self.save()

    def deprovision(self):
        assert self.provisioned == "yes", "Cette configuration n'est pas provisionnée"
        success, out, err = HookManager.run(self.configuration_type_name(), "DEPROVISION", **(self.convert_to_dict_for_hook()))
        if not success:
            raise Exception("Le déprovisionnement a échoué : %s" % err)
        self.provisioned = "no"
        self.save()

    def update_state(self, provisioned=None, status=None, status_color=None, **kwargs):
        if provisioned is not None:
            provisioned = 'yes' if provisioned is True else provisioned
            provisioned = 'no' if provisioned is False else provisioned
            self.provisioned = provisioned
        if status is not None:
            self.status = status
        if status_color is not None:
            self.status_color = status_color
        for key, value in kwargs.items():
            if value is not None:
                setattr(self, key, value)
        if any([info is not None for info in [provisioned, status, status_color]]):
            self.last_status_update = datetime.datetime.now()
            self.save()

    @classmethod
    def fetch_and_update_all_states(self):
        """
        Fetch all states for configuration of a given type (e.g. VPS, ...) using the
        FETCH_ALL_STATES hook define for this conf type using settings.HOOKS.

        The {conf_list} is a python list containing dicts like :
            [ {"id": 4, "offer_name": "VPS-de-test", "offer_ref": "vps-test", "username": "sam" },
              {"id": 6, "offer_name": "VPS-de-test", "offer_ref": "vps-test", "username": "saeed" },
              {"id": 7, "offer_name": "VPS-2G-500",  "offer_ref": "vps-2g",   "username": "sasha" }
            ]

        The {conf_list_csv} contains the same kind of information, but in the form of a CSV
        which is expected to be easier to interface with for bash hooks.

            4;VPS-de-test;vps-test;sam
            6;VPS-de-test;vps-test;seed
            7;VPS-2G-500;vps-2g;sasha

        The hook is expected to return the following kind of output (loaded from json or yaml or ...)

            [{"id":4, "provisioned": "disabled", "status": "down", "status_color": "red"},
             {"id":6, "provisioned": "yes",      "status": "running", "status_color": "green"},
             {"id":7, "provisioned": "ongoing",  "status": "booting", "status_color": "orange"}
            ]
        """
        assert self.state_is_managed_via_hook(), "There is no hook defined for %s" % self.url_namespace
        confs_to_update = self.objects.all()

        conf_list = [c.convert_to_dict_for_hook() for c in confs_to_update]
        conf_list_csv = StringIO()
        info_keys = list(conf_list[-1].keys())
        writer = csv.writer(conf_list_csv, delimiter=str(';'), quotechar=str('"'), quoting=csv.QUOTE_MINIMAL)
        writer.writerow([key for key in info_keys])
        for c in conf_list:
            row = []
            for info in info_keys:
                try:
                    c[info] = c[info].encode('utf8')
                except:
                    pass
                row.append(c.get(info))
            writer.writerow(row)
        conf_list_csv = conf_list_csv.getvalue()
        success, out, err = HookManager.run(self.url_namespace, "FETCH_ALL_STATES", conf_list=conf_list, conf_list_csv=conf_list_csv)

        if not success:
            raise Exception("Some errors happened during the execution of FETCH_ALL_STATES for %s : %s" % (self.url_namespace, err))

        assert isinstance(out, list), "Was expecting to get a list as output of FETCH_ALL_STATES"
        for state_infos in out:
            assert isinstance(state_infos, dict) and "id" in state_infos, "Was expecting to get a list of dict as output of FETCH_ALL_STATES with at least 'id' in it"
            try:
                id_ = state_infos.pop(u"id")
                conf_to_update = Configuration.objects.get(pk=id_)
                conf_to_update.update_state(**state_infos)
            except Exception as e:
                print("error with %s %s %s" % (str(id_), str(state_infos),  str(e)))

    def get_state_display(self):

        if self.provisioned == "yes":
            text = self.status
            color = self.status_color
        elif self.provisioned == "ongoing":
            text = "Pas encore provisionné"
            color = "orange"
        elif self.provisioned == "disabled":
            text = "Désactivé"
            color = "red"
        else:
            text = "Pas encore provisionné"
            color = "grey"

        return (text, color)

    def get_state_text_display(self):

        return self.get_state_display()[0]

    def get_state_icon_display(self):

        text, color = self.get_state_display()
        text = "Status: %s\nDernière mise à jour: %s" % (text, str(self.last_status_update))

        return mark_safe('<i class="fa fa-circle" style="color:{color}" title="{text}"></i>'.format(color=color, text=text))
    get_state_icon_display.short_description = 'État'

    @property
    def state_balance(self):
        if self.offersubscription.member.balance <= -20:
            style = "background-color:red; color: white;"
        elif self.offersubscription.member.balance < 0:
            style = "background-color:orange; color: white;"
        else:
            style = ""

        return mark_safe('<span style="{style}">{text} €</span>'.format(
                         style=style,
                         text=str(self.offersubscription.member.balance)))

    def __str__(self):
        return str(self.offersubscription)

    def model_name(self):
        return self.__class__.__name__
    model_name.short_description = 'Nom du modèle'

    def configuration_type_name(self):
        return self.url_namespace
    configuration_type_name.short_description = 'Type'

    def get_absolute_url(self):
        """
        Renvoi l'URL d'accès à la page "details" de l'objet
        Une url doit être nommée "details"
        """
        from django.urls import reverse
        return reverse('%s:details' % self.get_url_namespace(),
                       args=[str(self.id)])

    def get_url_namespace(self):
        """
        Renvoi le namespace utilisé par la configuration. Utilise en priorité
        celui définit dans la classe enfant dans url_namespace sinon
        par défaut utilise le nom de la classe en minuscule
        """
        if hasattr(self, 'url_namespace') and self.url_namespace:
            return self.url_namespace
        else:
            return self.model_name().lower()

    #
    # Standard hook to fetch states
    #

    def _has_hook_state(self):
        return HookManager.is_defined(self.configuration_type_name(), "GET_STATE")

    def _get_state(self, key=None):
        if self._has_hook_state():
            # FIXME : gotta see what info to feed exactly
            state = HookManager.run(self.configuration_type_name(), "GET_STATE", self)
            if key is None:
                return state
            else:
                value = state.get(key, None)
                return value if value else "N/A"
        else:
            return "Inconnu (pas de hook provisionné)"

    def is_provisioned(self):
        return self._get_state("provisioned")
    is_provisioned.short_description = 'Provisionné'

    def state_display(self):
        return self._get_state("display")
    state_display.short_description = 'État'

    #
    # Standard hook to provision
    #

    def _has_hook_provision(self):
        return HookManager.is_defined(self.configuration_type_name(), "PROVISION")


    def bulk_related_objects(self, objs, *args, **kwargs):
        # Fix delete screen. Workaround for https://github.com/chrisglass/django_polymorphic/issues/34
        return super().bulk_related_objects(objs, *args, **kwargs).non_polymorphic()

    class Meta:
        verbose_name = 'configuration'


class IPConfiguration(Configuration):
    ipv4_endpoint = InetAddressField(validators=[validation.validate_v4],
                                     verbose_name="IPv4", blank=True, null=True,
                                     help_text="Adresse IPv4 utilisée comme endpoint")
    ipv6_endpoint = InetAddressField(validators=[validation.validate_v6],
                                     verbose_name="IPv6", blank=True, null=True,
                                     help_text="Adresse IPv6 utilisée comme endpoint")
    #objects = NetManager()

    # This method is part of the general configuration interface.
    def subnet_event(self):
        self.check_endpoints(delete=True)
        # We potentially changed the endpoints, so we need to save.  Also,
        # saving will update the subnets in the LDAP backend.
        self.full_clean()
        self.save()

    def get_subnets(self, version):
        subnets = self.ip_subnet.all()
        return [subnet for subnet in subnets if subnet.inet.version == version]

    def generate_endpoints(self, v4=True, v6=True):
        """Generate IP endpoints in one of the attributed IP subnets.  If there is
        no available subnet for a given address family, then no endpoint
        is generated for this address family.  If there already is an
        endpoint, do nothing.

        Returns True if an endpoint was generated.
        """
        offer = self.offersubscription.offer
        # Note: lorsqu'un pool d'IP est rempli, le FAI va devoir changer les
        # pools d'IP associé à l'offre, du coup il se peut que d'anciens VPS
        # aient des subnets qui ne sont plus référencés dans les pools d'IPs
        # configurés pour une offre. Il faut donc avoir ça en tête.
        # Du coup, on retire de l'affectation automatique les sous-réseaux
        # qui sont explicitement indiquer comme ne servant pas pour le
        # endpoints (case "utiliser pour les ips d'endpoints")
        subnets = {s for s in self.ip_subnet.all()}
        subnets -= {s for s in self.ip_subnet.filter(
            ip_pool__offer_ip_pools__to_assign=False,
            ip_pool__offer_ip_pools__offer=offer)}
        updated = False
        if v4 and self.ipv4_endpoint is None:
            subnets_v4 = [s for s in subnets if s.inet.version == 4]
            if len(subnets_v4) > 0:
                self.ipv4_endpoint = ip_network(subnets_v4[0].inet)
                updated = True
        if v6 and self.ipv6_endpoint is None:
            subnets_v6 = [s for s in subnets if s.inet.version == 6]
            if len(subnets_v6) > 0:
                # With v6, we choose the second host of the subnet (cafe::1)
                inet = subnets_v6[0].inet
                if inet.prefixlen != 128:
                    gen = inet.hosts()
                    next(gen)
                    self.ipv6_endpoint = next(gen)
                else:
                    self.ipv6_endpoint = ip_network(inet)
                updated = True
        return updated

    def check_endpoints(self, delete=False):
        """Check that the IP endpoints are included in one of the attributed IP
        subnets.

        If [delete] is True, then simply delete the faulty endpoints
        instead of raising an exception.
        """
        error = "L'IP {} n'est pas dans un réseau attribué."
        subnets = self.ip_subnet.all()
        is_faulty = lambda endpoint: endpoint and not any([endpoint in subnet.inet for subnet in subnets])
        if is_faulty(self.ipv4_endpoint):
            if delete:
                self.ipv4_endpoint = None
            else:
                raise ValidationError(error.format(self.ipv4_endpoint))
        if is_faulty(self.ipv6_endpoint):
            if delete:
                self.ipv6_endpoint = None
            else:
                raise ValidationError(error.format(self.ipv6_endpoint))

    def fill_empty_fields(self):
        provided = {}
        if not self.ip_subnet.all():
            for offer_ip_pool in self.offersubscription.offer.offer_ip_pools.order_by('-to_assign'):
                if provided.get(offer_ip_pool.ip_pool.name, False):
                    continue
                try:
                    IPSubnet.objects.create(
                                configuration=self,
                                ip_pool=offer_ip_pool.ip_pool)
                    provided[offer_ip_pool.ip_pool.name] = True
                # IP pool full
                except ValidationError:
                    # If an ip pool is full we can go to the next ip pool with the same name
                    pass

        # If IP endpoints are not specified,
        # generate them automatically.
        if self.ipv4_endpoint is None or self.ipv6_endpoint is None:
            self.generate_endpoints()
            super().save()

    def save(self, **kwargs):
        config = super().save(**kwargs)
        self.fill_empty_fields()
        return config

    def clean(self):
        self.check_endpoints()

    def convert_to_dict_for_hook(self):
        d = super().convert_to_dict_for_hook()
        if self.ipv4_endpoint:
            d["ipv4"] = str(self.ipv4_endpoint)
        if self.ipv6_endpoint:
            d["ipv6"] = str(self.ipv6_endpoint)
        prefix = [str(subnet) for subnet in self.get_subnets(6) if self.ipv6_endpoint not in subnet.inet]
        prefix = ','.join(prefix)
        if prefix != '':
            d['prefix'] = prefix

        return d

    class Meta:
        abstract = True



@receiver(post_save, sender=IPSubnet)
def subnet_event_save(sender, **kwargs):
    kwargs["signal_type"] = "save"
    subnet_event(sender, **kwargs)


@receiver(post_delete, sender=IPSubnet)
def subnet_event_delete(sender, **kwargs):
    kwargs["signal_type"] = "delete"
    subnet_event(sender, **kwargs)

subnet_log = logging.getLogger("coin.subnets")
def subnet_event(sender, **kwargs):
    """Fires when a subnet is created, modified or deleted.  We tell the
    configuration backend to do whatever it needs to do with it.

    Note that we could provide a more advanced API to configurations
    (subnet created, subnet modified, subnet deleted), but this is quite
    complicated to do.  It's much simpler to simply tell the configuration
    model that something has changed in IP subnets.  The configuration
    model can then access the list of its associated subnets (via the
    "ip_subnet" attribute) to decide for itself what it wants to do.

    We should use a pre_save/pre_delete signal, so that if anything goes
    wrong in the backend (exception raised), nothing is actually saved in
    the database: this provides consistency between the database and the
    backend.  But if we do this, there is a major issue: the configuration
    backend will not see the new state of subnets by querying the
    database, since the changes have not been saved into the database yet.

    That's why we use a post_save/post_delete signal instead.  In theory,
    this is a bad idea, because if the backend fails to do whatever it
    needs to do, the subnet will be saved into Django's database anyway,
    causing a desynchronisation with the backend.  But surprisingly, even
    if not a documented feature of Django's signals, all is well: if we
    raise an exception here, the IPSubnet object will not be saved in the
    database.  It looks like the database rollbacks if an exception is
    raised, which is great (even if undocumented).

    """
    subnet = kwargs['instance']
    if not settings.IP_ALLOCATION_MESSAGE:
        return
    try:
        config = subnet.configuration
        if hasattr(config, 'subnet_event'):
            config.subnet_event()

        offer = config.offersubscription.offer.name
        ref = config.offersubscription.get_subscription_reference()
        member = config.offersubscription.member
        ip = subnet.inet

        if kwargs['signal_type'] == "save":
            msg = "[Allocating IP] " + settings.IP_ALLOCATION_MESSAGE
        elif kwargs['signal_type'] == "delete":
            msg = "[Deallocating IP] " + settings.IP_ALLOCATION_MESSAGE
        else:
            # Does not happens
            msg = ""

        subnet_log.info(msg.format(ip=ip, member=member, offer=offer, ref=ref))
    except ObjectDoesNotExist as e:
        subnet_log.info("No subnet allocated")
        subnet_log.info(str(e))


