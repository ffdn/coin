# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404
from django.contrib import admin, messages
from django.http import HttpResponseRedirect
from django.conf.urls import url
from django.conf import settings
from django.urls import reverse
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin

from coin.resources.models import IPSubnet
from coin.configuration.models import Configuration, IPConfiguration
from coin.configuration.forms import ConfigurationForm

"""
Implementation note : When creating child admin class, you have to inherit
ConfigurationAdminFormMixin. This make use of ConfigurationForm form that
filter offersubscription select input to avoid selecting wrong subscription.
"""


class IPSubnetInline(admin.TabularInline):
    model = IPSubnet
    extra = 0
    fields = ["inet", "ip_pool"]


class ParentConfigurationAdmin(PolymorphicParentModelAdmin):
    base_model = Configuration
    polymorphic_list = True
    list_display = ('model_name', 'configuration_type_name', 'offersubscription', 'offer_subscription_member')

    def offer_subscription_member(self, config):
        return config.offersubscription.member
    offer_subscription_member.short_description = 'Membre'

    def get_child_models(self):
        """
        Renvoi la liste des modèles enfants de Configuration
        ex :(VPNConfiguration, ADSLConfiguration, ...)
        """

        return tuple(x.base_model for x in ChildConfigurationAdmin.__subclasses__())

    def get_urls(self):
        """
        Fix a django-polymorphic bug that randomly set wrong url for a child
        model in admin.
        This remove somes dummy urls that have not to be returned by the parent model
        https://github.com/chrisglass/django_polymorphic/issues/105
        """
        urls = super().get_urls()
        for model in self.get_child_models():
            admin = self._get_real_admin_by_model(model)
            for admin_url in admin.get_urls():
                for url in urls:
                    if url.name == admin_url.name:
                        urls.remove(url)
        return urls


class ChildConfigurationAdmin(PolymorphicChildModelAdmin):

    base_form = ConfigurationForm

    change_form_template = "admin/configuration/configuration/change_form.html"

    specific_fields = []

    def get_list_display(self, request, obj=None):
        list_display = list(self.list_display[:])  # Copy

        if settings.HANDLE_BALANCE:
            list_display.insert(3, 'state_balance')

        return list_display

    def get_inlines(self, request, obj=None):

        if isinstance(obj, IPConfiguration):
            return tuple(IPSubnetInline, )
        else:
            return tuple()

    def get_fieldsets(self, request, obj=None):
        fields = [('', {'fields': ['offersubscription',
                                   'comment',
                                   'provisioned',
                                   'status']})]

        if self.specific_fields:
            fields.append(["Configuration spécifique",
                           {"fields": tuple(self.specific_fields)}])

        if hasattr(obj, "provisioning_infos") and obj.provisioning_infos:
            fields.append(["Informations pour le provisionnement",
                           {"fields": tuple(obj.provisioning_infos)}])

        if hasattr(obj, "ipv4_endpoint") and hasattr(obj, "ipv6_endpoint"):
            fields.append(["Adresses IP (endpoints)",
                           {"fields": tuple(["ipv4_endpoint", "ipv6_endpoint"])}])

        return fields

    def get_readonly_fields(self, request, obj=None):

        readonly_fields = []
        if self.base_model.provision_is_managed_via_hook():
            readonly_fields.append("provisioned")

        if self.base_model.state_is_managed_via_hook():
            readonly_fields.append("status")

        # If this config is provisioned, we shouldn't change the subscription or the provisioning infos
        if obj and obj.provisioned != "no":
            readonly_fields.append("offersubscription")
            if hasattr(obj, "provisioning_infos") and obj.provisioning_infos:
                readonly_fields.extend(obj.provisioning_infos)

        return readonly_fields

    def get_urls(self):
        """
        Custom admin urls
        """
        urls = super().get_urls()
        return urls + [
            url(r'^provision/(?P<id>.+)$',
                self.admin_site.admin_view(self.provision_view),
                name='configuration_provision'),
            url(r'^deprovision/(?P<id>.+)$',
                self.admin_site.admin_view(self.deprovision_view),
                name='configuration_deprovision'),
        ]

    def provision_view(self, request, id):
        # TODO : Add better perm here
        if request.user.is_superuser:
            conf = get_object_or_404(Configuration, pk=id)
            conf.provision()
            #try:
            #    conf.provision()
            #except Exception as e:
            #    messages.error(request, "Le provisionnement a échoué : %s" % e)
            #else:
            #    messages.success(request, "Le provisionnement a été déclenché")
        else:
            messages.error(request,
                "Vous n'avez pas l'autorisation de provisionner / déprovisionner des configurations.")

        return HttpResponseRedirect(reverse('admin:configuration_configuration_change',
                                            args=(id,)))

    def deprovision_view(self, request, id):
        # TODO : Add better perm here
        if request.user.is_superuser:
            conf = get_object_or_404(Configuration, pk=id)
            try:
                conf.deprovision()
            except Exception as e:
                messages.error(request, "Le déprovisionnement a échoué : %s" % e)
            else:
                messages.success(request, "Le déprovisionnement a été déclenché")
        else:
            messages.error(request,
                "Vous n'avez pas l'autorisation de provisionner / déprovisionner des configurations.")

        return HttpResponseRedirect(reverse('admin:configuration_configuration_change',
                                            args=(id,)))

class ChildConfigurationAdminInline(admin.StackedInline):

    specific_fields = []

    def get_fieldsets(self, request, obj=None):

        fields = [('', {'fields': ['comment',
                                   'provisioned',
                                   'status']})]

        conf = obj.configuration

        if self.specific_fields:
            fields.append(["Configuration spécifique",
                           {"fields": tuple(self.specific_fields)}])

        if hasattr(conf, "provisioning_infos") and conf.provisioning_infos:
            fields.append(["Informations pour le provisionnement",
                           {"fields": conf.provisioning_infos}])

        if hasattr(conf, "ipv4_endpoint") and hasattr(conf, "ipv6_endpoint"):
            fields.append(["Adresses IP (endpoints)",
                           {"fields": ["ipv4_endpoint", "ipv6_endpoint"]}])

        return fields

    def get_readonly_fields(self, request, obj=None):

        readonly_fields = []
        if self.model.provision_is_managed_via_hook():
            readonly_fields.append("provisioned")

        if self.model.state_is_managed_via_hook():
            readonly_fields.append("status")

        conf = obj.configuration

        # If this config is provisioned, we shouldn't change the subscription or the provisioning infos
        if conf and conf.provisioned != "no":
            readonly_fields.append("offersubscription")
            if hasattr(conf, "provisioning_infos") and conf.provisioning_infos:
                readonly_fields.extend(conf.provisioning_infos)

        return readonly_fields



admin.site.register(Configuration, ParentConfigurationAdmin)
