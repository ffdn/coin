# -*- coding: utf-8 -*-

from .settings_base import *

# Surcharge les paramètres en utilisant le fichier settings_local.py
try:
    from .settings_local import *
except ImportError as e:
    import sys
    print(f"Failed to import settings_local : {e}", file=sys.stderr)

TEMPLATE_DIRS = EXTRA_TEMPLATE_DIRS + TEMPLATE_DIRS
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': TEMPLATE_DIRS,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': TEMPLATE_CONTEXT_PROCESSORS
        },
    }
]
