# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='configuration_type',
            field=models.CharField(help_text='Type de configuration \xe0 utiliser avec cette offre', max_length=50, verbose_name='type de configuration', blank=True),
        ),
    ]
