# -*- coding: utf-8 -*-

from django.db import migrations, models
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('offers', '0010_auto_20170818_1835'),
    ]

    operations = [
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.CharField(default='pending', max_length=14, verbose_name='status', choices=[('pending', 'En attente'), ('accepted', 'Accept\xe9'), ('refused', 'Refus\xe9')])),
                ('request_date', models.DateTimeField(default=datetime.datetime.now, verbose_name='date de la demande')),
                ('resolution_date', models.DateTimeField(null=True, verbose_name='date de r\xe9solution', blank=True)),
                ('member_comments', models.TextField(help_text='Commentaires libres', verbose_name='commentaires du membre', blank=True)),
                ('admin_comments', models.TextField(help_text="Commentaires de l'\xe9quipe b\xe9n\xe9vole", verbose_name="commentaires de l'\xe9quipe b\xe9n\xe9vole", blank=True)),
            ],
            options={
                'verbose_name': 'demande',
            },
        ),
        migrations.AddField(
            model_name='offer',
            name='infos',
            field=models.TextField(help_text='Informations compl\xe9mentaires pour les membres, affich\xe9es dans leur espace', verbose_name='infos compl\xe9mentaires', blank=True),
        ),
        migrations.AddField(
            model_name='offer',
            name='requestable',
            field=models.BooleanField(default=False, verbose_name='Permettre aux membres de cr\xe9er des demandes pour cette offre depuis leur espace'),
        ),
        migrations.AlterField(
            model_name='offer',
            name='ip_pools',
            field=models.ManyToManyField(related_name='offers', through='offers.OfferIPPool', to='resources.IPPool'),
        ),
        migrations.AlterField(
            model_name='offerippool',
            name='ip_pool',
            field=models.ForeignKey(related_name='offer_ip_pools', verbose_name="pool d'IP", to='resources.IPPool', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='offerippool',
            name='offer',
            field=models.ForeignKey(related_name='offer_ip_pools', verbose_name='offre', to='offers.Offer', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='offerippool',
            name='to_assign',
            field=models.BooleanField(default=False, help_text="Si vous cocher cette case, COIN utilisera ce pool d'IP afin de d\xe9finir l'IP d'endpoint", verbose_name="utiliser pour les IP d'endpoints"),
        ),
        migrations.CreateModel(
            name='OfferSubscriptionRequest',
            fields=[
                ('request_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='offers.Request', on_delete=models.CASCADE)),
                ('offer', models.ForeignKey(verbose_name='offre', to='offers.Offer', on_delete=models.CASCADE)),
                ('offersubscription', models.OneToOneField(related_name='subscriptionrequest', null=True, blank=True, to='offers.OfferSubscription', verbose_name='abonnement cr\xe9\xe9', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'demande de souscription',
                'verbose_name_plural': 'demandes de souscription',
            },
            bases=('offers.request',),
        ),
        migrations.AddField(
            model_name='request',
            name='member',
            field=models.ForeignKey(verbose_name='membre', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='request',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_offers.request_set+', editable=False, to='contenttypes.ContentType', null=True, on_delete=models.CASCADE),
        ),
    ]
