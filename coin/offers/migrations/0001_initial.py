# -*- coding: utf-8 -*-

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Offer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name="nom de l'offre")),
                ('configuration_type', models.CharField(help_text='Type de configuration \xe0 utiliser avec cette offre', max_length=50, null=True, verbose_name='type de configuration', choices=[('', '')])),
                ('billing_period', models.IntegerField(default=1, help_text='en mois', verbose_name='p\xe9riode de facturation')),
                ('period_fees', models.DecimalField(help_text='en \\u20ac', verbose_name='montant par p\xe9riode de facturation', max_digits=5, decimal_places=2)),
                ('initial_fees', models.DecimalField(help_text='en \\u20ac', verbose_name='frais de mise en service', max_digits=5, decimal_places=2)),
            ],
            options={
                'verbose_name': 'offre',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OfferSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subscription_date', models.DateField(default=datetime.date.today, verbose_name="date de souscription \xe0 l'offre")),
                ('resign_date', models.DateField(null=True, verbose_name='date de r\xe9siliation', blank=True)),
                ('commitment', models.IntegerField(default=0, help_text='en mois', verbose_name="p\xe9riode d'engagement")),
                ('member', models.ForeignKey(verbose_name='membre', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
                ('offer', models.ForeignKey(verbose_name='offre', to='offers.Offer', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'abonnement',
            },
            bases=(models.Model,),
        ),
    ]
