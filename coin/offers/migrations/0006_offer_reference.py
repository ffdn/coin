# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0005_auto_20150210_0923'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='reference',
            field=models.CharField(help_text='Identifiant a utiliser par exemple comme identifiant de virement', max_length=255, blank=True, verbose_name="référence de l'offre"),
            preserve_default=True,
        ),
    ]
