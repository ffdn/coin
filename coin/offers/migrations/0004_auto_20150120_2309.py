# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0003_offer_non_billable'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='non_billable',
            field=models.BooleanField(default=False, help_text="L'offre ne sera pas factur\xe9e par la commande charge_members", verbose_name="n'est pas facturable"),
            preserve_default=True,
        ),
    ]
