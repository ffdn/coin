# -*- coding: utf-8 -*-

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0004_auto_20150120_2309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='billing_period',
            field=models.IntegerField(default=1, help_text='en mois', verbose_name='p\xe9riode de facturation', validators=[django.core.validators.MinValueValidator(1)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='offersubscription',
            name='commitment',
            field=models.IntegerField(default=0, help_text='en mois', verbose_name="p\xe9riode d'engagement", validators=[django.core.validators.MinValueValidator(0)]),
            preserve_default=True,
        ),
    ]
