# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0003_auto_20150203_1043'),
        ('offers', '0007_offersubscription_comments'),
    ]

    operations = [
        migrations.CreateModel(
            name='OfferIPPool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.IntegerField()),
                ('ippool', models.ForeignKey(to='resources.IPPool', on_delete=models.CASCADE)),
                ('offer', models.ForeignKey(to='offers.Offer', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['priority'],
            },
        ),
        migrations.AddField(
            model_name='offer',
            name='ippools',
            field=models.ManyToManyField(to='resources.IPPool', through='offers.OfferIPPool'),
        ),
    ]
