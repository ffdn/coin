# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0006_offer_reference'),
    ]

    operations = [
        migrations.AddField(
            model_name='offersubscription',
            name='comments',
            field=models.TextField(help_text="Commentaires libres (informations sp\xe9cifiques concernant l'abonnement)", verbose_name='commentaires', blank=True),
        ),
    ]
