# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0002_auto_20141009_2223'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='non_billable',
            field=models.BooleanField(default=False, help_text="L'offre ne sera pas factur\xe9 par la commande charge_members", verbose_name="n'est pas facturable"),
            preserve_default=True,
        ),
    ]
