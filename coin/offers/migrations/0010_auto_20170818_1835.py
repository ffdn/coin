# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0009_auto_20170818_1529'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='offerippool',
            options={'ordering': ['-to_assign'], 'verbose_name': "pool d'IP", 'verbose_name_plural': "pools d'IP"},
        ),
        migrations.RenameField(
            model_name='offer',
            old_name='ippools',
            new_name='ip_pools',
        ),
        migrations.RenameField(
            model_name='offerippool',
            old_name='ippool',
            new_name='ip_pool',
        ),
    ]
