# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0008_auto_20170818_1507'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='offerippool',
            options={'ordering': ['to_assign'], 'verbose_name': "pool d'IP", 'verbose_name_plural': "pools d'IP"},
        ),
        migrations.RemoveField(
            model_name='offerippool',
            name='priority',
        ),
        migrations.AddField(
            model_name='offerippool',
            name='to_assign',
            field=models.BooleanField(default=False, verbose_name='assignation par d\xe9faut'),
        ),
        migrations.AlterField(
            model_name='offerippool',
            name='ippool',
            field=models.ForeignKey(verbose_name="pool d'IP", to='resources.IPPool', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='offerippool',
            name='offer',
            field=models.ForeignKey(verbose_name='offre', to='offers.Offer', on_delete=models.CASCADE),
        ),
    ]
