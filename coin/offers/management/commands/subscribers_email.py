# -*- coding: utf-8 -*-

import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from coin.offers.models import OfferSubscription


class Command(BaseCommand):
    help = 'Returns the email addresses of all subscribers, in a format suitable for bulk importing in Sympa'

    def handle(self, *args, **options):
        emails = [s.member.email for s in OfferSubscription.objects.filter(Q(resign_date__gt=datetime.date.today) | Q(resign_date__isnull=True))]
        # Use a set to ensure uniqueness
        for email in set(emails):
            self.stdout.write(email)
