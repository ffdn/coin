# -*- coding: utf-8 -*-

import datetime

from polymorphic.models import PolymorphicModel

from django.conf import settings
from django.db import models
from django.db.models import Count, Q
from django.core.validators import MinValueValidator
from django.contrib.contenttypes.models import ContentType

from coin.utils import get_descendant_classes, send_templated_email
from coin.resources.models import IPPool


class OfferManager(models.Manager):
    def manageable_by(self, user):
        """" Renvoie la liste des offres dont l'utilisateur est autorisé à
        voir les membres et les abonnements dans l'interface d'administration.
        """
        from coin.members.models import RowLevelPermission
        # toutes les permissions appliquées à cet utilisateur
        # (liste de chaines de caractères)
        perms = user.get_all_permissions()
        allowedcodenames = [ s.split('offers.', 1)[1] for s in perms if s.startswith('offers.')]
        # parmi toutes les RowLevelPermission, celles qui sont relatives à des OfferSubscription et qui sont dans allowedcodenames
        rowperms = RowLevelPermission.objects.filter(content_type=ContentType.objects.get_for_model(OfferSubscription), codename__in=allowedcodenames)
        # toutes les Offers pour lesquelles il existe une RowLevelpermission correspondante dans rowperms
        return super().filter(rowlevelpermission__in=rowperms).distinct()

class Offer(models.Model):
    """Description of an offer available to subscribers.

    Implementation notes:
    configuration_type store the model name of the configuration backend
    (ex VPNConfiguration).
    The choices list is dynamically generated at start in the __init__
    """

    name = models.CharField(max_length=255, blank=False, null=False,
                            verbose_name="nom de l'offre")
    reference = models.CharField(max_length=255, blank=True,
                                    verbose_name="référence de l'offre",
                                    help_text="Identifiant a utiliser par exemple comme identifiant de virement")
    configuration_type = models.CharField(max_length=50,
                            blank=True,
                            verbose_name='type de configuration',
                            help_text="Type de configuration à utiliser avec cette offre")
    billing_period = models.IntegerField(blank=False, null=False, default=1,
                                         verbose_name='période de facturation',
                                         help_text='en mois',
                                         validators=[MinValueValidator(1)])
    period_fees = models.DecimalField(max_digits=5, decimal_places=2,
                                      blank=False, null=False,
                                      verbose_name='montant par période de '
                                                   'facturation',
                                      help_text='en €')
    initial_fees = models.DecimalField(max_digits=5, decimal_places=2,
                                      blank=False, null=False,
                                      verbose_name='frais de mise en service',
                                      help_text='en €')
    non_billable = models.BooleanField(default=False,
                                       verbose_name='n\'est pas facturable',
                                       help_text='L\'offre ne sera pas facturée par la commande charge_members')
    requestable = models.BooleanField(default=False,
                                      verbose_name='Permettre aux membres de créer des demandes pour cette offre depuis leur espace')

    infos = models.TextField(blank=True,
                             verbose_name='infos complémentaires',
                             help_text="Informations complémentaires pour les membres, affichées dans leur espace")

    ip_pools = models.ManyToManyField(IPPool, related_name='offers', through='OfferIPPool')

    objects = OfferManager()

    def get_configuration_type_display(self):
        return Offer._get_offer_type(self.configuration_type)
    get_configuration_type_display.short_description = 'type de configuration'

    @staticmethod
    def _get_offer_type(configuration_type):
        """
        This translates for example "VPNConfiguration" into "VPN" ...
        """
        from coin.configuration.models import Configuration
        for subconfig_class in get_descendant_classes(Configuration):
            if subconfig_class.__name__ == configuration_type:
                return subconfig_class._meta.verbose_name
        return configuration_type

    @staticmethod
    def _get_configuration_type(offer_type):
        """
        This translates for example "VPN" into "VPNConfiguration" ...
        """
        from coin.configuration.models import Configuration
        for subconfig_class in get_descendant_classes(Configuration):
            if subconfig_class._meta.verbose_name == offer_type:
                return subconfig_class.__name__
        return offer_type

    @property
    def configuration_class(self):
        """
        This translate an offer into its corresponding configuration class (e.g. VPNConfiguration)
        """
        from coin.configuration.models import Configuration
        for subconfig_class in get_descendant_classes(Configuration):
            if subconfig_class.__name__ == self.configuration_type:
                return subconfig_class
        return None


    @property
    def subscriptionrequest_class(self):
        """
        This translate an offer into its corresponding subscription request class (e.g. VPNSubscriptionRequest)
        """
        conf_class = self.configuration_class
        for subconfig_class in get_descendant_classes(OfferSubscriptionRequest):
            if subconfig_class.configuration_class == conf_class:
                return subconfig_class
        return None


    def display_price(self):
        """Displays the price of an offer in a human-readable manner
        (for instance "30€ / month")
        """
        if int(self.period_fees) == self.period_fees:
            fee = int(self.period_fees)
        else:
            fee = self.period_fees
        if self.billing_period == 1:
            period = ""
        else:
            period = self.billing_period
        return "{period_fee}€ / {billing_period} mois".format(
            period_fee=fee,
            billing_period=period)

    def __str__(self):
        return '{name} - {price}'.format(name=self.name,
                                         price=self.display_price())

    class Meta:
        verbose_name = 'offre'


class OfferIPPool(models.Model):
    offer = models.ForeignKey(Offer,
                              verbose_name='offre', related_name='offer_ip_pools', on_delete=models.CASCADE)
    ip_pool = models.ForeignKey(IPPool,
                                verbose_name='pool d\'IP', related_name='offer_ip_pools', on_delete=models.CASCADE)
    to_assign = models.BooleanField(default=False,
                                    verbose_name='utiliser pour les IP d\'endpoints',
                                    help_text='Si vous cocher cette case, COIN utilisera ce pool d\'IP'
                                              ' afin de définir l\'IP d\'endpoint')
    def __str__(self):
        return '{ip_pool} - {offer}'.format(ip_pool=self.ip_pool,
                                            offer=self.offer)

    class Meta:
        verbose_name = 'pool d\'IP'
        verbose_name_plural = "pools d'IP"
        ordering = ['-to_assign']


class OfferSubscriptionQuerySet(models.QuerySet):
    def running(self, at_date=None):
        """ Only the running contracts at a given date.

        Running mean already started and not stopped yet
        """

        if at_date is None:
            at_date = datetime.date.today()

        return self.filter(Q(subscription_date__lte=at_date) &
                           (Q(resign_date__gt=at_date) |
                            Q(resign_date__isnull=True)))

    def offer_summary(self):
        """ Agregates as a count of subscriptions per offer
        """
        return self.values('offer__name', 'offer__reference').annotate(
            num_subscriptions=Count('offer'))


class OfferSubscription(models.Model):
    """Only contains administrative details about a subscription, not
    technical.  Nothing here should end up into the LDAP backend.

    Implementation notes: the Configuration model (which actually implementing the backend
    (technical configuration for the technology)) relate to this class
    with a OneToOneField
    """
    subscription_date = models.DateField(
        null=False,
        blank=False,
        default=datetime.date.today,
        verbose_name="date de souscription à l'offre")
    # TODO: for data retention, prevent deletion of a subscription object
    # while the resign date is recent enough (e.g. one year in France).
    resign_date = models.DateField(
        null=True,
        blank=True,
        verbose_name='date de résiliation')
    # TODO: move this to offers?
    commitment = models.IntegerField(blank=False, null=False,
                                     verbose_name="période d'engagement",
                                     help_text='en mois',
                                     validators=[MinValueValidator(0)],
                                     default=0)
    comments = models.TextField(blank=True, verbose_name='commentaires',
                                help_text="Commentaires libres (informations"
                                " spécifiques concernant l'abonnement)")
    member = models.ForeignKey('members.Member', verbose_name='membre', on_delete=models.CASCADE)
    offer = models.ForeignKey('Offer', verbose_name='offre', on_delete=models.CASCADE)

    objects = OfferSubscriptionQuerySet().as_manager()

    def get_subscription_reference(self):
        return settings.SUBSCRIPTION_REFERENCE.format(subscription=self)
    get_subscription_reference.short_description = 'Référence'

    def create_config_if_it_does_not_exists_yet(self):
        if not hasattr(self, 'configuration'):
            config_class = self.offer.configuration_class
            if config_class is not None:
                config = config_class.objects.create(offersubscription=self)
                config.offersubscription = self
                config.save()
                return True

        return False

    def __str__(self):
        return '%s - %s - %s' % (self.member, self.offer.name,
                                       self.subscription_date)

    def bulk_related_objects(self, objs, *args, **kwargs):
        # Fix delete screen. Workaround for https://github.com/chrisglass/django_polymorphic/issues/34
        return super().bulk_related_objects(objs, *args, **kwargs).non_polymorphic()

    class Meta:
        verbose_name = 'abonnement'


def count_active_subscriptions():
    today = datetime.date.today()
    query = Q(subscription_date__lte=today) & (Q(resign_date__isnull=True) | Q(resign_date__gte=today))
    return OfferSubscription.objects.filter(query).count()


class Request(PolymorphicModel):
    """
    Corresponds to request made by a member, for instance a request to create a
    new VPN subscription, of a request to reinstall a VPS
    """

    member = models.ForeignKey('members.Member', verbose_name='membre', on_delete=models.CASCADE)

    state = models.CharField(max_length=14,
                             choices=[('pending', 'En attente'),
                                      ('accepted', 'Accepté'),
                                      ('refused', 'Refusé')],
                             default="pending",
                             verbose_name="status")

    request_date = models.DateTimeField(null=False,
                                        blank=False,
                                        default=datetime.datetime.now,
                                        verbose_name="date de la demande")

    resolution_date = models.DateTimeField(null=True,
                                           blank=True,
                                           verbose_name='date de résolution')

    member_comments = models.TextField(blank=True,
                                       verbose_name='commentaires du membre',
                                       help_text="Commentaires libres")

    admin_comments = models.TextField(blank=True,
                                      verbose_name='commentaires de l\'équipe bénévole',
                                      help_text="Commentaires de l\'équipe bénévole")

    def accept(self):
        self.state = "accepted"
        self.resolution_date = datetime.datetime.now()
        self.save()

    def refuse(self):
        self.state = "refused"
        self.resolution_date = datetime.datetime.now()
        self.save()

    def __str__(self):
        return 'Demande de %s' % self.member

    def bulk_related_objects(self, objs, *args, **kwargs):
        # Fix delete screen. Workaround for https://github.com/chrisglass/django_polymorphic/issues/34
        return super().bulk_related_objects(objs, *args, **kwargs).non_polymorphic()

    class Meta:
        verbose_name = 'demande'


class OfferSubscriptionRequest(Request):

    offer = models.ForeignKey('Offer', verbose_name='offre', on_delete=models.CASCADE)

    offersubscription = models.OneToOneField(OfferSubscription,
                                             blank=True, null=True, on_delete=models.CASCADE,
                                             related_name='subscriptionrequest',
                                             verbose_name='abonnement créé')

    offer_type = None

    def accept(self):

        self.offersubscription = OfferSubscription.objects.create(offer=self.offer,
                                                             member=self.member,
                                                             comments=self.admin_comments)
        self.offersubscription.save()

        super().accept()

        self.offersubscription.create_config_if_it_does_not_exists_yet()

        # If there are some provisioning_infos to transmit to the created configuration
        config = self.offersubscription.configuration
        for info in config.provisioning_infos:
            setattr(config, info, getattr(self, info))
        config.save()

        send_templated_email(
            to=self.member.email,
            subject_template='offers/emails/subscription_request_accepted_subject.txt',
            body_template='offers/emails/subscription_request_accepted.txt',
            context={ 'request': self })

    def refuse(self):

        super().refuse()

        send_templated_email(
            to=self.member.email,
            subject_template='offers/emails/subscription_request_refused_subject.txt',
            body_template='offers/emails/subscription_request_refused.txt',
            context={ 'request': self })

    @property
    def configuration_type(self):
        return Offer._get_configuration_type(self.offer_type)

    @staticmethod
    def requestable_offers(offer_type=None):

        if offer_type is None:
            return Offer.objects.filter(requestable=True).order_by('name')
        else:
            return Offer.objects.filter(configuration_type=Offer._get_configuration_type(offer_type), requestable=True).order_by('name')

    @staticmethod
    def requestable_offertypes():
        return sorted({Offer._get_offer_type(o.configuration_type) for o in Offer.objects.filter(requestable=True)})

    def __str__(self):
        return 'Demande de %s pour un %s' % (self.member, self.offer.name)

    class Meta:
        verbose_name = 'demande de souscription'
        verbose_name_plural = 'demandes de souscription'
