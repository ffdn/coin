# -*- coding: utf-8 -*-

from django.contrib import admin, messages
from django.db.models import Q
from django.conf.urls import url
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.safestring import mark_safe
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin

from coin.members.models import Member
from coin.offers.models import Offer, OfferIPPool, OfferSubscription, OfferSubscriptionRequest
from coin.offers.offersubscription_filter import\
            OfferSubscriptionTerminationFilter,\
            OfferSubscriptionCommitmentFilter
from coin.offers.forms import OfferAdminForm
from coin.configuration.admin import ChildConfigurationAdmin
from coin.configuration.models import Configuration
from coin.resources.models import IPSubnet

class OfferIPPoolAdmin(admin.TabularInline):
    model = OfferIPPool
    extra = 1


class OfferAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_configuration_type_display', 'reference', 'billing_period', 'period_fees')
    list_filter = ('configuration_type',)
    search_fields = ['name']
    form = OfferAdminForm
    inlines = (OfferIPPoolAdmin,)
    # def get_readonly_fields(self, request, obj=None):
    #     if obj:
    #         return ['backend',]
    #     else:
    #         return []


class OfferSubscriptionAdmin(admin.ModelAdmin):
    list_display = ('get_subscription_reference', 'member', 'offer',
                    'subscription_date', 'resign_date')
    list_display_links = ('get_subscription_reference',)
    list_filter = ( OfferSubscriptionTerminationFilter,
                    'offer')
    search_fields = ['member__first_name', 'member__last_name', 'member__email',
                     'member__nickname']

    fields = (
                'member',
                'offer',
                'subscription_date',
                'commitment',
                'resign_date',
                'comments'
             )

    # Si pas super user on restreint les membres et offres accessibles
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if not request.user.is_superuser:
            if db_field.name == "member":
                kwargs["queryset"] = Member.objects.manageable_by(request.user)
            if db_field.name == "offer":
                kwargs["queryset"] = Offer.objects.filter(id__in=[p.id for p in Offer.objects.manageable_by(request.user)])
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    # Si pas super user on restreint la liste des offres que l'on peut voir
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        else:
            offers = Offer.objects.manageable_by(request.user)
            return qs.filter(offer__in=offers)

    def get_inline_instances(self, request, obj=None):
        """
        Si en edition, alors affiche en inline le formulaire de la configuration
        correspondant à l'offre choisie
        """
        if obj is not None:
            for item in ChildConfigurationAdmin.__subclasses__():
                if (item.base_model.__name__ == obj.offer.configuration_type):
                    return [item.inline(self.model, self.admin_site)]
        return []

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)

        # On auto-complète le modèle que si on n'est pas en train de créer un
        # abonnement via une configuration
        if request.GET.get('_popup', 0):
            return

        obj.create_config_if_it_does_not_exists_yet()


class ParentOfferSubscriptionRequestAdmin(PolymorphicParentModelAdmin):

    base_model = OfferSubscriptionRequest

    list_display = ('__str__', 'enhanced_state', 'request_date')

    list_display_links = ('__str__',)

    list_filter = ("state",)

    polymorphic_list = True

    def get_child_models(self):
        """
        Renvoi la liste des modèles enfants de ChildOfferSubscriptionRequestAdmin
        ex :(VPNSubscriptionRequest, AccountSubscriptionRequest, ...)
        """
        return tuple(x.base_model for x in ChildOfferSubscriptionRequestAdmin.__subclasses__())

    def enhanced_state(self, obj):

        if obj.state == "pending":
            icon = ' <i class="fa fa-question-circle"></i>'
            color = "goldenrod"
        elif obj.state == "accepted":
            icon = ' <i class="fa fa-check-circle"></i>'
            color = "green"
        elif obj.state == "refused":
            icon = ' <i class="fa fa-times"></i>'
            color = "crimson"
        else:
            # Should not happen
            icon = ""

        return mark_safe("<span style='color: {color}'>{icon} {state}</span>".format(color=color, icon=icon, state=obj.get_state_display()))

    enhanced_state.short_description = "Status"

    def get_urls(self):
        """
        Custom admin urls
        """
        urls = super().get_urls()
        my_urls = [
            url(r'^process/(?P<id>.+)/(?P<whatdo>[a-z_]+)$',
                self.admin_site.admin_view(self.view_process),
                name='offersubscriptionrequest_process')
        ]
        return my_urls + urls

    def view_process(self, request, id, whatdo):

        if not request.user.is_superuser:
            messages.error(
                request, 'Vous n\'avez pas l\'autorisation de gérer les demandes de service.')
        elif whatdo not in ["accept", "refuse"]:
            messages.error(request, "This is not a valid process action")
        else:
            offersubscriptionrequest = get_object_or_404(OfferSubscriptionRequest, pk=id)

            if offersubscriptionrequest.state != "pending":
                messages.error(request, "Cette demande a déjà été acceptée ou refusée.")
            else:
                try:
                    if whatdo == "accept":
                        offersubscriptionrequest.accept()
                    elif whatdo == "refuse":
                        offersubscriptionrequest.refuse()
                except Exception as e:
                    messages.error(request, "Erreur en tentant de processer l'action %s pour %s : %s" % (whatdo, offersubscriptionrequest, str(e)))

        return HttpResponseRedirect(reverse('admin:offers_offersubscriptionrequest_change',
                                            args=(id,)))


class ChildOfferSubscriptionRequestAdmin(PolymorphicChildModelAdmin):

    change_form_template = "admin/offersubscriptionrequest/change_form.html"

    def get_fieldsets(self, request, obj=None):
        return [
                ('', {'fields': (
                    'member',
                    'offer',
                    ('state', 'request_date', 'resolution_date'),
                    'member_comments',
                    'admin_comments')}),
                ('Abonnement créé', {
                    'fields': ('offersubscription',),
                    'description': 'Un abonnement sera automatiquement créé une fois la demande validée'})
                ]

    def get_readonly_fields(self, request, obj=None):
        readonly = ["state", "request_date", "resolution_date", "offersubscription"]
        if obj:
            readonly += ["member_comments"]
            if obj.state != "pending":
                readonly += ["member", "offer"]
        return readonly

    #
    # This allows to only display VPS-related *requestable* offers
    # when creating a VPSSubscriptionRequests (for example)
    #
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "offer":
            kwargs["queryset"] = OfferSubscriptionRequest.requestable_offers(offer_type=self.base_model.offer_type)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Offer, OfferAdmin)
admin.site.register(OfferSubscription, OfferSubscriptionAdmin)
admin.site.register(OfferSubscriptionRequest, ParentOfferSubscriptionRequestAdmin)
