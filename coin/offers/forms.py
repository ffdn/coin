# -*- coding: utf-8 -*-
from django import forms
from django.forms.widgets import Select
from django.urls import reverse
from django.conf import settings

from coin.offers.models import Offer, OfferSubscriptionRequest
from coin.configuration.models import Configuration
from coin.utils import send_templated_email

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from crispy_forms.bootstrap import StrictButton

class ConfigurationTypeSelect(forms.Select):

    def render(self, *args, **kwargs):
        # We do this so that get_configurations_choices_list is called during
        # runtime once we're sure all Configuration subclasses are loaded...
        self.choices = [('', '---------'),] + list(Configuration.get_configurations_choices_list())
        return super().render(*args, **kwargs)

class OfferAdminForm(forms.ModelForm):
    class Meta:
        model = Offer
        widgets = {
            'configuration_type': ConfigurationTypeSelect()
        }
        exclude = ('', )

class OfferSubscriptionRequestStep1Form(forms.Form):

    offer_type = forms.ChoiceField(label="Type d'abonnement", widget=forms.RadioSelect, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['offer_type'].choices = tuple((type, type) for type in OfferSubscriptionRequest.requestable_offertypes())

    @property
    def helper(self):
        helper = FormHelper()

        helper.layout = Layout(
            *(list(self.fields.keys()) + [
                Div(
                    StrictButton('Continuer', css_class="btn-primary", type="submit"),
                    css_class="text-center"
                )
            ])
        )

        return helper

class OfferSubscriptionRequestStep2Form(forms.Form):

    offer = forms.ModelChoiceField(queryset=None, label="Offre", required=True)
    member_comments = forms.CharField(label="Commentaires pour l'équipe de bénévole", max_length=1000, widget=forms.Textarea(attrs={'style': 'height: 10em;'}), required=False)
    agree_tos = forms.BooleanField(label="Vous acceptez les conditions d'abonnement et d'utilisation de cette offre", required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Put the comment + agree tos checkbox at the end of the form
        member_comments = self.fields.pop("member_comments")
        agree_tos = self.fields.pop("agree_tos")
        self.fields["member_comments"] = member_comments
        self.fields["agree_tos"] = agree_tos

    def set_offer_choices(self, offer_type):
        self.offer_type = offer_type
        self.fields['offer'].queryset = OfferSubscriptionRequest.requestable_offers(offer_type=offer_type)

    def create_offersubscriptionrequest(self, request):

        request_class = self.cleaned_data["offer"].subscriptionrequest_class
        subscription_request = request_class.objects.create(offer=self.cleaned_data["offer"],
                                               member=request.user,
                                               member_comments=self.cleaned_data["member_comments"])
        subscription_request.save()

        if settings.REQUESTS_NOTIFICATION_EMAILS:

            relative_link = reverse('admin:offers_offersubscriptionrequest_change', args=[subscription_request.id])
            admin_link = request.build_absolute_uri(relative_link)

            send_templated_email(
                to=settings.REQUESTS_NOTIFICATION_EMAILS,
                subject_template='offers/emails/subscription_request_new_subject.txt',
                body_template='offers/emails/subscription_request_new.txt',
                context={'request': subscription_request, 'admin_link': admin_link},
            )

        return subscription_request

    @property
    def helper(self):
        helper = FormHelper()

        helper.form_action = reverse('members:subscriptionrequest_step2', args=[self.offer_type])

        helper.form_class = 'form-horizontal'
        helper.label_class = 'col-4'
        helper.field_class = 'col-8'
        helper.layout = Layout(
            *(list(self.fields.keys()) + [
                Div(
                    StrictButton('<i class="fa fa-paper-plane" aria-hidden="true"></i> Envoyer la demande', css_class="btn-success", type="submit"),
                    css_class="text-center"
                )
            ])
        )

        return helper
