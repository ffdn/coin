# -*- coding: utf-8 -*-

from django.apps import AppConfig


class OffersConfig(AppConfig):
    name = 'coin.offers'
    verbose_name = 'Offres et Abonnements'
    admin_menu_entry = {
        "id": "services",
        "icon": "shopping-cart",
        "title": "Services",
        "models": [
            ("Offres", "offers/offer"),
            ("Demandes", "offers/offersubscriptionrequest"),
            ("Abonnements", "offers/offersubscription"),
        ]
    }
