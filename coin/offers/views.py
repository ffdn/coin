# -*- coding: utf-8 -*-

import datetime
import json

from django.db.models import Q, Count
from django.views.generic.base import RedirectView
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.http import JsonResponse, HttpResponseServerError, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from coin.offers.models import Offer, OfferSubscription, OfferSubscriptionRequest
from coin.offers.forms import OfferSubscriptionRequestStep1Form, OfferSubscriptionRequestStep2Form

class ConfigurationRedirectView(RedirectView):
    """Redirects to the appropriate view for the configuration backend of the
    specified subscription."""

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        subscription = get_object_or_404(OfferSubscription, pk=self.kwargs['id'],
                                         member=self.request.user)
        return reverse(subscription.configuration.url_namespace + ':' + subscription.configuration.backend_name,
                       args=[subscription.configuration.pk])


# @cache_control(max_age=7200)
def subscription_count_json(request):
    output = []

    # Get date form url, or set default
    date = request.GET.get('date', datetime.date.today())

    # Validate date type
    if not isinstance(date, datetime.date):
        try:
            datetime.datetime.strptime(date, '%Y-%m-%d')
        except ValueError as TypeError:
            return HttpResponseServerError("Incorrect date format, should be YYYY-MM-DD")

    # Get current offer subscription
    offersubscriptions = list(OfferSubscription.objects.running(date).offer_summary())
    for offersub in offersubscriptions:
        output.append({
            'reference' : offersub['offer__reference'],
            'name' : offersub['offer__name'],
            'subscriptions_count' : offersub['num_subscriptions']
        })

    # Return JSON
    return JsonResponse(output, safe=False)


@login_required
def offersubscriptionrequest_step1(request):
    if request.method == 'POST':
        form = OfferSubscriptionRequestStep1Form(request.POST)
        if form.is_valid():
            return HttpResponseRedirect(reverse('members:subscriptionrequest_step2', args=[request.POST["offer_type"]]))
    else:
        form = OfferSubscriptionRequestStep1Form()

    return render(request, 'members/offersubscriptionrequest/form.html', {'form': form})


@login_required
def offersubscriptionrequest_step2(request, offer_type):
    if offer_type not in OfferSubscriptionRequest.requestable_offertypes():
        return HttpResponseServerError("Ce type d'offre n'est pas correct.")

    # Try to get the form class specific to this offer type.
    # If we do not find such a class, fallback to the regular "OfferSubscriptionRequestStep2Form"
    FormsPerOfferType = {x.offer_type:x for x in OfferSubscriptionRequestStep2Form.__subclasses__()}
    FormForThisOfferType = FormsPerOfferType.get(offer_type, OfferSubscriptionRequestStep2Form)

    if request.method == 'POST':
        form = FormForThisOfferType(request.POST)
        form.set_offer_choices(offer_type)
        if form.is_valid():
            form.create_offersubscriptionrequest(request)
            return HttpResponseRedirect(reverse('members:subscriptionrequest_step3'))
    else:
        form = FormForThisOfferType()
        form.set_offer_choices(offer_type)

    return render(request, 'members/offersubscriptionrequest/form.html', {'form': form})

@login_required
def offersubscriptionrequest_step3(request):
    return render(request, 'members/offersubscriptionrequest/form.html', {'success': True})
