# -*- coding: utf-8 -*-

from django.conf.urls import url
from coin.offers.views import ConfigurationRedirectView, subscription_count_json

app_name = "coin.subscriptions"

urlpatterns = [
    #'',
    # Redirect to the appropriate configuration backend.
    url(r'^configuration/(?P<id>.+)$', ConfigurationRedirectView.as_view(), name="configuration-redirect"),
    url(r'^api/v1/count$', subscription_count_json),
]
