import os
import shutil
import tempfile

from django.conf import settings
from django.test import TestCase, override_settings

from coin.hooks import HookManager

root_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
custom_hooks_dir = os.path.join(root_dir, "custom_hooks")

@override_settings()
class SubscriptionTestCase(TestCase):
    def setUp(self):

        self.tmpdir = tempfile.mkdtemp()

        settings.HOOKS = {"foo": {"CREATE_FILE": {"type": "bash",
                                                  "command": "touch {dir}/{file}"},
                                  "CREATE_REMOTE_FILE": {"type": "bash",
                                                         "remote": "user@foo.com",
                                                         "command": "touch {dir}/{file}"},
                                  "UPDATE_FILE": {"type": "bash",
                                                  "command": "tee {dir}/{file}",
                                                  "stdin": "{content}"},
                                  "LOAD_FILE": {"type": "bash",
                                                "command": "cat {dir}/{file}",
                                                "parse_output": "yaml"},
                                  "PROVISION": {"type": "bash",
                                                "remote": "user@machine",
                                                "command": "provision_foo.sh --name {name} --ipv4 {ipv4}",
                                                "stdin": "{password}"}
                                  },
                          "bar": {"PROVISION": {"type": "python",
                                                "module": "bar",
                                                "function": "provision"},
                                  "GET_STATE": {"type": "python",
                                                "module": "bar",
                                                "function": "state"}
                                  }
                          }

        if not os.path.exists(custom_hooks_dir):
            os.makedirs(custom_hooks_dir)
        os.system("touch %s/__init__.py" % custom_hooks_dir)
        open("%s/bar.py" % custom_hooks_dir, "w").write("""
import os

def provision(**kwargs):
    dir = kwargs["dir"]
    file = kwargs["file"]
    open("%s/%s" % (dir, file), "w").write("running")

def state(**kwargs):
    dir = kwargs["dir"]
    file = kwargs["file"]
    path = "%s/%s" % (dir, file)
    provisioned = os.path.exists(path)
    state = open(path).read().strip() if provisioned else "N/A"
    return {"provisioned": provisioned,
            "state": state }
""")

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        os.remove("%s/bar.py" % custom_hooks_dir)

    def test_hook_is_defined(self):

        self.assertTrue(HookManager.is_defined("foo", "CREATE_FILE"))
        self.assertTrue(HookManager.is_defined("foo", "UPDATE_FILE"))
        self.assertTrue(HookManager.is_defined("foo", "LOAD_FILE"))
        self.assertFalse(HookManager.is_defined("foo", "WAT"))

    def test_hook_format_hook_bash(self):

        hook_create_file = settings.HOOKS["foo"]["CREATE_FILE"]
        hook_update_file = settings.HOOKS["foo"]["UPDATE_FILE"]
        hook_create_remote_file = settings.HOOKS["foo"]["CREATE_REMOTE_FILE"]

        self.assertEqual(HookManager._format_hook_bash(hook_create_file, dir=self.tmpdir, file="foo"),
                         ["touch", self.tmpdir + "/foo"])
        self.assertEqual(HookManager._format_hook_bash(hook_update_file, dir=self.tmpdir, file="bar"),
                         ["tee", self.tmpdir + "/bar"])
        self.assertEqual(HookManager._format_hook_bash(hook_create_remote_file, dir=self.tmpdir, file="wat"),
                         ["timeout", "300", "ssh", "user@foo.com", "touch %s/wat" % self.tmpdir])

    def test_hook_format_hook_bash_injection_attempt(self):

        hook_create_file = settings.HOOKS["foo"]["CREATE_FILE"]
        hook_create_remote_file = settings.HOOKS["foo"]["CREATE_REMOTE_FILE"]

        self.assertEqual(HookManager._format_hook_bash(hook_create_file, dir="/foo", file="wat; rm -rf /"),
                         ["touch", "/foo/wat; rm -rf /"])
                                  # ------------------           # noqa
                                  #  ^ one single arg for touch  # noqa
        self.assertEqual(HookManager._format_hook_bash(hook_create_remote_file, dir="/foo", file="wat; rm -rf /"),
                         ["timeout", "300", "ssh", "user@foo.com", "touch '/foo/wat; rm -rf /'"])
                                                           # ---------------              # noqa
                                                           #  ^ one single arg for touch  # noqa
        self.assertEqual(HookManager._format_hook_bash(hook_create_remote_file, dir="/foo", file="wat\"; rm -rf /"),
                         ["timeout", "300", "ssh", "user@foo.com", "touch '/foo/wat\"; rm -rf /'"])
                                                           # ----------------------       # noqa
                                                           #  ^ one single arg for touch  # noqa
        self.assertEqual(HookManager._format_hook_bash(hook_create_remote_file, dir="/foo", file="wat'; rm -rf /"),
                         ["timeout", "300", "ssh", "user@foo.com", "touch '/foo/wat'\"'\"'; rm -rf /'"])
                                                           # ---------------------------  # noqa
                                                           #  ^ one single arg for touch  # noqa

    def test_run_bash(self):

        self.assertEqual(HookManager.run("foo", "CREATE_FILE", dir=self.tmpdir, file="foo"),
                         (True, "", ""))
        self.assertTrue(os.path.exists(self.tmpdir + "/foo"))

        self.assertEqual(HookManager.run("foo", "UPDATE_FILE", content="yolo: swag", dir=self.tmpdir, file="bar"),
                         (True, "yolo: swag", ""))
        self.assertEqual(open(self.tmpdir + "/bar").read().strip(), "yolo: swag")

        self.assertEqual(HookManager.run("foo", "LOAD_FILE", dir=self.tmpdir, file="bar"),
                         (True, {"yolo": "swag"}, ""))

    def test_run_python(self):

        self.assertEqual(HookManager.run("bar", "PROVISION", dir=self.tmpdir, file="foo"),
                         (True, None, None))

        self.assertEqual(HookManager.run("bar", "GET_STATE", dir=self.tmpdir, file="foo"),
                         (True, {"provisioned": True, "state": "running"}, None))

        self.assertEqual(HookManager.run("bar", "GET_STATE", dir=self.tmpdir, file="pwet"),
                         (True, {"provisioned": False, "state": "N/A"}, None))
