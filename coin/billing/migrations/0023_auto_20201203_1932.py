# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0022_auto_20201203_1913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='bill_ptr',
            field=models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='billing.Bill', on_delete=models.CASCADE),
        ),
    ]
