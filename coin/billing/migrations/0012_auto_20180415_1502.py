# -*- coding: utf-8 -*-

from django.db import migrations, models
import datetime
import coin.billing.models
import django.db.models.deletion
import django.core.files.storage
from django.conf import settings


def migrate_invoices_to_bills(apps, schema_editor):
    from django.core.management.color import no_style

    Bill = apps.get_model('billing', 'Bill')
    Invoice = apps.get_model('billing', 'Invoice')

    for invoice in Invoice.objects.all():
        Bill.objects.create(
            id=invoice.id,
            member=invoice.member,
            status=invoice.status,
            date=invoice.date,
            pdf=invoice.pdf,
        )
    # When specifying id manually, sequence should then be reset, at least with postgresql
    # https://django.readthedocs.io/en/stable/ref/databases.html#manually-specifying-values-of-auto-incrementing-primary-keys
    # https://stackoverflow.com/a/50275895/1377500
    sequence_sql = schema_editor.connection.ops.sequence_reset_sql(
        no_style(),
        [Bill],
    )
    for sql in sequence_sql:
        schema_editor.execute(sql)


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('billing', '0011_auto_20180414_2250'),
    ]

    operations = [
        # 1/ Create Bill model
        migrations.CreateModel(
            name='Bill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default='open', max_length=50, verbose_name='statut', choices=[('open', '\xc0 payer'), ('closed', 'R\xe9gl\xe9e'), ('trouble', 'Litige')])),
                ('date', models.DateField(default=datetime.date.today, help_text='Cette date sera d\xe9finie \xe0 la date de validation dans le document final', null=True, verbose_name='date')),
                ('pdf', models.FileField(storage=django.core.files.storage.FileSystemStorage(location='/vagrant/apps/extra/coin2/smedia/'), upload_to=coin.billing.models.bill_pdf_filename, null=True, verbose_name='PDF', blank=True)),
                ('member', models.ForeignKey(related_name='bills', on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='membre')),
            ],
            options={
                'verbose_name': 'note',
            },
        ),
        # 2/ Create 1 Bill object per Invoice object, copying status, date and pdf, member
        migrations.RunPython(migrate_invoices_to_bills),
        # 3/ Create a link between Bill and Invoice
        migrations.RenameField(
            model_name='invoice',
            old_name='id',
            new_name='bill_ptr',
        ),
        migrations.AlterField(
            model_name='invoice',
            name='bill_ptr',
            field=models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, default=1, serialize=False, to='billing.Bill', on_delete=models.CASCADE),
        ),
        # 4/ Delete duplicate Invoice fields that are present on Bill : status, date, pdf, member

        migrations.RemoveField(model_name='invoice', name='status'),
        migrations.RemoveField(model_name='invoice', name='date'),
        migrations.RemoveField(model_name='invoice', name='pdf'),
        migrations.RemoveField(model_name='invoice', name='member'),

        migrations.AlterField(
            model_name='payment',
            name='invoice',
            field=models.ForeignKey(related_name='payments_old', verbose_name='facture associ\xe9e', blank=True, to='billing.Invoice', null=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='paymentallocation',
            name='invoice',
            field=models.ForeignKey(related_name='allocations', verbose_name='facture associ\xe9e', to='billing.Bill', on_delete=models.CASCADE),
        ),
        migrations.CreateModel(
            name='Donation',
            fields=[
                ('bill_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='billing.Bill', on_delete=models.CASCADE)),
                ('_amount', models.DecimalField(verbose_name='Montant', max_digits=8, decimal_places=2)),
            ],
            options={
                'verbose_name': 'don',
            },
            bases=('billing.bill',),
        ),
        migrations.CreateModel(
            name='TempMembershipFee',
            fields=[
                ('bill_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='billing.Bill', on_delete=models.CASCADE)),
                ('_amount', models.DecimalField(default=None, help_text='en \\u20ac', verbose_name='montant', max_digits=5, decimal_places=2)),
                ('start_date', models.DateField(verbose_name='date de d\xe9but de cotisation')),
                ('end_date', models.DateField(help_text='par d\xe9faut, la cotisation dure un an', verbose_name='date de fin de cotisation', blank=True)),
            ],
            options={
                'verbose_name': 'cotisation',
            },
            bases=('billing.bill',),
        ),
        migrations.RenameField(
            model_name='payment',
            old_name='invoice',
            new_name='bill',
        ),
        migrations.AlterField(
            model_name='payment',
            name='bill',
            field=models.ForeignKey(related_name='payments', verbose_name='facture associ\xe9e', blank=True, to='billing.Bill', null=True, on_delete=models.CASCADE),
        ),
    ]
