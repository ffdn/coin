# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0010_new_billing_system_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='amount',
            field=models.DecimalField(null=True, verbose_name='montant', max_digits=6, decimal_places=2),
        ),
    ]
