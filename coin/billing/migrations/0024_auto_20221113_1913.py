# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0023_auto_20201203_1932'),
    ]

    operations = [
        migrations.CreateModel(
            name='Refund',
            fields=[
                ('bill_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='billing.Bill', on_delete=models.CASCADE)),
                ('number', models.CharField(max_length=25, verbose_name='num\xe9ro')),
            ],
            options={
                'verbose_name': 'avoir',
            },
            bases=('billing.bill',),
        ),
        migrations.AlterField(
            model_name='bill',
            name='status',
            field=models.CharField(default='open', max_length=50, verbose_name='statut', choices=[('open', '\xc0 payer'), ('closed', 'R\xe9gl\xe9e'), ('trouble', 'Litige'), ('cancelled', 'Annul\xe9')]),
        ),
        migrations.AddField(
            model_name='refund',
            name='refunded_bill',
            field=models.ForeignKey(related_name='refund_bill', verbose_name='facture/note \xe0 annuler', to='billing.Bill', on_delete=models.CASCADE),
        ),
    ]
