# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0015_remove_payment_invoice'),
    ]

    operations = [
        migrations.RenameField(
            model_name='paymentallocation',
            old_name='invoice',
            new_name='bill',
        ),
    ]
