# -*- coding: utf-8 -*-

from django.db import migrations, models
import django.core.files.storage
import coin.billing.models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0017_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bill',
            name='pdf',
            field=models.FileField(storage=django.core.files.storage.FileSystemStorage(location='/var/www/adherents.arn-fai.net/smedia/'), upload_to=coin.billing.models.bill_pdf_filename, null=True, verbose_name='PDF', blank=True),
        ),
        migrations.AlterField(
            model_name='invoicedetail',
            name='amount',
            field=models.DecimalField(verbose_name='montant', max_digits=8, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='membershipfee',
            name='_amount',
            field=models.DecimalField(default=None, help_text='en \\u20ac', verbose_name='montant', max_digits=8, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='payment',
            name='amount',
            field=models.DecimalField(null=True, verbose_name='montant', max_digits=8, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='payment',
            name='payment_mean',
            field=models.CharField(default='transfer', max_length=100, null=True, verbose_name='moyen de paiement', choices=[('cash', 'Esp\xe8ces'), ('check', 'Ch\xe8que'), ('transfer', 'Virement'), ('creditnote', 'Avoir'), ('other', 'Autre')]),
        ),
        migrations.AlterField(
            model_name='paymentallocation',
            name='amount',
            field=models.DecimalField(null=True, verbose_name='montant', max_digits=8, decimal_places=2),
        ),
    ]
