# -*- coding: utf-8 -*-

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('billing', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoicedetail',
            name='offersubscription',
            field=models.ForeignKey(default=None, blank=True, to='offers.OfferSubscription', null=True, verbose_name='abonnement', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='invoice',
            name='member',
            field=models.ForeignKey(related_name='invoices', on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='membre'),
            preserve_default=True,
        ),
    ]
