# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0013_auto_20180415_0413'),
        # After MembershipFee model from members app has been deleted
        ('members', '0019_auto_20180415_1814'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='TempMembershipFee',
            new_name='MembershipFee',
        ),
    ]
