# -*- coding: utf-8 -*-

from django.db import migrations, models
import coin.utils


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0004_auto_20161230_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='date_due',
            field=models.DateField(default=coin.utils.end_of_month, help_text='Le d\xe9lai de paiement sera fix\xe9 \xe0 30 jours \xe0 la validation si laiss\xe9 vide', null=True, verbose_name="date d'\xe9ch\xe9ance de paiement", blank=True),
        ),
    ]
