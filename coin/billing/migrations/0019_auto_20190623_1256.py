# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0018_auto_20181118_2001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoicedetail',
            name='label',
            field=models.CharField(max_length=255),
        ),
    ]
