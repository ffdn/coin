# -*- coding: utf-8 -*-

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0010_new_billing_system_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoicedetail',
            name='offersubscription',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='offers.OfferSubscription', null=True, verbose_name='abonnement'),
        ),
    ]
