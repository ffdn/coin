# -*- coding: utf-8 -*-

from django.db import migrations, models

def fill_empty_fee_payment_date(apps, schema_editor):
    """
    If a MembershipFee lacks a payment date, we fill it with the start of the
    corresponding membership period, considering all members are very serious
    and pay exactly at the due date.
    """
    MembershipFee = apps.get_model('members', 'MembershipFee')
    for fee in MembershipFee.objects.filter(payment_date=None):
        fee.payment_date = fee.start_date
        fee.save()

def forwards(apps, schema_editor):

    Payment = apps.get_model('billing', 'Payment')
    MembershipFee = apps.get_model('members', 'MembershipFee')
    TempMembershipFee = apps.get_model('billing', 'TempMembershipFee')
    PaymentAllocation = apps.get_model('billing', 'PaymentAllocation')

    # Update balance for all members
    for fee in MembershipFee.objects.all():

        temp_fee = TempMembershipFee()
        temp_fee._amount = fee.amount
        temp_fee.start_date = fee.start_date
        temp_fee.end_date = fee.end_date
        temp_fee.status = 'closed'
        temp_fee.date = temp_fee.start_date
        temp_fee.member = fee.member
        temp_fee.save()

        payment = Payment()
        payment.member = fee.member
        payment.payment_mean = fee.payment_method
        payment.amount = fee.amount
        payment.date = fee.payment_date
        payment.label = fee.reference
        payment.bill = temp_fee
        payment.save()

        allocation = PaymentAllocation()
        allocation.invoice = temp_fee
        allocation.payment = payment
        allocation.amount = fee.amount
        allocation.save()


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0012_auto_20180415_1502'),
    ]

    operations = [
        migrations.RunPython(
            fill_empty_fee_payment_date,
            reverse_code=migrations.RunPython.noop,
        ),
        migrations.RunPython(forwards),
    ]
