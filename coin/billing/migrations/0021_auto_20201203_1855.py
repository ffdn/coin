# -*- coding: utf-8 -*-

from django.db import migrations, models
import django.core.files.storage
import coin.members.models
import coin.billing.models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0020_auto_20200717_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='_amount',
            field=models.DecimalField(default=coin.members.models.default_membership_fee, help_text='en \\u20ac', verbose_name='montant', max_digits=8, decimal_places=2),
        ),
    ]
