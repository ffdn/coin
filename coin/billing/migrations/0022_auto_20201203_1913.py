# -*- coding: utf-8 -*-

from django.db import migrations, models
import coin.billing.models
import coin.utils


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0021_auto_20201203_1855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bill',
            name='pdf',
            field=models.FileField(storage=coin.utils.PrivateFileStorage(), upload_to=coin.billing.models.bill_pdf_filename, null=True, verbose_name='PDF', blank=True),
        ),
    ]
