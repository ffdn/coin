# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0019_auto_20190623_1256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='_amount',
            field=models.DecimalField(default=15, help_text='en \\u20ac', verbose_name='montant', max_digits=8, decimal_places=2),
        ),
    ]
