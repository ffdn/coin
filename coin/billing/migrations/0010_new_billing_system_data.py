# -*- coding: utf-8 -*-

import sys

from django.db import migrations


def check_current_state(apps, schema_editor):

    Member = apps.get_model('members', 'Member')
    Invoice = apps.get_model('billing', 'Invoice')
    InvoiceDetail = apps.get_model('billing', 'InvoiceDetail')
    Payment = apps.get_model('billing', 'Payment')


    for invoice in Invoice.objects.all():

        invoice_name = invoice.__str__()

        related_payments = invoice.payments.all()

        total_related_payments = sum([p.amount for p in related_payments])

        if total_related_payments > invoice.amount:
            error = "For invoice, current sum of payment is higher than total of invoice. Please fix this before running this migration" % invoice_name
            raise AssertionError(error.encode('utf-8'))

        if total_related_payments != 0 and not invoice.validated:
            error = "Invoice %s is not validated but already has allocated payments. Please remove them before running this migration" % invoice_name
            raise AssertionError(error.encode('utf-8'))


def forwards(apps, schema_editor):

    Member = apps.get_model('members', 'Member')
    Invoice = apps.get_model('billing', 'Invoice')
    InvoiceDetail = apps.get_model('billing', 'InvoiceDetail')
    Payment = apps.get_model('billing', 'Payment')

    # Create allocation for all payment to their respective invoice
    for payment in Payment.objects.all():
        payment.member = payment.invoice.member
        payment.allocate_to_invoice(payment.invoice)

    # Update balance for all members
    for member in Member.objects.all():

        this_member_invoices = [i for i in member.invoices.filter(validated=True).order_by("date")]
        this_member_payments = [p for p in member.payments.order_by("date")]

        member.balance = compute_balance(this_member_invoices,
                                         this_member_payments)
        member.save()


def compute_balance(invoices, payments):

    active_payments = [p for p in payments if p.amount_not_allocated()    > 0]
    active_invoices = [i for i in invoices if i.amount_remaining_to_pay() > 0]

    s = 0
    s -= sum([i.amount_remaining_to_pay() for i in active_invoices])
    s += sum([p.amount_not_allocated()    for p in active_payments])

    return s



class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0009_new_billing_system_schema'),
        ('members', '0016_merge'),
    ]

    operations = [
        migrations.RunPython(check_current_state),
        migrations.RunPython(forwards),
    ]
