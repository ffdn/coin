import re

import unidecode

from coin.billing.models import Payment
from coin.members.models import Member

# Parser / import / matcher configuration

# If the label of the payment contains one of these, the payment won't be
# matched to a member when importing it.
KEYWORDS_TO_NOTMATCH = ["REM CHQ"]

# The default regex used to match the label of a payment with a member ID
ID_REGEX = r"(?i)(\b|_)ID[\s\-\_\/]*(\d+)(\b|_)"

################################################################################

def try_to_match_payment_with_members(payments):

    members = Member.objects.all()

    idregex = re.compile(ID_REGEX)

    for payment in payments:

        payment_label = payment["label"].upper()
        sender_name = payment["sender_name"].upper()

        # First, attempt to match the member ID
        idmatches = idregex.findall(payment_label)
        if len(idmatches) == 1:
            i = int(idmatches[0][1])
            member_matches = [ member.username for member in members if member.pk==i ]
            if len(member_matches) == 1:
                payment["member_matched"] = member_matches[0]
                #print("Matched by ID to "+member_matches[0])
                continue


        # Second, attempt to find the username
        usernamematch = None
        for member in members:
            username = flatten(member.username)
            matches = re.compile(r"(?i)(\b|_)"+re.escape(username)+r"(\b|_)") \
                        .findall(payment_label)

            # If not found, try next
            if len(matches) == 0:
                continue

            # If we already had a match, abort the whole search because we
            # have multiple usernames matched !
            if usernamematch != None:
                usernamematch = None
                break

            usernamematch = member.username

        if usernamematch != None:
            payment["member_matched"] = usernamematch
            #print("Matched by username to "+usernamematch)
            continue


        # Third, attempt to match by family name
        familynamematch = None
        for member in members:
            if member.last_name == "":
                continue

            # "Flatten" accents in the last name... (probably the CSV
            # don't contain 'special' chars like accents
            member_last_name = flatten(member.last_name)

            matches = re.compile(r"(?i)(\b|_)"+re.escape(member_last_name)+r"(\b|_)") \
                        .findall(sender_name)

            # If not found, try next
            if len(matches) == 0:
                continue

            # If this familyname was matched several time, abort the whole search
            #if len(matches) > 1:
            #    print("Several matches ! Aborting !")
            #    familynamematch = None
            #    break

            # If we already had a match, abort the whole search because we
            # have multiple familynames matched !
            if familynamematch != None:
                familynamematch = None
                break

            familynamematch = member_last_name
            usernamematch = member.username

        if familynamematch != None:
            payment["member_matched"] = usernamematch
            #print("Matched by familyname to "+familynamematch)
            continue

        #print("Could not match")
        payment["member_matched"] = None

    return payments


def unmatch_payment_with_keywords(payments):

    matchers = {}
    for keyword in KEYWORDS_TO_NOTMATCH:
        matchers[keyword] = re.compile(r"(?i)(\b|_|-)"+re.escape(keyword)+r"(\b|_|-)")

    for i, payment in enumerate(payments):

        # If no match found, don't filter anyway
        if payment["member_matched"] == None:
            continue

        for keyword, matcher in list(matchers.items()):
            matches = matcher.findall(payment["label"])

            # If not found, try next
            if len(matches) == 0:
                continue

            #print("Ignoring possible match for payment '%s' because " \
            #      "it contains the keyword %s"                        \
            #      % (payment["label"], keyword))
            payments[i]["member_matched"] = None

            break

    return payments


def filter_already_known_payments(payments):

    new_payments = []

    known_payments = Payment.objects.all()

    for payment in payments:

        found_match = False
        for known_payment in known_payments:
            if  (str(known_payment.date) == payment["date"]) \
            and (known_payment.label == payment["label"]) \
            and (float(known_payment.amount) == float(payment["amount"])):
                found_match = True
                break

        if not found_match:
            new_payments.append(payment)

    return new_payments

def flatten(some_string):
    return unidecode.unidecode(some_string).upper()


def add_new_payments(new_payments):

    for new_payment in new_payments:

        # Get the member if there's a member matched
        member = None
        if new_payment["member_matched"]:
            member = Member.objects.filter(username=new_payment["member_matched"])
            assert len(member) == 1
            member = member[0]

        print("Adding new payment : ")
        print(new_payment)

        # Create the payment
        payment = Payment.objects.create(amount=float(new_payment["amount"]),
                                         label=new_payment["label"],
                                         date=new_payment["date"],
                                         member=member)
