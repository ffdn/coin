# -*- coding: utf-8 -*-
import json
import os

from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.conf.urls import url
from django.contrib.admin.utils import flatten_fieldsets
from django import forms
from django.shortcuts import render

from coin.filtering_queryset import LimitedAdminInlineMixin
from coin.billing.models import Invoice, InvoiceDetail, Payment, \
    PaymentAllocation, MembershipFee, Donation
from coin.billing.utils import get_bill_from_id_or_number
from coin.members.models import Member
from coin.members.admin import MemberAdmin
from django.urls import reverse

from .forms import WizardImportPaymentCSVStep1, WizardImportPaymentCSVStep2
from .import_payments import add_new_payments
from .import_payments_from_xml import process as process_xml
from .import_payments_from_csv import process as process_csv


class InvoiceDetailInlineForm(forms.ModelForm):
    class Meta:
        model = InvoiceDetail
        fields = (
            'label', 'amount', 'quantity', 'tax',
            'offersubscription', 'period_from', 'period_to'
        )
        widgets = {'quantity': forms.NumberInput(attrs={'step': 1})}


class InvoiceDetailInline(LimitedAdminInlineMixin, admin.StackedInline):
    model = InvoiceDetail
    extra = 0
    fields = (('label', 'amount', 'quantity', 'tax'),
              ('offersubscription', 'period_from', 'period_to'))
    form = InvoiceDetailInlineForm

    def get_filters(self, obj):
        """
        Le champ "Abonnement" est filtré afin de n'afficher que les abonnements
        du membre choisi dans la facture. Si pas de membre alors renvoi
        une liste vide
        """
        if obj and obj.member:
            return (('offersubscription', {'member': obj.member}),)
        else:
            return (('offersubscription', None),)

    def get_readonly_fields(self, request, obj=None):
        if not obj or not obj.member:
            return self.readonly_fields + ('offersubscription',)
        return self.readonly_fields


class InvoiceDetailInlineReadOnly(admin.StackedInline):

    """
    Lorsque la facture est validée, il n'est plus possible de la modifier
    Ce inline est donc identique à InvoiceDetailInline, mais tous
    les champs sont en lecture seule
    """
    model = InvoiceDetail
    extra = 0
    fields = InvoiceDetailInline.fields
    can_delete = False

    def has_add_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if self.get_fieldsets(request, obj):
            result = flatten_fieldsets(self.get_fieldsets(request, obj))
        else:
            result = list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))
            result.remove('id')
        return result


class PaymentAllocatedReadOnly(admin.TabularInline):
    model = PaymentAllocation
    extra = 0
    fields = ("payment", "amount")
    readonly_fields = ("payment", "amount")
    verbose_name = None
    verbose_name_plural = "Paiement alloués"

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class PaymentInlineAdd(admin.StackedInline):
    model = Payment
    extra = 0
    fields = (('date', 'payment_mean', 'amount', 'label'),)
    can_delete = False

    verbose_name_plural = "Ajouter des paiements"

    def has_change_permission(self, request, obj=None):
        return False


class DummyPaymentInlineAdd(PaymentInlineAdd):
    def has_add_permission(self, request, obj=None):
        return False


class InvoiceAdmin(admin.ModelAdmin):
    list_display_links = ('number', 'date')
    list_filter = ('status', 'validated', 'date')
    search_fields = ['member__username', 'member__first_name', 'member__last_name', 'member__email', 'member__nickname', 'number']
    list_display = ('number', 'date', 'status', 'amount', 'member',
                    'validated')
    fields = (('number', 'date', 'status'),
              ('date_due'),
              ('member'),
              ('amount', 'amount_paid'),
              ('validated', 'pdf'))
    readonly_fields = ('amount', 'amount_paid', 'validated', 'pdf', 'number')

    def get_readonly_fields(self, request, obj=None):
        """
        Si la facture est validée, passe tous les champs en readonly
        """
        if obj and obj.validated:
            if self.get_fieldsets(request, obj):
                ro_fields = flatten_fieldsets(self.get_fieldsets(request, obj))
            else:
                ro_fields = list(set(
                    [field.name for field in self.opts.local_fields] +
                    [field.name for field in self.opts.local_many_to_many]
                ))
            try:
                ro_fields.remove("status")
            except:
                pass
            return ro_fields
        return self.readonly_fields

    def get_inline_instances(self, request, obj=None):
        """
        Renvoi les inlines selon le context :
        * Si création, alors ne renvoi aucun inline
        * Si modification, renvoi InvoiceDetail et PaymentInline
        * Si facture validée, renvoi InvoiceDetail en ReadOnly et PaymentInline
        """
        inlines = []
        inline_instances = []

        if obj is not None:
            if obj.validated:
                inlines = [InvoiceDetailInlineReadOnly]
            else:
                inlines = [InvoiceDetailInline]

            if obj.validated:
                inlines += [PaymentAllocatedReadOnly]
                if obj.status == "open":
                    inlines += [PaymentInlineAdd]
                else:
                    # if we do not provide any inline, coin will yell on
                    # closed->open status, expecting POST data with Payment
                    # inline data. Thus, we provide an empty.
                    inlines += [DummyPaymentInlineAdd]

        for inline_class in inlines:
            inline = inline_class(self.model, self.admin_site)

            if request:
                if not (inline.has_add_permission(request) or
                        inline.has_change_permission(request, obj) or
                        inline.has_delete_permission(request, obj)):
                    continue
                if not inline.has_add_permission(request):
                    inline.max_num = 0
            inline_instances.append(inline)

        return inline_instances

    def get_urls(self):
        """
        Custom admin urls
        """
        urls = super().get_urls()
        my_urls = [
            url(r'^validate/(?P<id>.+)$',
                self.admin_site.admin_view(self.validate_view),
                name='invoice_validate'),
            url(r'^cancel/(?P<id>.+)$',
                self.admin_site.admin_view(self.bill_cancel),
                name='bill_cancel'),
        ]
        return my_urls + urls

    def validate_view(self, request, id):
        """
        Vue appelée lorsque l'admin souhaite valider une facture et
        générer son pdf
        """

        # TODO : Add better perm here
        if request.user.is_superuser:
            invoice = get_bill_from_id_or_number(id)
            if invoice.amount == 0:
                messages.error(request, 'Une facture validée ne peut pas avoir'
                                        ' un total de 0€.')
            else:
                invoice.validate()
                messages.success(request, 'La facture a été validée.')
        else:
            messages.error(
                request, 'Vous n\'avez pas l\'autorisation de valider '
                         'une facture.')

        return HttpResponseRedirect(reverse('admin:billing_invoice_change',
                                            args=(id,)))

    def bill_cancel(self, request, id):
        if request.user.is_superuser:
            bill = get_bill_from_id_or_number(id)
            bill.cancel()
            messages.success(request, 'Un avoir a été créé pour annuler cette facture.')
        else:
            messages.error(
                request, 'Vous n\'avez pas l\'autorisation d\'annuler des factures/notes')
        return HttpResponseRedirect(reverse('admin:billing_invoice_change',
                                            args=(id,)))


class PaymentAllocationInlineReadOnly(admin.TabularInline):
    model = PaymentAllocation
    extra = 0
    fields = ("bill", "amount")
    readonly_fields = ("bill", "amount")
    verbose_name = None
    verbose_name_plural = "Alloué à"

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class PaymentAdmin(admin.ModelAdmin):

    list_display = ('__str__', 'member', 'payment_mean', 'amount', 'date',
                    'amount_already_allocated', 'label')
    list_display_links = ()
    fields = (('member'),
              ('amount', 'payment_mean', 'date', 'label'),
              ('amount_already_allocated'))
    readonly_fields = ('amount_already_allocated',)
    list_filter = ['payment_mean']
    search_fields = ['member__username', 'member__first_name', 'member__last_name', 'member__email', 'member__nickname']

    def get_readonly_fields(self, request, obj=None):

        # If payment already started to be allocated or already have a member
        if obj and (obj.amount_already_allocated() != 0 or obj.member != None):
            # All fields are readonly
            return flatten_fieldsets(self.get_fieldsets(request, obj))
        else:
            return self.readonly_fields

    def get_inline_instances(self, request, obj=None):
        return [PaymentAllocationInlineReadOnly(self.model, self.admin_site)]

    def get_urls(self):

        urls = super().get_urls()

        my_urls = [
            url(r'wizard_import_payment_csv/$', self.wizard_import_payment_csv, name='wizard_import_payment_csv'),
        ]

        return my_urls + urls

    def wizard_import_payment_csv(self, request):
        template = "admin/billing/payment/wizard_import_payment.html"

        phase2 = False
        form1 = WizardImportPaymentCSVStep1(request.POST, request.FILES)

        if request.method == 'POST':
            phase2 = True

            # If the user didn't ask for commit yet
            # display the result of the analysis (i.e. the matching)
            if "commit" not in request.POST:

                if form1.is_valid():
                    # Analyze
                    file_name = request.FILES["file"].name
                    ext = os.path.splitext(file_name)[1]
                    if ext == ".csv":
                        new_payments = process_csv(request.FILES["file"])
                    else:
                        new_payments = process_xml(request.FILES["file"])

                    # Add id on each payment, so we can detect which one was selected
                    i = 0
                    for p in new_payments:
                        p["id"] = "payment_" + str(i)
                        i += 1

                    return render(request, template, {
                        'adminform': form1,
                        'opts': self.model._meta,
                        'payments_data': json.dumps(new_payments),
                        'new_payments': new_payments,
                        'phase2': phase2
                    })
            else:
                form2 = WizardImportPaymentCSVStep2(request.POST, request.FILES)
                if form2.is_valid():
                    new_payments = json.loads(request.POST.get("payments_data"))
                    # Import only selected payments
                    new_payments = [p for p in new_payments if request.POST.get(p["id"])]
                    add_new_payments(new_payments)

                    if "send_reminders" in request.POST:
                        members_to_remind = Member.objects.filter(balance__lt=-14)
                        for member in members_to_remind:
                            member.send_reminder_negative_balance(auto=True)

                    return HttpResponseRedirect('../')
        else:
            form = WizardImportPaymentCSVStep1()

        return render(request, template, {
            'adminform': form1,
            'opts': self.model._meta,
            'phase2': phase2
            })

class MembershipFeeAdmin(admin.ModelAdmin):
    list_display = ('member', 'end_date', '_amount')
    search_fields = ['member__username', 'member__first_name', 'member__last_name', 'member__email', 'member__nickname']
    list_filter = ['date']


class DonationAdmin(admin.ModelAdmin):
    list_display = ('member', 'date', '_amount')

admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(MembershipFee, MembershipFeeAdmin)
admin.site.register(Donation, DonationAdmin)
