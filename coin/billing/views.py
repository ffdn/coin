# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render
from django.contrib import messages

from django_sendfile import sendfile

from coin.billing.models import Invoice
from coin.members.models import Member
from coin.html2pdf import render_as_pdf

from coin.billing.create_subscriptions_invoices import create_all_members_invoices_for_a_period
from coin.billing.utils import get_bill_from_id_or_number, assert_user_can_view_the_bill


def bill_pdf(request, id):
    """
    Renvoi une note générée en format pdf
    id peut être soit la pk d'une note, soit le numero d'une facture
    """
    bill = get_bill_from_id_or_number(id)

    assert_user_can_view_the_bill(request, bill)

    human_type = bill.__class__._meta.verbose_name
    id_for_filename = bill.number if bill.type == "Invoice" else bill.pk

    pdf_filename = '%s_%s.pdf' % (human_type, id_for_filename)

    return sendfile(request, bill.pdf.path,
                    attachment=True, attachment_filename=pdf_filename)
