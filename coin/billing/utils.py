# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied

from coin.billing.models import Bill, Invoice


def get_bill_from_id_or_number(id):
    """
    Return an bill using id as bill id (or failing as invoice number)
    """

    try:
        return Bill.objects.get(pk=id).as_child()
    except:
        return get_object_or_404(Invoice, number=id)


def assert_user_can_view_the_bill(request, bill):
    """
    Raise PermissionDenied if logged user can't access given bill
    """
    if not bill.has_owner(request.user.username)\
       and not request.user.is_superuser:
        raise PermissionDenied
