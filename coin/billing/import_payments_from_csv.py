# -*- coding: utf-8 -*-
"""
Import payments from a CSV file from a bank.  The payments will automatically be
parsed, and there'll be an attempt to automatically match payments with members.

The matching is performed using the label of the payment.
- First, try to find a string such as 'ID-42' where 42 is the member's ID
- Second (if no ID found), try to find a member username (with no ambiguity with
  respect to other usernames)
- Third (if no username found), try to find a member family name (with no
  ambiguity with respect to other family name)

This script will check if a payment has already been registered with same
properies (date, label, price) to avoid creating duplicate payments inside coin.

By default, only a dry-run is perfomed to let you see what will happen ! You
should run this command with --commit if you agree with the dry-run.
"""

# Standard python libs
import csv
import datetime
import logging
from io import StringIO

from coin.billing.import_payments import try_to_match_payment_with_members, unmatch_payment_with_keywords, \
    filter_already_known_payments

# Parser / import / matcher configuration

# The CSV delimiter
DELIMITER = str(';')
# The date format in the CSV
DATE_FORMAT = "%d/%m/%Y"

################################################################################


def process(f):

    raw_csv = list(csv.reader(StringIO(f.read().decode()), delimiter=DELIMITER))
    cleaned_csv = clean_csv(raw_csv)

    payments = convert_csv_to_dicts(cleaned_csv)

    payments = try_to_match_payment_with_members(payments)
    new_payments = filter_already_known_payments(payments)
    new_payments = unmatch_payment_with_keywords(new_payments)

    return new_payments


def is_date(text):
    try:
        datetime.datetime.strptime(text, DATE_FORMAT)
        return True
    except ValueError:
        return False


def is_money_amount(text):
    try:
        float(text.replace(",", "."))
        return True
    except ValueError:
        return False


def load_csv(filename):
    with open(filename, "r") as f:
        return list(csv.reader(f, delimiter=DELIMITER))


def clean_csv(data):

    output = []

    for i, row in enumerate(data):

        if len(row) < 4:
            continue

        if not is_date(row[0]):
            logging.warning("Ignoring the following row (bad format for date in the first column) :")
            logging.warning(str(row))
            continue

        if is_money_amount(row[2]):
            logging.warning("Ignoring row %s (not a payment)" % str(i))
            logging.warning(str(row))
            continue

        if not is_money_amount(row[3]):
            logging.warning("Ignoring the following row (bad format for money amount in colun three) :")
            logging.warning(str(row))
            continue

        # Clean the date
        row[0] = datetime.datetime.strptime(row[0], DATE_FORMAT).strftime("%Y-%m-%d")

        # Clean the label ...
        row[4] = row[4].replace('\r', ' ')
        row[4] = row[4].replace('\n', ' ')

        output.append(row)

    return output


def convert_csv_to_dicts(data):

    output = []

    for row in data:
        payment = {}

        payment["date"] = row[0]
        payment["label"] = row[4]
        payment["amount"] = float(row[3].replace(",", "."))

        output.append(payment)

    return output
