# -*- coding: utf-8 -*-

from django.apps import AppConfig


class BillingConfig(AppConfig):
    name = 'coin.billing'
    verbose_name = 'Facturation'

    admin_menu_entry = {
        "id": "billing",
        "icon": "euro",
        "title": "Facturation",
        "models": [
            ("Cotisations", "billing/membershipfee"),
            ("Dons", "billing/donation"),
            ("Paiements", "billing/payment"),
            ("Factures", "billing/invoice"),
        ]
    }
