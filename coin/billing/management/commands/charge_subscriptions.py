# -*- coding: utf-8 -*-
import datetime

from argparse import RawTextHelpFormatter
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from coin.utils import respect_language
from coin.billing.create_subscriptions_invoices import create_all_members_invoices_for_a_period


class Command(BaseCommand):

    help = 'Create invoices for members subscriptions for date specified (or today if no date passed)'

    def create_parser(self, *args, **kwargs):
        parser = super().create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def add_arguments(self, parser):

        parser.add_argument(
            'date',
            type=str,
            help="The date for the period for which to charge subscription (e.g. 2011-07-04)"
        )

        parser.add_argument(
            '--antidate',
            action='store_true',
            dest='antidate',
            default=False,
            help="'Antidate' invoices, in the sense that invoices won't be validated with today's date but using the date of the end of the service. Meant to be use to charge subscription from a few months in the past..."
        )

        parser.add_argument(
            '--send-email-notifications',
            action='store_true',
            dest='send_email_notifications',
            default=False,
            help="Send email notification after creating invoices"
        )

    def handle(self, *args, **options):
        verbosity = int(options['verbosity'])
        antidate = options['antidate']
        send_email_notifications = options['send_email_notifications']
        date = options["date"]

        try:
            date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        except IndexError:
            date = datetime.date.today()
        except ValueError:
            raise CommandError(
                'Please enter a valid date : YYYY-mm-dd (ex: 2011-07-04)')

        if verbosity >= 2:
            self.stdout.write(
                'Create invoices for all members for the date : %s' % date)
        with respect_language(settings.LANGUAGE_CODE):
            invoices = create_all_members_invoices_for_a_period(date, antidate, send_email_notifications)

        if len(invoices) > 0 or verbosity >= 2:
            self.stdout.write(
                '%d invoices were created' % len(invoices))

