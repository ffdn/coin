# -*- coding: utf-8 -*-

# Standard python libs
import logging

# Django specific imports
from argparse import RawTextHelpFormatter
from django.core.management.base import BaseCommand, CommandError

# Coin specific imports
from coin.billing.models import Invoice


class Command(BaseCommand):

    help = """
Send a reminder to members for invoices which are due and not paid since a few
weeks.
"""

    def create_parser(self, *args, **kwargs):
        parser = super().create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def handle(self, *args, **options):

        invoices = Invoice.objects.filter(status="open")

        for invoice in invoices:

            if not invoice.reminder_needed():
                continue

            invoice.send_reminder(auto=True)

