# -*- coding: utf-8 -*-

import os

from django.core.exceptions import ValidationError
from django import forms

def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.csv', '.xml']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')

class WizardImportPaymentCSVStep1(forms.Form):
    file = forms.FileField(validators=[validate_file_extension])


class WizardImportPaymentCSVStep2(forms.Form):
    payments_data = forms.HiddenInput()
