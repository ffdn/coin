# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.views.generic import DetailView
from coin.billing import views

app_name = "coin.billing"

urlpatterns = [
    #'',
    url(r'^bill/(?P<id>.+)/pdf$', views.bill_pdf, name="bill_pdf"),
]
