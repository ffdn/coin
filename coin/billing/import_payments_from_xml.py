# -*- coding: utf-8 -*-
"""
Import payments from a XML file from a bank.  The payments will automatically be
parsed, and there'll be an attempt to automatically match payments with members.

The matching is performed using the label of the payment.
- First, try to find a string such as 'ID-42' where 42 is the member's ID
- Second (if no ID found), try to find a member username (with no ambiguity with
  respect to other usernames)
- Third (if no username found), try to find a member family name (with no
  ambiguity with respect to other family name)

This script will check if a payment has already been registered with same
properties (date, label, price) to avoid creating duplicate payments inside coin.

By default, only a dry-run is performed to let you see what will happen ! You
should run this command with --commit if you agree with the dry-run.
"""

from lxml import etree

from .import_payments import try_to_match_payment_with_members, unmatch_payment_with_keywords, \
    filter_already_known_payments


################################################################################


def process(f):
    payments = parse_xml(f.read().decode())

    payments = try_to_match_payment_with_members(payments)
    new_payments = filter_already_known_payments(payments)
    new_payments = unmatch_payment_with_keywords(new_payments)

    return new_payments


def parse_xml(raw_data: str) -> list[dict[str, str]]:
    xml_tree: etree._Element = etree.XML(raw_data.encode())
    xml_ns: str = xml_tree.nsmap.get(None, None)

    def get_tag(name: str) -> str:
        if xml_ns:
            return '{' + xml_ns + '}' + name
        else:
            return name

    all_transactions = xml_tree.iterchildren(get_tag('BkToCstmrStmt')).__next__() \
        .iterchildren(get_tag('Stmt')).__next__() \
        .iterchildren(get_tag('Ntry'))

    def extract_transaction(item: etree._Element) -> dict[str, str]:
        details_tree: etree._Element = item.iterchildren(get_tag('NtryDtls')).__next__() \
            .iterchildren(get_tag('TxDtls')).__next__()
        address_tree: etree._Element = details_tree.iterchildren(get_tag('RltdPties')).__next__() \
            .iterchildren(get_tag('Dbtr')).__next__()
        return dict(date=item.iterchildren(get_tag('ValDt')).__next__()
                    .iterchildren(get_tag('Dt')).__next__().text,
                    amount=item.iterchildren(get_tag('AmtDtls')).__next__()
                    .iterchildren(get_tag('TxAmt')).__next__()
                    .iterchildren(get_tag('Amt')).__next__().text,
                    transaction_description=item.iterchildren(get_tag('AddtlNtryInf')).__next__().text,
                    label=details_tree.iterchildren(get_tag('RmtInf')).__next__()
                    .iterchildren(get_tag('Ustrd')).__next__().text
                    if (len(list(details_tree.iterchildren(get_tag('RmtInf')))) > 0) else '',
                    sender_name=address_tree.iterchildren(get_tag('Nm')).__next__().text,
                    sender_addr=' '.join([i.text for i in address_tree.iterchildren(get_tag('PstlAdr')).__next__()
                                         .iterchildren()])
                    )

    debit = [extract_transaction(i) for i in all_transactions
             if i.iterchildren(get_tag('CdtDbtInd')).__next__().text == 'CRDT']
    return debit
