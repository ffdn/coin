# -*- coding: utf-8 -*-

from django.test import TestCase
from django.db import models

from coin.utils import get_descendant_classes


class UtilsTestCases(TestCase):
    def test_get_descendant_classes(self):
        class Ancestor(models.Model):
            class Meta:
                app_label = 'testapp'

        self.assertEqual(
            list(get_descendant_classes(Ancestor)),
            []
        )

        class ConcreteChild(Ancestor):
            pass

        self.assertEqual(
            list(get_descendant_classes(Ancestor)),
            [ConcreteChild]
        )

        class ConcreteSubChild(ConcreteChild):
            pass

        self.assertEqual(
            list(get_descendant_classes(Ancestor)),
            [ConcreteChild, ConcreteSubChild]
        )

        class AbstractChild(Ancestor):
            class Meta:
                abstract = True
                app_label = 'testapp'

        self.assertEqual(
            list(get_descendant_classes(Ancestor)),
            [ConcreteChild, ConcreteSubChild]
        )

        class OtherConcreteSubChild(AbstractChild):
            class Meta:
                app_label = 'testapp'

        self.assertEqual(
            list(get_descendant_classes(Ancestor)),
            [ConcreteChild, ConcreteSubChild, OtherConcreteSubChild]
        )
