from django.apps import apps
from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from coin import views
import coin.apps

from django.contrib import admin
admin.autodiscover()

from coin.isp_database.views import isp_json
import coin.members.views

app_name = "coin"

def apps_urlpatterns():
    """ Yields url lists ready to be appended to urlpatterns list
    """
    for app_config in apps.get_app_configs():
        if isinstance(app_config, coin.apps.AppURLs):
            for prefix, pats in app_config.exported_urlpatterns:
                yield path(
                    f'{prefix}/',
                    include(pats, namespace=prefix))

urlpatterns = [
    #'',
    path('', coin.members.views.index, name='home'),

    path('isp.json', isp_json),
    path('members/', include('coin.members.urls', namespace='members')),
    path('billing/', include('coin.billing.urls', namespace='billing')),
    path('subscription/', include('coin.offers.urls', namespace='subscription')),
    path('admin/', admin.site.urls),
    path('hijack/', include('hijack.urls', namespace='hijack')),

    # path('admin/doc/', include('django.contrib.admindocs.urls')),

    path('feed/<feed_name>', views.feed, name='feed'),
]


urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Pluggable apps URLs
urlpatterns += list(apps_urlpatterns())


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
