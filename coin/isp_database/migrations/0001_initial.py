# -*- coding: utf-8 -*-

from django.db import models, migrations
import django.core.validators
import coin.isp_database.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ChatRoom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=256, verbose_name='URL')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CoveredArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=512)),
                ('technologies', models.CharField(max_length=16, choices=[('ftth', 'FTTH'), ('dsl', '*DSL'), ('wifi', 'WiFi')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ISPInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text="The ISP's name", max_length=512)),
                ('shortname', models.CharField(help_text='Shorter name', max_length=15, blank=True)),
                ('description', models.TextField(help_text='Short text describing the project', blank=True)),
                ('logoURL', models.URLField(help_text="HTTP(S) URL of the ISP's logo", verbose_name='logo URL', blank=True)),
                ('website', models.URLField(help_text='URL to the official website', blank=True)),
                ('email', models.EmailField(help_text='Contact email address', max_length=254)),
                ('mainMailingList', models.EmailField(help_text='Main public mailing-list', max_length=254, verbose_name='main mailing list', blank=True)),
                ('creationDate', models.DateField(help_text='Date of creation for legal structure', null=True, verbose_name='creation date', blank=True)),
                ('ffdnMemberSince', models.DateField(help_text='Date at wich the ISP joined the Federation', null=True, verbose_name='FFDN member since', blank=True)),
                ('progressStatus', models.PositiveSmallIntegerField(blank=True, help_text='Progression status of the ISP', null=True, verbose_name='progress status', validators=[django.core.validators.MaxValueValidator(7)])),
                ('latitude', models.FloatField(help_text='Coordinates of the registered office (latitude)', null=True, blank=True)),
                ('longitude', models.FloatField(help_text='Coordinates of the registered office (longitude)', null=True, blank=True)),
            ],
            options={
            },
            bases=(coin.isp_database.models.SingleInstanceMixin, models.Model),
        ),
        migrations.CreateModel(
            name='OtherWebsite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=512)),
                ('url', models.URLField(verbose_name='URL')),
                ('isp', models.ForeignKey(to='isp_database.ISPInfo', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegisteredOffice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_office_box', models.CharField(max_length=512, blank=True)),
                ('extended_address', models.CharField(max_length=512, blank=True)),
                ('street_address', models.CharField(max_length=512, blank=True)),
                ('locality', models.CharField(max_length=512)),
                ('region', models.CharField(max_length=512)),
                ('postal_code', models.CharField(max_length=512, blank=True)),
                ('country_name', models.CharField(max_length=512)),
                ('isp', models.OneToOneField(to='isp_database.ISPInfo', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='coveredarea',
            name='isp',
            field=models.ForeignKey(to='isp_database.ISPInfo', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='chatroom',
            name='isp',
            field=models.ForeignKey(to='isp_database.ISPInfo', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
