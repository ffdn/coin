# -*- coding: utf-8 -*-

from django.db import models, migrations
import localflavor.fr.models


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0005_registeredoffice_siret'),
    ]

    operations = [
        migrations.AddField(
            model_name='bankinfo',
            name='check_order',
            field=models.CharField(default='', max_length=100, verbose_name="Ordre devant figurer sur un ch\xe8que bancaire \xe0 destination de l'association"),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='administrative_email',
            field=models.EmailField(help_text='Adresse email pour les contacts administratifs (ex: bureau)', max_length=254, verbose_name='Contact administratif', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='support_email',
            field=models.EmailField(help_text='Adresse email pour les demandes de support technique', max_length=254, verbose_name='Contact de support', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='siret',
            field=localflavor.fr.models.FRSIRETField(max_length=14, verbose_name='SIRET'),
            preserve_default=True,
        ),
    ]
