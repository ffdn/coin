# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0008_ispinfo_lists_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coveredarea',
            name='technologies',
            field=models.CharField(max_length=16, choices=[('ftth', 'FTTH'), ('dsl', '*DSL'), ('wifi', 'WiFi'), ('vpn', 'VPN'), ('cube', 'Brique Internet')]),
            preserve_default=True,
        ),
    ]
