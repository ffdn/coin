# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0007_auto_20141112_2353'),
    ]

    operations = [
        migrations.AddField(
            model_name='ispinfo',
            name='lists_url',
            field=models.URLField(default='', help_text='URL du serveur de listes de discussions/diffusion', verbose_name='serveur de listes', blank=True),
            preserve_default=False,
        ),
    ]
