# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0006_auto_20141111_1740'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bankinfo',
            options={'verbose_name': 'coordonn\xe9es bancaires', 'verbose_name_plural': 'coordonn\xe9es bancaires'},
        ),
        migrations.AlterField(
            model_name='bankinfo',
            name='bank_name',
            field=models.CharField(max_length=100, null=True, verbose_name='\xe9tablissement bancaire', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bankinfo',
            name='check_order',
            field=models.CharField(help_text="Ordre devant figurer sur un                                    ch\xe8que bancaire \xe0 destination de                                   l'association", max_length=100, verbose_name='ordre'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='administrative_email',
            field=models.EmailField(help_text='Adresse email pour les contacts administratifs (ex: bureau)', max_length=254, verbose_name='contact administratif', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='support_email',
            field=models.EmailField(help_text='Adresse email pour les demandes de support technique', max_length=254, verbose_name='contact de support', blank=True),
            preserve_default=True,
        ),
    ]
