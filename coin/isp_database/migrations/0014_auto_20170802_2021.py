# -*- coding: utf-8 -*-

from django.db import migrations, models
import multiselectfield.db.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0013_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='chatroom',
            options={'verbose_name': 'Salon de discussions', 'verbose_name_plural': 'Salons de discussions'},
        ),
        migrations.AlterModelOptions(
            name='coveredarea',
            options={'verbose_name': 'Zone couverte', 'verbose_name_plural': 'Zones couvertes'},
        ),
        migrations.AlterModelOptions(
            name='ispinfo',
            options={'verbose_name': 'Information du FAI', 'verbose_name_plural': 'Informations du FAI'},
        ),
        migrations.AlterModelOptions(
            name='otherwebsite',
            options={'verbose_name': 'Autre site Internet', 'verbose_name_plural': 'Autres sites Internet'},
        ),
        migrations.AlterModelOptions(
            name='registeredoffice',
            options={'verbose_name': 'Si\xe8ge social', 'verbose_name_plural': 'Si\xe8ges sociaux'},
        ),
        migrations.AlterField(
            model_name='coveredarea',
            name='name',
            field=models.CharField(max_length=512, verbose_name='Nom'),
        ),
        migrations.AlterField(
            model_name='coveredarea',
            name='technologies',
            field=multiselectfield.db.fields.MultiSelectField(max_length=42, verbose_name='Technologie', choices=[('ftth', 'FTTH'), ('dsl', '*DSL'), ('wifi', 'WiFi'), ('vpn', 'VPN'), ('cube', 'Brique Internet')]),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='creationDate',
            field=models.DateField(help_text='Date de cr\xe9ation de la structure l\xe9gale', null=True, verbose_name='Date de cr\xe9ation', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='description',
            field=models.TextField(help_text='Description courte du projet', verbose_name='Description', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='email',
            field=models.EmailField(help_text='Adresse courriel de contact', max_length=254, verbose_name='Courriel'),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='ffdnMemberSince',
            field=models.DateField(help_text='Date \xe0 laquelle le FAI a rejoint la F\xe9d\xe9ration FDN', null=True, verbose_name='Membre de FFDN depuis', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='latitude',
            field=models.FloatField(help_text='Coordonn\xe9es latitudinales du si\xe8ge', null=True, verbose_name='Latitude', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='logoURL',
            field=models.URLField(help_text='Adresse HTTP(S) du logo du FAI', verbose_name='URL du logo', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='longitude',
            field=models.FloatField(help_text='Coordonn\xe9es longitudinales du si\xe8ge', null=True, verbose_name='Longitude', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='mainMailingList',
            field=models.EmailField(help_text='Principale liste de discussion publique', max_length=254, verbose_name='Liste de discussion principale', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='name',
            field=models.CharField(help_text='Nom du FAI', max_length=512, verbose_name='Nom'),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='phone_number',
            field=models.CharField(help_text='Num\xe9ro de t\xe9l\xe9phone de contact principal', max_length=25, verbose_name='Num\xe9ro de t\xe9l\xe9phone', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='progressStatus',
            field=models.PositiveSmallIntegerField(blank=True, help_text="\xc9tat d'avancement du FAI", null=True, verbose_name="\xc9tat d'avancement", validators=[django.core.validators.MaxValueValidator(7)]),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='shortname',
            field=models.CharField(help_text='Nom plus court', max_length=15, verbose_name='Abr\xe9viation', blank=True),
        ),
        migrations.AlterField(
            model_name='ispinfo',
            name='website',
            field=models.URLField(help_text='Adresse URL du site Internet', verbose_name='URL du site Internet', blank=True),
        ),
        migrations.AlterField(
            model_name='otherwebsite',
            name='name',
            field=models.CharField(max_length=512, verbose_name='Nom'),
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='country_name',
            field=models.CharField(max_length=512, verbose_name='Pays'),
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='extended_address',
            field=models.CharField(max_length=512, verbose_name='Adresse compl\xe9mentaire', blank=True),
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='locality',
            field=models.CharField(max_length=512, verbose_name='Ville'),
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='post_office_box',
            field=models.CharField(max_length=512, verbose_name='Bo\xeete postale', blank=True),
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='postal_code',
            field=models.CharField(max_length=512, verbose_name='Code postal', blank=True),
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='region',
            field=models.CharField(max_length=512, verbose_name='R\xe9gion'),
        ),
        migrations.AlterField(
            model_name='registeredoffice',
            name='street_address',
            field=models.CharField(max_length=512, verbose_name='Adresse', blank=True),
        ),
    ]
