# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0002_bankinfo'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bankinfo',
            options={'verbose_name': 'Coordonn\xe9es bancaires', 'verbose_name_plural': 'Coordonn\xe9es bancaires'},
        ),
        migrations.AddField(
            model_name='ispinfo',
            name='administrative_email',
            field=models.EmailField(default='', help_text='Adresse pour les contacts administratifs (ex: bureau)', max_length=254, verbose_name='Contact administratif', blank=True),
            preserve_default=False,
        ),
    ]
