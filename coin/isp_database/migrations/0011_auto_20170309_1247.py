# -*- coding: utf-8 -*-

from django.db import migrations, models
import multiselectfield.db.fields
from coin.isp_database.models import TECHNOLOGIES


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0010_ispinfo_phone_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coveredarea',
            name='technologies',
            field=multiselectfield.db.fields.MultiSelectField(max_length=42, choices=TECHNOLOGIES),
        ),
    ]
