# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0009_auto_20151230_1759'),
    ]

    operations = [
        migrations.AddField(
            model_name='ispinfo',
            name='phone_number',
            field=models.CharField(help_text='Main contact phone number', max_length=25, verbose_name='phone number', blank=True),
            preserve_default=True,
        ),
    ]
