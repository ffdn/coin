# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('isp_database', '0003_auto_20141109_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='ispinfo',
            name='support_email',
            field=models.EmailField(default='', help_text='Adresse pour les demandes de support technique', max_length=254, verbose_name='Contact de support', blank=True),
            preserve_default=False,
        ),
    ]
