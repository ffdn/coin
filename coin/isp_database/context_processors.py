# -*- coding: utf-8 -*-

from django.conf import settings

from coin.isp_database.models import ISPInfo

def branding(request):
    """ Just a shortcut to get the ISP object in templates
    """
    return {
        'branding': ISPInfo.objects.first(),
        'SITE_HEADER': settings.SITE_HEADER,
        'SITE_TITLE': settings.SITE_TITLE,
        'SITE_LOGO_URL': settings.SITE_LOGO_URL,
    }
