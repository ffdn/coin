# -*- coding: utf-8 -*-

import json

from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.cache import cache_control

from coin.isp_database.models import ISPInfo


@cache_control(max_age=7200)
def isp_json(request):
    try:
        isp = ISPInfo.objects.get()
    except ISPInfo.DoesNotExist:
        raise Http404
    data = isp.to_dict()
    return HttpResponse(json.dumps(data), content_type="application/json")
