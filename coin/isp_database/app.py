# -*- coding: utf-8 -*-

from django.apps import AppConfig


class ISPdatabaseConfig(AppConfig):
    name = 'coin.isp_database'
    verbose_name = 'Identité du FAI'

    admin_menu_entry = {
        "id": "meta",
        "icon": "institution",
        "title": "Meta",
        "models": [
            ("Infos du FAI", "isp_database/ispinfo"),
            ("Infos du site", "sites/site"),
        ]
    }
