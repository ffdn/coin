# -*- coding: utf-8 -*-

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

from .utils import re_chat_url


def validate_v4(address):
    if address.version != 4:
        raise ValidationError('{} is not an IPv4 address'.format(address))


def validate_v6(address):
    if address.version != 6:
        raise ValidationError('{} is not an IPv6 address'.format(address))


chatroom_url_validator = RegexValidator(
    regex=re_chat_url,
    message="Enter a value of the form  <proto>://<server>/<channel>")
