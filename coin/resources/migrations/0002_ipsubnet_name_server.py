# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0001_initial'),
        ('reverse_dns', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='ipsubnet',
            name='name_server',
            field=models.ManyToManyField(help_text='Serveur de noms \xe0 qui d\xe9l\xe9guer la r\xe9solution DNS inverse', to='reverse_dns.NameServer', verbose_name='serveur de noms', blank=True),
            preserve_default=True,
        ),
    ]
