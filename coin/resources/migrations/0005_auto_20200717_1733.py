# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0004_auto_20190826_0011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipsubnet',
            name='comment',
            field=models.CharField(help_text="peut servir par exemple \xe0 indiquer que cette IP est r\xe9serv\xe9e \xe0 un \xe9l\xe9ment d'infra", max_length=255, verbose_name='commentaire', blank=True),
        ),
    ]
