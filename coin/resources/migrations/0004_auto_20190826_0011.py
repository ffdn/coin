# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0003_auto_20150203_1043'),
    ]

    operations = [
        migrations.AddField(
            model_name='ipsubnet',
            name='comment',
            field=models.CharField(default='', help_text='Commentaire', max_length=255, verbose_name='commentaire', blank=True),
        ),
        migrations.AlterField(
            model_name='ipsubnet',
            name='configuration',
            field=models.ForeignKey(related_name='ip_subnet', verbose_name='configuration', blank=True, to='configuration.Configuration', null=True, on_delete=models.CASCADE),
        ),
    ]
