# -*- coding: utf-8 -*-

from django.apps import AppConfig


class ResourcesConfig(AppConfig):
    name = 'coin.resources'
    verbose_name = "IPAM / Ressources"

    admin_menu_addons = {
        'infra': [
            ("Pool d'IP", "resources/ippool"),
            ("Subnets", "resources/ipsubnet"),
        ]
    }
