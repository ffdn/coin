# -*- coding: utf-8 -*-

from django.contrib import admin

from coin.resources.models import IPPool, IPSubnet
from coin.offers.models import OfferIPPool


class OfferIPPoolAdmin(admin.TabularInline):
    model = OfferIPPool
    extra = 1
    verbose_name_plural = "Offres utilisant ce pool d'IP"
    verbose_name = "Offre utilisant ce pool d'IP"


class IPPoolAdmin(admin.ModelAdmin):
    list_display = ('name', 'inet', 'default_subnetsize', 'linked_offers')
    ordering = ('inet',)
    inlines = [OfferIPPoolAdmin]

    def linked_offers(self, obj):
        offers = (i.name for i in obj.offers.all())
        return '{}'.format(', '.join(offers) or 'aucune')
    linked_offers.short_description = 'Offres liées'


# TODO: don't display "Delegate reverse DNS" checkbox and Nameservers when
# creating/editing the object in the admin (since it is a purely
# user-specific parameter)
class IPSubnetAdmin(admin.ModelAdmin):
    list_display = ('inet', 'ip_pool', 'configuration', 'comment')
    list_filter = ('ip_pool',)
    search_fields = ('inet',)
    ordering = ('inet',)


admin.site.register(IPPool, IPPoolAdmin)
admin.site.register(IPSubnet, IPSubnetAdmin)
