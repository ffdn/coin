# -*- coding: utf-8 -*-

from django.apps import apps
from django.contrib import admin


def collect_admin_cards():
    """
    Collects menu entries among apps and builds the admin main menu from it

    Collects in apps appconfigs, in two variables :

    AppConfig.admin_menu_entry for defining a full per-app menu entry
    AppConfig.admin_menu_addons to add links to a shared menu entry

    Format for those vars can be found in apps.py files
    """

    admin_cards = [
        {"id": "configs",
         "icon": "gears",
         "title": "Configurations",
         "models": []
        },

        {"id": "infra",
         "icon": "database",
         "title": "Infra",
         "models": [],
        },
    ]

    # Menu entries that can receive addons
    addons_containers = {
        'configs': admin_cards[0]['models'],
        'infra': admin_cards[1]['models'],
    }

    for app_config in apps.get_app_configs():
        menu_entry = getattr(app_config, 'admin_menu_entry', None)
        menu_addons = getattr(app_config, 'admin_menu_addons', None)
        if menu_entry:
            admin_cards.append(menu_entry)

        if menu_addons:
            for entry_id, links in list(menu_addons.items()):
                addons_containers[entry_id] += links

    return admin_cards


original_index = admin.site.index

def new_index(request, extra_context=None):
    extra_context = extra_context or {}
    extra_context["admin_cards"] = collect_admin_cards()
    return original_index(request, extra_context)

admin.site.index = new_index
