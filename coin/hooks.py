import json
import yaml
import logging
import shlex
import subprocess
from importlib import import_module
from pipes import quote

from django.conf import settings

logger = logging.getLogger(__name__)


"""
Hooks are defined in the settings with a syntax like :

settings.HOOKS = {"foo": {"CREATE_FILE": {"type": "bash",
                                          "command": "touch {dir}/{file}"},
                          "CREATE_REMOTE_FILE": {"type": "bash",
                                                 "remote": "user@foo.com",
                                                 "command": "touch {dir}/{file}"},
                          "UPDATE_FILE": {"type": "bash",
                                          "command": "tee {dir}/{file}",
                                          "stdin": "{content}"},
                          "LOAD_FILE": {"type": "bash",
                                        "command": "cat {dir}/{file}",
                                        "parse_output": "yaml"},
                          "PROVISION": {"type": "bash",
                                        "remote": "user@machine",
                                        "command": "provision_foo.sh --name {name} --ipv4 {ipv4}",
                                        "stdin": "{password}"}
                          },
                  "bar": {"PROVISION": {"type": "python",
                                        "module": "bar",
                                        "function": "provision"},
                          "GET_STATE": {"type": "python",
                                        "module": "bar",
                                        "function": "state"}
                          }
                  }
"""


class HookManager():

    @staticmethod
    def is_defined(module, name):
        """
        Returns true or false if the given hook name has been defined in
        settings (and is not empty string)
        """
        return hasattr(settings, "HOOKS") and settings.HOOKS.get(module, {}).get(name, {})

    @staticmethod
    def run(module, name, **kwargs):

        # The arg stdin can be provided and defines what will be fed on the
        # standard input of the command
        # In particular, in can / should be used to transmit password in a more
        # secure way compared to using regular arguments which can be sniffed
        # through ps -ef by other users on the system.

        if not HookManager.is_defined(module, name):
            raise Exception("No hook been defined for module %s, hook %s" % (module, name))

        logger.debug("Running hook %s for module %s with args %s" % (name, module, kwargs))

        hook = settings.HOOKS[module][name]
        if hook["type"] == "bash":
            (success, stdout, stderr) = HookManager._run_bash(hook, **kwargs)
        elif hook["type"] == "python":
            (success, stdout, stderr) = HookManager._run_python(hook, **kwargs)
        return (success, stdout, stderr)

    @staticmethod
    def _format_hook_bash(hook, **kwargs):

        command = hook["command"]

        # N.B. : here we split() the string to have separate piece.
        # So for example
        #     foo --bar "zblerg {bar}"
        # becomes something like
        #     ["foo", "--bar", "zblerg {bar}"]
        # and then format each piece independently.
        #
        # This is meant to avoid obvious injections. Though if you're using
        # a hook, you should try to validate the format of the args before
        # calling this (e.g. check that a variable domain provided by the
        # user really looks like a domain)

        to_run = [e.format(**kwargs) for e in shlex.split(command)]

        if "remote" in hook:
            user_at_machine = hook["remote"]
            remote_command = [quote(e) for e in to_run]
            remote_command = " ".join(remote_command)
            to_run = ["timeout", "300", "ssh", user_at_machine, remote_command]

        return to_run

    @staticmethod
    def _run_bash(hook, **kwargs):

        # Format command with provided infos
        to_run = HookManager._format_hook_bash(hook, **kwargs)

        if "stdin" in hook:
            stdin = hook["stdin"].format(**kwargs)
        else:
            stdin = None

        logger.debug("Will run hook command : %s" % to_run)

        p = subprocess.Popen(
            to_run,
            stdin=subprocess.PIPE if stdin else None,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        stdout, stderr = p.communicate(stdin.encode() if stdin else None)
        stdout = stdout.decode()
        stderr = stderr.decode()
        success = p.returncode == 0

        if "parse_output" in hook:
            parse_format = hook["parse_output"]
            if parse_format == "json":
                try:
                    stdout = json.loads(stdout)
                except Exception as e:
                    raise Exception("Failed to load stdout as json.\nStdout:\n%sStderr:\n%s\nError parsing stdout:%s" % (stdout, stderr, e))
            elif parse_format == "yaml":
                try:
                    stdout = yaml.safe_load(stdout)
                except Exception as e:
                    raise Exception("Failed to load stdout as yaml. Raw content:\n%s\nError:%s" % (stdout, e))
            else:
                raise Exception("Unsupported parse_format %s" % parse_format)

        return (success, stdout, stderr)

    @staticmethod
    def _run_python(hook, **kwargs):
        try:
            module = import_module("custom_hooks." + hook["module"])
        except Exception as e:
            raise Exception("Could not load python hook %s : %s" % (hook["module"], e))

        try:
            output = getattr(module, hook["function"])(**kwargs)
            return (True, output, None)
        except Exception as e:
            return (False, None, str(e))
