# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import PasswordResetForm, ReadOnlyPasswordHashField, PasswordChangeForm, SetPasswordForm
from django.conf import settings
from django.forms.utils import ErrorList
from django.forms.forms import BoundField

from coin.members.models import Member

from registration.forms import RegistrationForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from crispy_forms.bootstrap import StrictButton


class MemberRegistrationForm(RegistrationForm):

    # Protect against robot
    trap = forms.CharField(label='Trap',
                           required=False,
                           widget=forms.TextInput(attrs={'style' : 'display:none'}),
                           help_text="Si vous êtes humain ignorez ce champ")

    ack_status = forms.BooleanField(label=settings.MEMBER_TERMS,
                                    widget=forms.CheckboxInput(attrs={
                                        'style' : '' if settings.MEMBER_TERMS else 'display:none'
                                    }),
                                    required=bool(settings.MEMBER_TERMS))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for fieldname in ['email', 'organization_name', 'password2']:
            self.fields[fieldname].help_text = None
        self.fields['username'].help_text = "Requis. 30 caractères maximum. Uniquement des lettres minuscules, nombres et les caractères « - » et « _ »."
        self.fields['username'].label = "Nom d'utilisateurice"


    def is_valid(self):
         valid = super().is_valid()
         avoid_trap = not self.data['trap']
         if valid and avoid_trap:
             return True
         else:
             return False

    @property
    def helper(self):
        helper = FormHelper()

        helper.form_class = 'form-horizontal'
        helper.label_class = 'col-4'
        helper.field_class = 'col-8'
        helper.layout = Layout(
            *(list(self.fields.keys()) + [
                Div(
                    StrictButton('<i class="fa fa-rocket" aria-hidden="true"></i> Créer mon compte', css_class="btn-success", type="submit"),
                    css_class="text-center"
                )
            ])
        )

        return helper

    class Meta:
        model = Member
        fields = ['type', 'first_name', 'last_name',
                  'organization_name', 'email', 'username', 'trap']

class MemberCreationForm(forms.ModelForm):

    """
    This form was inspired from django.contrib.auth.forms.UserCreationForm
    and adapted to coin specificities
    """
    username = forms.RegexField(required=False,
                                label="Nom d'utilisateur", max_length=30, regex=r"^[\w.@+-]+$",
                                help_text="Laisser vide pour le générer automatiquement à partir du "
                                "nom d'usage, nom et prénom, ou nom de l'organisme")
    password = forms.CharField(
        required=False, label='Mot de passe', widget=forms.PasswordInput,
        help_text="Laisser vide et envoyer un mail de bienvenue pour que "
        "l'utilisateur choisisse son mot de passe lui-même")

    class Meta:
        model = Member
        fields = '__all__'

    def save(self, commit=True):
        """
        Save member, then set his password
        """
        member = super().save(commit=False)
        member.set_password(self.cleaned_data["password"])
        if commit:
            member.member()
        return member


class AbstractMemberChangeForm(forms.ModelForm):
    """
    This form was inspired from django.contrib.auth.forms.UserChangeForm
    and adapted to coin specificities
    """

    class Meta:
        model = Member
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

        if not settings.MEMBER_CAN_EDIT_PROFILE:
            for _, field in list(self.fields.items()):
                # In recent Django versions, setting field.disabled should be enough for
                # both visual and processing disable
                field.disabled = True
                field.widget.attrs['readonly'] = True

    def save(self, *args, **kwargs):
        if settings.MEMBER_CAN_EDIT_PROFILE:
            return super().save(*args, **kwargs)
        else:
            # skip form saving. In recent Django versions, this save()
            # override should no longer be required.
            return self.instance


    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

    def clean_username(self):
        # idem clean_password
        return self.initial["username"]

    @property
    def helper(self):
        helper = FormHelper()

        helper.form_class = 'form-horizontal'
        helper.label_class = 'col-4'
        helper.field_class = 'col-8'
        helper.layout = Layout(
            *(list(self.fields.keys()) + [
                Div(
                    StrictButton('<i class="fa fa-edit" aria-hidden="true"></i> Modifier', css_class="btn-primary", type="submit"),
                    css_class="text-center"
                )
            ])
        )

        return helper

    def save_m2m(self):
        pass


class AdminMemberChangeForm(AbstractMemberChangeForm):
    password = ReadOnlyPasswordHashField()


class SpanError(ErrorList):
    def __str__(self):
        return self.as_spans()
    def __str__(self):
        return self.as_spans()
    def as_spans(self):
        if not self: return ''
        return ''.join(['<span class="error">%s</span>' % e for e in self])

class PersonMemberChangeForm(AbstractMemberChangeForm):
    """
    Form use to allow natural person to change their info
    """
    class Meta:
        model = Member
        fields = ['first_name', 'last_name', 'email', 'nickname',
                  'home_phone_number', 'mobile_phone_number',
                  'address', 'postal_code', 'city', 'country']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.error_class = SpanError
        for fieldname in self.fields:
            self.fields[fieldname].help_text = None


class OrganizationMemberChangeForm(AbstractMemberChangeForm):
    """
    Form use to allow organization to change their info
    """
    class Meta:
        model = Member
        fields = ['organization_name', 'email', 'nickname',
                  'home_phone_number', 'mobile_phone_number',
                  'address', 'postal_code', 'city', 'country']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.error_class = SpanError
        for fieldname in self.fields:
            self.fields[fieldname].help_text = None

class MemberPasswordResetForm(PasswordResetForm):
    pass

def helper_password_reset(self):
    helper = FormHelper()


    helper.field_template = 'bootstrap4/layout/inline_field.html'

    helper.layout = Layout(
            *(list(self.fields.keys()) + [
                Div(
                    StrictButton('Réinitialiser mon mot de passe', css_class="btn-primary mt-2", type="submit"),
                    css_class="text-center"
                    )
                ])
            )

    return helper

PasswordResetForm.helper = property(helper_password_reset)

def helper_password_change(self):
    helper = FormHelper()

    #helper.form_class = 'form-horizontal'
    #helper.label_class = 'col-4'
    #helper.field_class = 'col-8'
    helper.layout = Layout(
            *(list(self.fields.keys()) + [
                Div(
                    StrictButton('Changer mon mot mot de passe', css_class="btn-primary", type="submit"),
                    css_class="text-center"
                    )
                ])
            )

    return helper

PasswordChangeForm.helper = property(helper_password_change)
SetPasswordForm.helper = property(helper_password_change)

