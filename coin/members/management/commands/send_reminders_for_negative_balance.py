# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Standard python libs
import logging

# Django specific imports
from argparse import RawTextHelpFormatter
from django.core.management.base import BaseCommand, CommandError

# Coin specific imports
from coin.members.models import Member


class Command(BaseCommand):

    help = """
Send a reminder to members with a negative balance higher than given threshold
"""

    def create_parser(self, *args, **kwargs):
        parser = super(Command, self).create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def add_arguments(self, parser):

        parser.add_argument(
            'threshold',
            type=int,
            default=20,
            help="Threshold for balances to consider (default 20)"
        )

    def handle(self, *args, **options):
        threshold = int(options['threshold'])

        members_to_remind = Member.objects.filter(status='member') \
                                          .filter(balance__lt=-1 * threshold)
        for member in members_to_remind:
            member.send_reminder_negative_balance(auto=True)
