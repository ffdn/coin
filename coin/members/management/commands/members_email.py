# -*- coding: utf-8 -*-

import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from coin.members.models import Member
from coin.offers.models import Offer
from coin.offers.models import OfferSubscription

class Command(BaseCommand):
    help = """Returns email addresses of members in a format suitable for bulk importing in Sympa."""

    def add_arguments(self, parser):
        parser.add_argument('--subscribers', action='store_true',
                            help='Return only the email addresses of subscribers to any offers.')
        parser.add_argument('--offer', metavar='OFFER-ID or OFFER-REF',
                            help='Return only the email addresses of subscribers to the specified offer')
        parser.add_argument('--all', action='store_true',
                            help='Return all email in database')

    def handle(self, *args, **options):
        if options['subscribers']:
            today = datetime.date.today()

            offer_subscriptions = OfferSubscription.objects.filter(
                Q(resign_date__gt=today)
                | Q(resign_date__isnull=True)
            )
            members = [s.member for s in offer_subscriptions]
        elif options['offer']:
            try:
                # Try to find the offer by its reference
                offer = Offer.objects.get(reference=options['offer'])
            except Offer.DoesNotExist:
                try:
                    # No reference found, maybe it's an offer_id
                    offer_id = int(options['offer'])
                    offer = Offer.objects.get(pk=offer_id)
                except Offer.DoesNotExist:
                    raise CommandError('Offer "%s" does not exist' % options['offer'])
                except (IndexError, ValueError):
                    raise CommandError('Please enter a valid offer reference or id')
            today = datetime.date.today()

            offer_subscriptions = OfferSubscription.objects.filter(
                 # Fetch all OfferSubscription to the given Offer
                Q(offer=offer)
                # Check if OfferSubscription isn't resigned
                & (Q(resign_date__isnull=True) | Q(resign_date__gt=today))
            ).select_related('member')
            members = [s.member for s in offer_subscriptions]
        elif options['all']:
            members = Member.objects.all()
        else:
            members = Member.objects.filter(status='member')

        emails = list({m.email for m in members})
        for email in emails:
            self.stdout.write(email)
