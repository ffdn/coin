# -*- coding: utf-8 -*-

import datetime
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.conf import settings

from coin.utils import respect_language
from coin.members.models import Member
from coin.billing.models import MembershipFee, Payment 

class Command(BaseCommand):
    args = '[date=2011-07-04]'
    help = """Create membership fees if a member has an active service
           """

    def handle(self, *args, **options):

        # Member with a service
        self.stdout.write("Member with a service")
        members = Member.objects.exclude(status='not_member').exclude(offersubscription=None).filter(offersubscription__resign_date=None).distinct('username')
       
        membership_fees = MembershipFee.objects.order_by('member__username', '-end_date').distinct('member__username').filter(member__in=members)

        for member in members:
            membership_fee = membership_fees.filter(member=member)
            start_date = member.offersubscription_set.order_by('subscription_date')[0].subscription_date
            if membership_fee and start_date < membership_fee[0].end_date:
                start_date = membership_fee[0].end_date

            while start_date <= datetime.date.today():
                self.stdout.write("Create MembershipFee for {username} {date}".format(username=member.username, date=start_date))
                end_date = start_date + relativedelta(years=1)
                fee = MembershipFee(member=member, _amount=settings.DEFAULT_MEMBERSHIP_FEE,
                                     start_date=start_date,
                                     end_date=start_date + relativedelta(years=1))
                fee.save()
                start_date = end_date
            member.status='member'
            member.save()
            

        # Member whithout services
        self.stdout.write("\n\nMember without a service")
        members = Member.objects.exclude(status='not_member').filter(offersubscription=None, balance__gte=settings.DEFAULT_MEMBERSHIP_FEE).distinct('username')
        for member in members:
            membership_fee = membership_fees.filter(member=member)
            start_date = None
            past_year = datetime.date.today() + relativedelta(years=-1)
            if not (membership_fee and membership_fee[0].end_date > datetime.date.today()): 
                continue
            if membership_fee and past_year < membership_fee[0].end_date:
                start_date = membership_fee[0].end_date
            
            last_payment = Payment.objects.filter(member=member, amount__gte=settings.DEFAULT_MEMBERSHIP_FEE, date__gte=past_year).order_by('-date')
            if last_payment:
                start_date = last_payment[0].date


            if start_date:
                self.stdout.write("Create MembershipFee for {username} {date}".format(username=member.username, date=start_date))
                fee = MembershipFee(member=member, _amount=settings.DEFAULT_MEMBERSHIP_FEE,
                                     start_date=start_date,
                                     end_date=start_date + relativedelta(years=1))
                fee.save()
                member.status='member'
                member.save()


