# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0017_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rowlevelpermission',
            options={'verbose_name': 'permission fine', 'verbose_name_plural': 'permissions fines'},
        ),
        migrations.AlterModelManagers(
            name='member',
            managers=[
            ],
        ),
    ]
