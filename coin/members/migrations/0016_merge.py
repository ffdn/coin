# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0015_auto_20170824_2308'),
        ('members', '0014_member_balance'),
    ]

    operations = [
    ]
