# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0019_auto_20190825_2329'),
        ('members', '0022_auto_20190623_1256'),
    ]

    operations = [
    ]
