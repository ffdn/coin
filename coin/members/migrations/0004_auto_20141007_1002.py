# -*- coding: utf-8 -*-

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_auto_20141007_0956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='amount',
            field=models.DecimalField(default=settings.DEFAULT_MEMBERSHIP_FEE, help_text='en \\u20ac', verbose_name='montant', max_digits=5, decimal_places=2),
        ),
    ]
