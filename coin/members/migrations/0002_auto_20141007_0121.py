# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='membershipfee',
            name='payment_date',
            field=models.DateField(null=True, verbose_name='date du paiement'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='membershipfee',
            name='payment_method',
            field=models.CharField(max_length=100, null=True, verbose_name='moyen de paiement', choices=[('cash', 'Esp\xe8ces'), ('check', 'Ch\xe8que'), ('transfer', 'Virement'), ('other', 'Autre')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='membershipfee',
            name='reference',
            field=models.CharField(help_text='num\xe9ro de ch\xe8que, r\xe9f\xe9rence de virement, commentaire...', max_length=125, null=True, verbose_name='r\xe9f\xe9rence du paiement'),
            preserve_default=True,
        ),
    ]
