# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0011_member_comments'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='date_last_call_for_membership_fees_email',
            field=models.DateTimeField(null=True, verbose_name='Date du dernier email de relance de cotisation envoy\xe9', blank=True),
            preserve_default=True,
        ),
    ]
