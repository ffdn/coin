# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0006_auto_20141008_1056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='start_date',
            field=models.DateField(verbose_name='date de d\xe9but de cotisation'),
        ),
    ]
