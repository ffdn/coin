# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0005_auto_20141008_1038'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='end_date',
            field=models.DateField(help_text='par d\xe9faut, la cotisation dure un an', verbose_name='date de fin de cotisation', blank=True),
        ),
    ]
