# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0018_auto_20180414_2250'),
        # Migration of MembershipFee from members app to billing app:
        ('billing', '0013_auto_20180415_0413'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='membershipfee',
            name='member',
        ),
        migrations.DeleteModel(
            name='MembershipFee',
        ),
    ]
