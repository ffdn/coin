# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0025_auto_20220219_1749'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='send_negative_balance_reminder_email',
            field=models.BooleanField(default=True, help_text="Pr\xe9cise si l'utilisateur doit recevoir des mails de relance lorsque la balance est trop n\xe9gative (script send_reminders_for_negative_balance)", verbose_name='relance si balance n\xe9gative'),
        ),
    ]
