# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0007_auto_20141008_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='nickname',
            field=models.CharField(default='', help_text='Pseudonyme, \\u2026', max_length=64, verbose_name="nom d'usage", blank=True),
            preserve_default=False,
        ),
    ]
