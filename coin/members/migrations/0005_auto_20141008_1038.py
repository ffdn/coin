# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0004_auto_20141007_1002'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='member',
            name='entry_date',
        ),
        migrations.AlterField(
            model_name='member',
            name='resign_date',
            field=models.DateField(help_text='En cas de d\xe9part pr\xe9matur\xe9', null=True, verbose_name="date de d\xe9part de l'association", blank=True),
        ),
    ]
