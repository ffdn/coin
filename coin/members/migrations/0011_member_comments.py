# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0010_auto_20141008_2246'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='comments',
            field=models.TextField(default='', help_text="Commentaires libres (informations sp\xe9cifiques concernant l'adh\xe9sion, raison du d\xe9part, etc)", verbose_name='commentaires', blank=True),
            preserve_default=False,
        ),
    ]
