# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0018_auto_20180819_0211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='balance',
            field=models.DecimalField(default=0, verbose_name='solde', max_digits=6, decimal_places=2),
        ),
    ]
