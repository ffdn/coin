# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0013_auto_20161015_1837'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='balance',
            field=models.DecimalField(default=0, verbose_name='account balance', max_digits=5, decimal_places=2),
        ),
    ]
