# -*- coding: utf-8 -*-

from django.db import migrations, models
import coin.members.models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0023_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='amount',
            field=models.DecimalField(default=coin.members.models.default_membership_fee, help_text='en \\u20ac', verbose_name='montant', max_digits=5, decimal_places=2),
        ),
    ]
