# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0021_auto_20181118_2001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='amount',
            field=models.DecimalField(default=15, help_text='en \\u20ac', verbose_name='montant', max_digits=5, decimal_places=2),
        ),
    ]
