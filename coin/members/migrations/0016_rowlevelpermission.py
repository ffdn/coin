# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('offers', '0007_offersubscription_comments'),
        ('members', '0015_auto_20170824_2308'),
    ]

    operations = [
        migrations.CreateModel(
            name='RowLevelPermission',
            fields=[
                ('permission_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='auth.Permission', on_delete=models.CASCADE)),
                ('description', models.TextField(blank=True)),
                ('offer', models.ForeignKey(verbose_name='Offre', to='offers.Offer', help_text="Offre dont l'utilisateur est autoris\xe9 \xe0 voir et modifier les membres et les abonnements.", null=True, on_delete=models.CASCADE)),
            ],
            bases=('auth.permission',),
        ),
    ]
