# -*- coding: utf-8 -*-

from django.db import models, migrations
import datetime
import coin.utils
from django.conf import settings
import django.utils.timezone
import coin.mixins
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, max_length=30, verbose_name='username', validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username.', 'invalid')])),
                ('first_name', models.CharField(max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(max_length=30, verbose_name='last name')),
                ('email', models.EmailField(unique=True, max_length=75, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('status', models.CharField(default='pending', max_length=50, verbose_name='statut', choices=[('member', 'Adh\xe9rent'), ('not_member', 'Non adh\xe9rent'), ('pending', "Demande d'adh\xe9sion")])),
                ('type', models.CharField(default='natural_person', max_length=20, verbose_name='type', choices=[('natural_person', 'Personne physique'), ('legal_entity', 'Personne morale')])),
                ('organization_name', models.CharField(help_text='Pour une personne morale', max_length=200, verbose_name="nom de l'organisme", blank=True)),
                ('home_phone_number', models.CharField(max_length=25, verbose_name='t\xe9l\xe9phone fixe', blank=True)),
                ('mobile_phone_number', models.CharField(max_length=25, verbose_name='t\xe9l\xe9phone mobile', blank=True)),
                ('address', models.TextField(null=True, verbose_name='adresse postale', blank=True)),
                ('postal_code', models.CharField(blank=True, max_length=5, null=True, verbose_name='code postal', validators=[django.core.validators.RegexValidator(regex='^\\d{5}$', message='Code postal non valide.')])),
                ('city', models.CharField(max_length=200, null=True, verbose_name='commune', blank=True)),
                ('country', models.CharField(default='France', max_length=200, null=True, verbose_name='pays', blank=True)),
                ('entry_date', models.DateField(default=datetime.date.today, verbose_name='date de premi\xe8re adh\xe9sion')),
                ('resign_date', models.DateField(null=True, verbose_name="date de d\xe9part de l'association", blank=True)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'membre',
            },
            bases=(models.Model,), # coin.mixins.CoinLdapSyncMixin, models.Model),
        ),
        migrations.CreateModel(
            name='CryptoKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=3, verbose_name='type', choices=[('RSA', 'RSA'), ('GPG', 'GPG')])),
                ('key', models.TextField(verbose_name='cl\xe9')),
                ('member', models.ForeignKey(verbose_name='membre', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'cl\xe9',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MembershipFee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField(default='20', help_text='en \\u20ac', verbose_name='montant')),
                ('start_date', models.DateField(default=datetime.date.today, verbose_name='date de d\xe9but de cotisation')),
                ('end_date', models.DateField(default=coin.utils.in_one_year, verbose_name='date de fin de cotisation')),
                ('member', models.ForeignKey(related_name='membership_fees', verbose_name='membre', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'cotisation',
            },
            bases=(models.Model,),
        ),
        #migrations.CreateModel(
        #    name='LdapUser',
        #    fields=[
        #    ],
        #    options={
        #        'managed': False,
        #    },
        #    bases=(models.Model,),
        #),
    ]
