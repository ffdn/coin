# -*- coding: utf-8 -*-

from django.db import models, migrations
from django.conf import settings

class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_auto_20141007_0121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='amount',
            field=models.IntegerField(default=settings.DEFAULT_MEMBERSHIP_FEE, help_text='en \\u20ac', verbose_name='montant'),
        ),
        migrations.AlterField(
            model_name='membershipfee',
            name='payment_date',
            field=models.DateField(null=True, verbose_name='date du paiement', blank=True),
        ),
        migrations.AlterField(
            model_name='membershipfee',
            name='payment_method',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='moyen de paiement', choices=[('cash', 'Esp\xe8ces'), ('check', 'Ch\xe8que'), ('transfer', 'Virement'), ('other', 'Autre')]),
        ),
        migrations.AlterField(
            model_name='membershipfee',
            name='reference',
            field=models.CharField(help_text='num\xe9ro de ch\xe8que, r\xe9f\xe9rence de virement, commentaire...', max_length=125, null=True, verbose_name='r\xe9f\xe9rence du paiement', blank=True),
        ),
    ]
