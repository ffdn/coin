# -*- coding: utf-8 -*-

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0020_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='MembershipFee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.DecimalField(default=20, help_text='en \\u20ac', verbose_name='montant', max_digits=5, decimal_places=2)),
                ('start_date', models.DateField(verbose_name='date de d\xe9but de cotisation')),
                ('end_date', models.DateField(help_text='par d\xe9faut, la cotisation dure un an', verbose_name='date de fin de cotisation', blank=True)),
                ('payment_method', models.CharField(blank=True, max_length=100, null=True, verbose_name='moyen de paiement', choices=[('cash', 'Esp\xe8ces'), ('check', 'Ch\xe8que'), ('transfer', 'Virement'), ('other', 'Autre')])),
                ('reference', models.CharField(help_text='num\xe9ro de ch\xe8que, r\xe9f\xe9rence de virement, commentaire...', max_length=125, null=True, verbose_name='r\xe9f\xe9rence du paiement', blank=True)),
                ('payment_date', models.DateField(null=True, verbose_name='date du paiement', blank=True)),
            ],
            options={
                'verbose_name': 'cotisation',
            },
        ),
        # Duplicate of 0019_auto_20190825_2329
        # migrations.AlterField(
        #     model_name='member',
        #     name='balance',
        #     field=models.DecimalField(default=0, verbose_name='solde', max_digits=6, decimal_places=2),
        # ),
        migrations.AddField(
            model_name='membershipfee',
            name='member',
            field=models.ForeignKey(related_name='membership_fees', verbose_name='membre', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
    ]
