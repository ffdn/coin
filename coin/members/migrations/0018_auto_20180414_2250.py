# -*- coding: utf-8 -*-

from django.db import migrations, models
import coin.members.models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0017_merge'),
    ]

    operations = [
        # Duplicate of 0018_auto_20180819_0211
        # migrations.AlterModelOptions(
        #     name='rowlevelpermission',
        #     options={'verbose_name': 'permission fine', 'verbose_name_plural': 'permissions fines'},
        # ),
        migrations.AlterModelManagers(
            name='member',
            managers=[
                ('objects', coin.members.models.MemberManager()),
            ],
        ),
        # Duplicate of 0019_auto_20190825_2329
        # migrations.AlterField(
        #     model_name='member',
        #     name='balance',
        #     field=models.DecimalField(default=0, verbose_name='account balance', max_digits=6, decimal_places=2),
        # ),
    ]
