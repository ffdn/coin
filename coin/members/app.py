# -*- coding: utf-8 -*-

from django.apps import AppConfig

class MembersConfig(AppConfig):
    name = 'coin.members'
    verbose_name = 'Membres'
    admin_menu_entry = {
        "id": "members",
        "icon": "users",
        "title": "Membres",
        "models": [
            ("Membres", "members/member"),
            ("Groupes", "auth/group"),
            ("Permissions", "members/rowlevelpermission"),
        ]
    }
