# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.conf import settings
from .forms import PersonMemberChangeForm, OrganizationMemberChangeForm
from coin.billing.models import Bill

from coin.offers.models import Offer, OfferSubscriptionRequest

@login_required
def index(request):
    has_isp_feed = 'isp' in [k for k, _, _ in settings.FEEDS]
    return render(request,
                  'members/index.html',
                  {'has_isp_feed': has_isp_feed})


@login_required
def detail(request):

    if request.user.type == "natural_person":
        form_cls = PersonMemberChangeForm
    else:
        form_cls = OrganizationMemberChangeForm

    if request.method == "POST":
        form = form_cls(data = request.POST, instance = request.user)
        if form.is_valid():
            form.save()
            return redirect(request.path)
    else:
        form = form_cls(instance = request.user)

    return render(request,
                  'members/detail.html',
                  {'membership_info_url': settings.MEMBER_MEMBERSHIP_INFO_URL,
                   'form': form})


@login_required
def subscriptions(request):
    subscriptions = request.user.get_active_subscriptions()
    old_subscriptions = request.user.get_inactive_subscriptions()
    pending_subscriptions = OfferSubscriptionRequest.objects.filter(member=request.user, state="pending")

    return render(request,
                  'members/subscriptions.html',
                  {'can_request_subscription': Offer.objects.filter(requestable=True).exists(),
                   'subscriptions': subscriptions,
                   'old_subscriptions': old_subscriptions,
                   'pending_subscriptions': pending_subscriptions})


@login_required
def invoices(request):
    balance  = request.user.balance
    invoices = Bill.get_member_validated_bills(request.user)
    invoices.reverse()
    payments = request.user.payments.filter().order_by('-date')

    return render(request,
                  'members/invoices.html',
                  {'balance' : balance,
                   'handle_balance' : settings.HANDLE_BALANCE,
                   'invoices': invoices,
                   'payments': payments,
                   'member': request.user})


@login_required
def contact(request):
    return render(request, 'members/contact.html')


@login_required
def activation_completed(request):

    return render(request,
                  'members/registration/activation_complete.html',
                  {'member': request.user,
                   'dues': settings.DEFAULT_MEMBERSHIP_FEE
                  })
