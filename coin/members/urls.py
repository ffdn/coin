from django.urls import path
from coin.members import forms, views
from django.views.generic.base import TemplateView
from . import registration_views as views_r
from coin import settings

from registration.signals import user_activated
from django.contrib.auth import login
from coin.offers.views import offersubscriptionrequest_step1, offersubscriptionrequest_step2, offersubscriptionrequest_step3
import django.contrib.auth.views as auth_views

def login_on_activation(sender, user, request, **kwargs):
    """Logs in the user after activation"""
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    login(request, user)

# Registers the function with the django-registration user_activated signal
user_activated.connect(login_on_activation)

app_name = 'coin.members'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', auth_views.LoginView.as_view(template_name='members/registration/login.html', extra_context={'settings': settings}), name='login'),
    path('logout/', auth_views.logout_then_login, name='logout'),

    path('password_change/', auth_views.PasswordChangeView.as_view(success_url='/members/password_change/done', template_name='members/registration/password_change_form.html'), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(template_name='members/registration/password_change_done.html'), name='password_change_done'),

    path('password_reset/', auth_views.PasswordResetView.as_view(success_url='/members/password_reset/done', template_name='members/registration/password_reset_form.html', email_template_name='members/registration/password_reset_email.html', subject_template_name='members/registration/password_reset_subject.txt'), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='members/registration/password_reset_done.html'), name='password_reset_done'),
    path('password_reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(success_url='/members/password_reset/complete', template_name='members/registration/password_reset_confirm.html'), name='password_reset_confirm'),
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='members/registration/password_reset_complete.html'), name='password_reset_complete'),

    path('activate/complete/', views.activation_completed, name='registration_activation_complete'),
    # The activation key can make use of any character from the
    # URL-safe base64 alphabet, plus the colon as a separator.
    path('activate/<activation_key>/', views_r.MemberActivationView.as_view(), name='registration_activate'),
    path('register/',
        views_r.MemberRegistrationView.as_view(
            form_class=forms.MemberRegistrationForm,
            template_name='members/registration/registration_form.html'
        ),
        name='registration_register'),
    path('register/complete/',
        TemplateView.as_view(
            template_name='members/registration/registration_complete.html'
        ),
        name='registration_complete'),
    path('register/closed/',
        TemplateView.as_view(
            template_name='members/registration/registration_closed.html'
        ),
        name='registration_disallowed'),
    #path(r'', include('registration.auth_urls')),




    path('detail/', views.detail,
        name='detail'),

    path('subscriptions/', views.subscriptions, name='subscriptions'),
    # path('subscription/(?P<id>\d+)', views.subscriptions, name = 'subscription'),
    path('request_subscriptions/step1', offersubscriptionrequest_step1, name="subscriptionrequest_step1"),
    path('request_subscriptions/step2/<offer_type>', offersubscriptionrequest_step2, name="subscriptionrequest_step2"),
    path('request_subscriptions/step3', offersubscriptionrequest_step3, name="subscriptionrequest_step3"),

    path('invoices/', views.invoices, name='invoices'),
    path('contact/', views.contact, name='contact'),
]
