# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vpn', '0003_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vpnconfiguration',
            name='activated',
            field=models.BooleanField(default=True, verbose_name='activ\xe9'),
        ),
    ]
