# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vpn', '0003_merge'),
    ]

    operations = [
        # Duplicate of 0004_auto_20190825_2329.py
        # migrations.AlterField(
        #     model_name='vpnconfiguration',
        #     name='activated',
        #     field=models.BooleanField(default=True, verbose_name='activ\xe9'),
        # ),
    ]
