# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vpn', '0001_squashed_0002_remove_vpnconfiguration_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='vpnconfiguration',
            name='crypto_link',
            field=models.URLField(help_text='Lien \xe0 usage unique (d\xe9truit apr\xe8s ouverture)', null=True, verbose_name='Mat\xe9riel cryptographique', blank=True),
        ),
    ]
