# -*- coding: utf-8 -*-

from django.db import migrations, models
import netfields.fields
import coin.validation


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0011_auto_20200717_1733'),
        ('vpn', '0005_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='VPNSubscriptionRequest',
            fields=[
                ('offersubscriptionrequest_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='offers.OfferSubscriptionRequest', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'demande de VPN',
                'verbose_name_plural': 'demandes de VPN',
            },
            bases=('offers.offersubscriptionrequest',),
        ),
        migrations.RemoveField(
            model_name='vpnconfiguration',
            name='activated',
        ),
        migrations.AlterField(
            model_name='vpnconfiguration',
            name='ipv4_endpoint',
            field=netfields.fields.InetAddressField(validators=[coin.validation.validate_v4], max_length=39, blank=True, help_text='Adresse IPv4 utilis\xe9e comme endpoint', null=True, verbose_name='IPv4'),
        ),
        migrations.AlterField(
            model_name='vpnconfiguration',
            name='ipv6_endpoint',
            field=netfields.fields.InetAddressField(validators=[coin.validation.validate_v6], max_length=39, blank=True, help_text='Adresse IPv6 utilis\xe9e comme endpoint', null=True, verbose_name='IPv6'),
        ),
    ]
