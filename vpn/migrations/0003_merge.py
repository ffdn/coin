# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vpn', '0002_auto_20170802_2021'),
        ('vpn', '0002_vpnconfiguration_crypto_link'),
    ]

    operations = [
    ]
