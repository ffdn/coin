# -*- coding: utf-8 -*-

from django.db import models
from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse
from netfields import InetAddressField, NetManager

from coin.configuration.models import IPConfiguration
from coin.offers.models import OfferSubscriptionRequest
from coin import utils


class VPNConfiguration(IPConfiguration):

    url_namespace = "vpn"

    login = models.CharField(max_length=50, unique=True, blank=True,
                             verbose_name="identifiant",
                             help_text="Laisser vide pour une génération automatique")
    password = models.CharField(max_length=256, verbose_name="mot de passe",
                                blank=True, null=True)
    crypto_link = models.URLField(verbose_name="Matériel cryptographique", blank=True,
                           null=True, help_text="Lien à usage unique (détruit après ouverture)")

    def get_absolute_url(self):
        return reverse('vpn:details', args=[str(self.pk)])

    def fill_empty_fields(self):
        # Generate VPN login, of the form "login-vpnX".  The resulting
        # login should not contain any ".", because graphite uses "." as a
        # separator.
        # Hash password if needed
        if not self.login:
            self.login = "vpn{id}".format(id=self.pk)
        #self.password = utils.ldap_hash(self.password)
        super().fill_empty_fields()

    def clean(self):
        super().clean()
        # We may have failed.
        if not self.login:
            ValidationError("Impossible de générer un login VPN")

    def convert_to_dict_for_hook(self):
        d = super().convert_to_dict_for_hook()
        d['login'] = self.login
        return d

    def __str__(self):
        return 'VPN ' + self.login

    class Meta:
        verbose_name = 'VPN'
        verbose_name_plural = 'VPN'
        app_label = "vpn"


class VPNSubscriptionRequest(OfferSubscriptionRequest):

    configuration_class = VPNConfiguration

    class Meta:
        verbose_name = 'demande de VPN'
        verbose_name_plural = 'demandes de VPN'
        app_label = "vpn"
