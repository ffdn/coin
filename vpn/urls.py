# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import VPNView, VPNGeneratePasswordView

app_name = "vpn"

urlpatterns = [
    #'',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', VPNView.as_view(template_name="vpn/vpn.html"), name="details"),
    url(r'^password/(?P<pk>\d+)$', VPNGeneratePasswordView.as_view(template_name="vpn/fragments/password.html"), name="generate_password"),
]
