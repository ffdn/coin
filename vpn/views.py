# -*- coding: utf-8 -*-

import os

from django.http import StreamingHttpResponse, HttpResponseServerError
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from coin.members.models import Member

from .models import VPNConfiguration


class VPNView(SuccessMessageMixin, UpdateView):
    model = VPNConfiguration
    fields = ['ipv4_endpoint', 'ipv6_endpoint', 'comment']
    success_message = "Configuration enregistrée avec succès !"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        if settings.MEMBER_CAN_EDIT_VPN_CONF:
            return super().get_form(form_class)
        return None

    def get_object(self):
        if self.request.user.is_superuser:
            return get_object_or_404(VPNConfiguration, pk=self.kwargs.get("pk"))
        # For normal users, ensure the VPN belongs to them.
        return get_object_or_404(VPNConfiguration, pk=self.kwargs.get("pk"),
                                 offersubscription__member=self.request.user)


class VPNGeneratePasswordView(VPNView):
    """This generates a random password, saves it in hashed form, and returns
    it to the user in cleartext.
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Generate a new random password and save it
        password = Member.objects.make_random_password()
        self.object.password = password
        # This will hash the password automatically
        self.object.full_clean()
        self.object.save()
        context['password'] = password
        return context
