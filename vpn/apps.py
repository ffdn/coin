# -*- coding: utf-8 -*-

from django.apps import AppConfig
import coin.apps

#from . import urls


class VPNConfig(AppConfig, coin.apps.AppURLs):
    name = 'vpn'
    verbose_name = "Tunnels VPN"

    exported_urlpatterns = [('vpn', 'vpn.urls')]

    admin_menu_addons = {
        'configs': [
            ("Tunnel VPN", "vpn/vpnconfiguration"),
        ]
    }
