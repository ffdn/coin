FROM python:3.8-bullseye

ENV DEBIAN_FRONTEND noninteractive
ENV TZ="Europe/Paris"
ENV LC_ALL fr_FR.UTF-8

RUN apt-get update \
    && apt-get install -y --no-install-recommends libsasl2-dev python3-dev libldap2-dev libssl-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /coin
COPY . .
RUN pip install -r requirements.txt

RUN useradd --uid 10001 --user-group --shell /bin/bash coin
RUN chown coin:coin /coin

USER coin:coin

VOLUME ["/coin"]
