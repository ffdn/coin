# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0008_auto_20160405_2234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='notes',
            field=models.TextField(null=True, verbose_name='emplacement', blank=True),
            preserve_default=True,
        ),
    ]
