# -*- coding: utf-8 -*-

from django.db import models, migrations
import hardware_provisioning.fields


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0004_auto_20160405_1816'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='serial',
            field=models.CharField(help_text='ou toute autre r\xe9f\xe9rence unique)', max_length=250, verbose_name='N\xb0 de s\xe9rie', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='mac_address',
            field=hardware_provisioning.fields.MACAddressField(help_text='pr\xe9f\xe9rable au n\xb0 de s\xe9rie si possible', max_length=17, null=True, verbose_name='addresse MAC', blank=True),
            preserve_default=True,
        ),
    ]
