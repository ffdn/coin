# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0016_auto_20170802_2021'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='deployed',
            field=models.BooleanField(default=False, help_text='Cocher si le mat\xe9riel est en production', verbose_name='d\xe9ploy\xe9'),
        ),
    ]
