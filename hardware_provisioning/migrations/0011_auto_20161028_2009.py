# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0010_auto_20160405_2237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='loan_date',
            field=models.DateTimeField(verbose_name='date de pr\xeat'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='loan',
            name='loan_date_end',
            field=models.DateTimeField(null=True, verbose_name='date de fin de pr\xeat', blank=True),
            preserve_default=True,
        ),
    ]
