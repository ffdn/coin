# -*- coding: utf-8 -*-

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0018_auto_20180819_0211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='loan_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='date de pr\xeat'),
        ),
    ]
