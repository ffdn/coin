# -*- coding: utf-8 -*-

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0017_item_deployed'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='item',
            options={'ordering': ['designation', 'mac_address', 'serial'], 'verbose_name': 'objet'},
        ),
        migrations.AlterField(
            model_name='loan',
            name='user',
            field=models.ForeignKey(related_name='loans', on_delete=django.db.models.deletion.SET_NULL, verbose_name='membre', to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
