# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0012_auto_20161110_2225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='serial',
            field=models.CharField(help_text='ou toute autre r\xe9f\xe9rence unique)', unique=True, max_length=250, verbose_name='N\xb0 de s\xe9rie', blank=True, null=True),
            preserve_default=True,
        ),
    ]
