# -*- coding: utf-8 -*-

from django.db import models, migrations
import hardware_provisioning.fields


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0011_auto_20161028_2009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='mac_address',
            field=hardware_provisioning.fields.MACAddressField(null=True, max_length=17, blank=True, help_text='pr\xe9f\xe9rable au n\xb0 de s\xe9rie si possible', unique=True, verbose_name='addresse MAC'),
            preserve_default=True,
        ),
    ]
