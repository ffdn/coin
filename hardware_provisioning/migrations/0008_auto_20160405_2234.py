# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0007_item_storage'),
    ]

    operations = [
        migrations.RenameField('loan', 'location', 'notes')
    ]
