# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0002_auto_20150625_2313'),
    ]

    operations = [
        migrations.RenameField('item', 'user_in_charge', 'owner')
    ]
