# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0005_auto_20160405_1841'),
    ]

    operations = [
        migrations.CreateModel(
            name='Storage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='nom')),
                ('notes', models.TextField(help_text='Lisible par tous les adh\xe9rents', blank=True)),
            ],
            options={
                'verbose_name': 'lieu de stockage',
                'verbose_name_plural': 'lieux de stockage',
            },
            bases=(models.Model,),
        ),
    ]
