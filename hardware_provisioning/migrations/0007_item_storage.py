# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_provisioning', '0006_storage'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='storage',
            field=models.ForeignKey(related_name='items', blank=True, to='hardware_provisioning.Storage', help_text='Laisser vide si inconnu', null=True, verbose_name='Lieu de stockage', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
