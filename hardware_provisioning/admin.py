# -*- coding: utf-8 -*-


from django.conf.urls import url
from django.shortcuts import get_object_or_404
from django.contrib import admin, messages
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.http import HttpResponseRedirect

from .models import ItemType, Item, Loan, Storage
import coin.members.admin


User = get_user_model()

admin.site.register(ItemType)


def _give_back_success_messages(items, request):
    for item in items:
        messages.success(request, "{} a bien été marqué comme rendu.".format(item))
    if len(items) > 1:
        messages.warning(
            request,
            "N'oubliez pas de renseigner, pour chaque retour, le nouvel emprunteur "
            + "ou le nouveau lieu de stockage.",
        )
    else:
        messages.warning(
            request,
            "N'oubliez pas de renseigner le nouvel emprunteur "
            + "ou le nouveau lieu de stockage.",
        )



def give_back_loan(request, id):
    # could be better : could not be a POST, and could not rely on HTTP_REFERER
    # We could for that rely on django-inline-actions, but the version
    # we could use with our Django 1.9 is outdated and buggy
    # We could also offer an intermediate page to specify the storage
    loan = get_object_or_404(Loan, pk=id)
    try:
        redirect_url = request.META['HTTP_REFERER']
    except KeyError:
        # Fallback if no referer header is present
        redirect_url = reverse(
            'admin:hardware_provisioning_item_change',
            args=[loan.item.id]
        )

    loan.item.give_back()
    _give_back_success_messages([loan.item], request)

    return HttpResponseRedirect(redirect_url)


class OwnerFilter(admin.SimpleListFilter):
    title = "propriétaire"
    parameter_name = 'owner'

    def lookups(self, request, model_admin):
        owners = [
            (i.pk, i) for i in User.objects.filter(items__isnull=False)]

        return [(None, "L'association")] + owners

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(owner__pk=self.value())
        else:
            return queryset


class AvailabilityFilter(admin.SimpleListFilter):
    title = "Disponibilité"
    parameter_name = 'availability'

    def lookups(self, request, model_admin):
        return [
            ('available', 'Disponible'),
            ('borrowed', 'Emprunté'),
            ('deployed', 'Déployé'),
        ]

    def queryset(self, request, queryset):
        if self.value() == 'available':
            return queryset.available()
        elif self.value() == 'borrowed':
            return queryset.borrowed()
        elif self.value() == 'deployed':
            return queryset.deployed()
        else:
            return queryset


class CurrentLoanInline(admin.TabularInline):
    model = Loan
    extra = 0
    fields = ('user', 'item', 'short_date', 'notes', 'action_buttons')
    readonly_fields = ('user', 'item', 'short_date', 'notes', 'action_buttons')
    verbose_name_plural = "Prêt en cours"
    show_change_link = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.running()

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def action_buttons(self, obj):
        if obj.is_running():
            return """<a class="button "href="{}">Déclarer rendu</a>""".format(
                reverse('admin:loan-give_back', args=[obj.pk]))
        else:
            return ''
    action_buttons.short_description = 'Actions'
    action_buttons.allow_tags = True


class LoanHistoryInline(admin.TabularInline):
    model = Loan
    extra = 0
    fields = ('user', 'item', 'short_date', 'short_date_end', 'notes')
    readonly_fields = ('user', 'item', 'short_date', 'short_date_end', 'notes')
    ordering = ['-loan_date_end']
    verbose_name_plural = "Historique de prêt de cet objet"
    empty_value_display = '(membre effacé)'  # Django >= 1.10-ready
    show_change_link = True
    classes = ['collapse']  # Django >= 1.10-ready

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.finished()

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class AddLoanInline(admin.StackedInline):
    model = Loan
    extra = 1
    max_num = 1
    fields = ('user', 'item', 'loan_date', 'notes')
    verbose_name_plural = "Déclarer le prêt de cet objet"
    classes = ['collapse']  # Django >= 1.10-ready

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.none()

    def has_delete_permission(self, request, obj=None):
        return False


class BorrowerFilter(admin.SimpleListFilter):
    title = 'détenteur actuel'
    parameter_name = 'user'

    def _filter_loans(self, items_queryset, user_pk=None):
        qs = Loan.objects.running().filter(item__in=items_queryset)
        if user_pk is not None:
            qs.filter(user=user_pk)
        return qs

    def lookups(self, request, model_admin):
        # Get relevant (and authorized) users only
        relevant_items = model_admin.get_queryset(request)
        users = set()
        for loan in self._filter_loans(relevant_items):
            if loan.user:  # can be empty for deleted user
                users.add((loan.user.pk, loan.user))
        return users

    def queryset(self, request, queryset):
        if self.value():
            loans_qs = self._filter_loans(queryset).filter(
                user__pk=self.value(),
            )
            return queryset.filter(loans__in=loans_qs)
        else:
            return queryset


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        'designation',
        'current_borrower',
        'get_mac_and_serial',
        'deployed', 'is_available', 'storage',
        'buy_date', 'owner',
    )
    list_filter = (
        AvailabilityFilter, 'type', 'storage',
        'buy_date', BorrowerFilter, OwnerFilter)
    search_fields = (
        'designation', 'mac_address', 'serial',
        'owner__email', 'owner__nickname',
        'owner__first_name', 'owner__last_name')
    save_as = True
    actions = ['give_back']

    inlines = [AddLoanInline, CurrentLoanInline, LoanHistoryInline]

    def give_back(self, request, queryset):
        items = queryset.filter(loans__loan_date_end=None)
        for item in items:
            item.give_back()
        _give_back_success_messages(items, request)
    give_back.short_description = 'Rendre le matériel'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            url(
                r'^give_back/(?P<id>.+)$',
                self.admin_site.admin_view(give_back_loan),
                name='loan-give_back'),
        ]
        return my_urls + urls


class StatusFilter(admin.SimpleListFilter):
    title = 'Statut'
    parameter_name = 'status'

    def lookups(self, request, model_admin):
        return [
            ('all', 'Tout'),
            (None, 'En cours'),
            ('finished', 'Passés'),
        ]

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        v = self.value()
        if v in (None, 'running'):
            return queryset.running()
        elif v == 'finished':
            return queryset.finished()
        else:
            return queryset


@admin.register(Storage)
class StorageAdmin(admin.ModelAdmin):
    list_display = ('name', 'truncated_notes', 'items_count')

    def truncated_notes(self, obj):
        if len(obj.notes) > 50:
            return '{}…'.format(obj.notes[:50])
        else:
            return obj.notes
    truncated_notes.short_description = 'notes'


class MemberLoanHistoryInline(LoanHistoryInline):
    verbose_name_plural = "Historique de prêt de matériel"


class MemberCurrentLoanInline(CurrentLoanInline):
    verbose_name_plural = "Prêts de matériel en cours"


class MemberAddLoanInline(AddLoanInline):
    extra = 0
    max_num = 10
    verbose_name_plural = "Déclarer un prêt de matériel"


# Enrich the MemberAdmin with hardware-related information
_MemberAdmin = admin.site._registry[coin.members.admin.Member].__class__


class MemberAdmin(_MemberAdmin):
    inlines = _MemberAdmin.inlines + [
        MemberCurrentLoanInline,
        MemberAddLoanInline,
        MemberLoanHistoryInline,
    ]


admin.site.unregister(coin.members.admin.Member)
admin.site.register(coin.members.admin.Member, MemberAdmin)
