# -*- coding: utf-8 -*-

from django.apps import AppConfig
import coin.apps


class HardwareProvisioningConfig(AppConfig, coin.apps.AppURLs):
    name = 'hardware_provisioning'
    verbose_name = 'Prêt de matériel'
    exported_urlpatterns = [('hardware_provisioning', 'hardware_provisioning.urls')]

    admin_menu_entry = {
        "id": "hardware",
        "icon": "cubes",
        "title": "Matos",
        "models": [
            ("Lieux de stockage", "hardware_provisioning/storage"),
            ("Type d'objet", "hardware_provisioning/itemtype"),
            ("Objets", "hardware_provisioning/item"),
        ]
    }
