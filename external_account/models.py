# -*- coding: utf-8 -*-

from django.db import models
from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse
from netfields import InetAddressField, NetManager

from coin.configuration.models import Configuration
from coin.offers.models import OfferSubscriptionRequest
from coin import utils


class ExternalAccountConfiguration(Configuration):

    url_namespace = "external_account"

    login = models.CharField(max_length=50, unique=True, blank=True,
                             verbose_name="identifiant")

    def get_absolute_url(self):
        return reverse('external_account:details', args=[str(self.pk)])

    def __str__(self):
        return "%s pour %s" % (self.offersubscription.offer, self.offersubscription.member)

    def convert_to_dict_for_hook(self):
        d = super().convert_to_dict_for_hook()
        d["login"] = self.login
        d['mail'] = self.offersubscription.member.email
        d["firstname"] = 'Asso'
        if self.offersubscription.member.first_name:
            d["firstname"] = self.offersubscription.member.first_name
        d["lastname"] = self.offersubscription.member.organization_name
        if self.offersubscription.member.last_name:
            d["lastname"] = self.offersubscription.member.last_name
        return d

    def save(self, **kwargs):
        if self.login == '':
            login = self.offersubscription.member.username.lower()
            self.login = login
            i=2
            while ExternalAccountConfiguration.objects.filter(login=self.login).count() > 0:
                self.login = login + str(i)
                i += 1
        config = super().save(**kwargs)
        return config
    class Meta:
        verbose_name = settings.VERBOSE_NAME_EXTERNAL_ACCOUNT
        verbose_name_plural = settings.VERBOSE_NAME_PLURAL_EXTERNAL_ACCOUNT
        app_label = "external_account"


class ExternalAccountSubscriptionRequest(OfferSubscriptionRequest):

    configuration_class = ExternalAccountConfiguration

    class Meta:
        verbose_name = 'demande de compte externe'
        verbose_name_plural = 'demandes de compte externe'
        app_label = "external_account"
