# -*- coding: utf-8 -*-

from django.contrib import admin

from coin.configuration.admin import ChildConfigurationAdmin, ChildConfigurationAdminInline
from coin.offers.admin import ChildOfferSubscriptionRequestAdmin
from coin.utils import delete_selected

from .models import ExternalAccountConfiguration, ExternalAccountSubscriptionRequest


class ExternalAccountConfigurationInline(ChildConfigurationAdminInline):
    model = ExternalAccountConfiguration
    readonly_fields = ['configuration_ptr']

    specific_fields = ["login"]


class ExternalAccountConfigurationAdmin(ChildConfigurationAdmin):
    base_model = ExternalAccountConfiguration
    list_display = ('__str__', 'offersubscription', 'get_state_icon_display', 'comment')

    actions = (delete_selected,)
    inline = ExternalAccountConfigurationInline

    specific_fields = ["login"]


class ExternalAccountSubscriptionRequestAdmin(ChildOfferSubscriptionRequestAdmin):

    base_model = ExternalAccountSubscriptionRequest


admin.site.register(ExternalAccountConfiguration, ExternalAccountConfigurationAdmin)
admin.site.register(ExternalAccountSubscriptionRequest, ExternalAccountSubscriptionRequestAdmin)
