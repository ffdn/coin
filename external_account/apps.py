# -*- coding: utf-8 -*-

from django.apps import AppConfig
import coin.apps



class ExternalAccountConfig(AppConfig, coin.apps.AppURLs):
    name = 'external_account'
    verbose_name = "Compte externe"

    exported_urlpatterns = [('external_account', 'external_account.urls')]

    admin_menu_addons = {
        'configs': [
            ("Comptes externes", "external_account/externalaccountconfiguration"),
        ]
    }
