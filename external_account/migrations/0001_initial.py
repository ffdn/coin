# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0005_auto_20200717_1733'),
        ('offers', '0011_auto_20200717_1733'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExternalAccountConfiguration',
            fields=[
                ('configuration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='configuration.Configuration', on_delete=models.CASCADE)),
                ('login', models.CharField(unique=True, max_length=50, verbose_name='identifiant', blank=True)),
            ],
            options={
                'verbose_name': 'Compte externe',
                'verbose_name_plural': 'Comptes externe',
            },
            bases=('configuration.configuration',),
        ),
        migrations.CreateModel(
            name='ExternalAccountSubscriptionRequest',
            fields=[
                ('offersubscriptionrequest_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='offers.OfferSubscriptionRequest', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'demande de compte externe',
                'verbose_name_plural': 'demandes de compte externe',
            },
            bases=('offers.offersubscriptionrequest',),
        ),
    ]
