# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import ExternalAccountView

app_name = "external_account"

urlpatterns = [
    #'',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', ExternalAccountView.as_view(template_name="external_account/external_account.html"), name="details"),
]
