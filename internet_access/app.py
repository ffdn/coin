# -*- coding: utf-8 -*-

from django.apps import AppConfig
import coin.apps

class InternetAccessConfig(AppConfig, coin.apps.AppURLs):
    name = 'internet_access'
    verbose_name = 'Accès Internet'

    exported_urlpatterns = [('optic_fiber', 'internet_access.urls')]

    admin_menu_addons = {
        'configs': [
            ("Accès Internet", "internet_access/opticfiberconfiguration"),
        ]
    }

