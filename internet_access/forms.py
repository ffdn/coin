# -*- coding: utf-8 -*-

from django import forms

from coin.offers.forms import OfferSubscriptionRequestStep2Form
from .models import OpticFiberSubscriptionRequest


class OpticFiberSubscriptionRequestStep2Form(OfferSubscriptionRequestStep2Form):

    offer_type = "Fibre optique"
    subscriptionrequest_class = OpticFiberSubscriptionRequest

    address = forms.CharField(required=True, label="Adresse", max_length=1024, widget=forms.Textarea(attrs={'style': 'height: 10em;'}),
        help_text="Pour les appartements, merci d'indiquer aussi l'étage et la porte de l'appartement")
    postal_code = forms.CharField(required=True, label="Code postal")
    city = forms.CharField(required=True, label="Commune")
    building_reference = forms.CharField(label="Référence batiment", required=False,
                help_text="La référence de votre batiment si vous la connaissez")
    mac_address = forms.CharField(required=False, label="Adresse mac",
                                   help_text="Indiquez l'adresse mac de la box/routeur que vous souhaitez utiliser. Laissez vide si vous souhaitez un routeur en réemplois fournis par l'association.")

    appointment_phone_number = forms.CharField(required=True, label="Téléphone",
                                    help_text="Numéro de téléphone pour le jour du rendez-vous avec le ou la raccordeuse")
    appointment_availibility = forms.CharField(required=True, label="Disponibilités",max_length=1024, widget=forms.Textarea(attrs={'style': 'height: 10em;'} ),
        help_text="Une liste de demi journée disponibles en semaine sur les 15 prochains jours pour la prise de rendez-vous")
    
    def create_offersubscriptionrequest(self, request):
        subscriptionrequest = super(OpticFiberSubscriptionRequestStep2Form, self).create_offersubscriptionrequest(request)
        subscriptionrequest.address = self.cleaned_data["address"]
        subscriptionrequest.postal_code = self.cleaned_data["postal_code"]
        subscriptionrequest.city = self.cleaned_data["city"]
        subscriptionrequest.building_reference = self.cleaned_data["building_reference"]
        subscriptionrequest.mac_address = self.cleaned_data["mac_address"]
        subscriptionrequest.appointment_phone_number = self.cleaned_data["appointment_phone_number"]
        subscriptionrequest.appointment_availibility = self.cleaned_data["appointment_availibility"]
        subscriptionrequest.save()
        return subscriptionrequest
