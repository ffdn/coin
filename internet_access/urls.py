# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import OpticFiberView

app_name = "internet_access"

urlpatterns = [
    #'',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', OpticFiberView.as_view(template_name="optic_fiber/optic_fiber.html"), name="details"),
]
