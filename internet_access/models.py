# -*- coding: utf-8 -*-

from django.db import models

from coin.configuration.models import Configuration

from django.core.validators import RegexValidator
from coin.offers.models import OfferSubscriptionRequest
from netfields import InetAddressField, NetManager

from coin import validation

class InternetAccessParameters(models.Model):

    address = models.TextField(
        verbose_name='adresse postale', blank=True, null=True,
        help_text="Pour les appartements, merci d'indiquer aussi l'étage et la porte de l'appartement")
    postal_code = models.CharField(max_length=5, blank=True, null=True,
                                   validators=[RegexValidator(regex=r'^\d{5}$',
                                                              message='Code postal non valide.')],
                                   verbose_name='code postal')
    city = models.CharField(max_length=200, blank=True, null=True,
                            verbose_name='commune')
    building_reference = models.CharField(max_length=50, blank=True, null=True,
                            verbose_name='Référence batiment',
                help_text="La référence de votre batiment si vous la connaissez")
    mac_address = models.CharField(max_length=17, blank=True, null=True,
                                   validators=[RegexValidator(regex=r'^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$',
                                                              message='Adresse mac non valide.')],
                                   help_text="Indiquez l'adresse mac de la box/routeur que vous souhaitez utiliser. Laissez vide si vous souhaitez un routeur en réemplois fournis par l'association.",
                                   verbose_name='adresse mac du routeur')

    class Meta:
        abstract = True
        app_label = "internet_access"

#class WifiParameters(InternetAccessParameters):
#    roof_access = models.BooleanField(verbose_name='Je peux accèder à mon toit')
#    level = models.BooleanField(verbose_name='Etage')
#    level_number = models.BooleanField(verbose_name="Nombre d'étages")
#
#class DSLParameters(InternetAccessParameters):
#    dsl_phone_number = models.CharField(max_length=20,
#                                    verbose_name='Numéro de tel fixe',
#                                    help_text="Si vous souhaitez du xDSL, le numéro de téléphone fixe auxquels associer la ligne. Laisser vide pour une fibre optique.")

class OpticFiberParameters(InternetAccessParameters):
    appointment_phone_number = models.CharField(max_length=20,
                                    verbose_name='telephone',
                                    help_text="Numéro de téléphone pour le jour du rendez-vous avec le ou la raccordeuse")
    appointment_availibility = models.TextField(
        verbose_name='disponibilités', blank=True, null=True,
        help_text="Une liste de demi journée disponibles en semaine sur les 15 prochains jours pour la prise de rendez-vous")
    class Meta:
        abstract = True


class OpticFiberConfiguration(Configuration, OpticFiberParameters):
    """Very simple DSL model, mostly to demonstrate the use of the generic
    functionality of COIN.  There is no real configuration backend, and no
    authentication data.  But this still allows to track the phone number
    and IP addresses of subscribers, which may be useful for "white label"
    DSL reselling.
    """
    ipv4_endpoint = InetAddressField(validators=[validation.validate_v4],
                                     verbose_name="IPv4", blank=True, null=True,
                                     help_text="Adresse IPv4")
    ipv6_endpoint = InetAddressField(validators=[validation.validate_v6],
                                     verbose_name="IPv6", blank=True, null=True,
                                     help_text="Adresse IPv6 utilisée")

    class Meta:
        verbose_name = 'Fibre optique'
        verbose_name_plural = 'Fibres optiques'
        app_label = "internet_access"

    url_namespace = "optic_fiber"

    def subnet_event(self):
        # Do something with self.ip_subnet.all() here.
        pass
    def get_absolute_url(self):
        return reverse('optic_fiber:details', args=[str(self.pk)])

    def __str__(self):
        return "%s pour %s" % (self.offersubscription.offer, self.offersubscription.member)


class OpticFiberSubscriptionRequest(OfferSubscriptionRequest, OpticFiberParameters):

    configuration_class = OpticFiberConfiguration

    class Meta:
        verbose_name = 'demande d\'accès internet fibre optique'
        verbose_name_plural = 'demandes d\'accès internet fibre optique'
        app_label = "internet_access"
