# -*- coding: utf-8 -*-

from django.shortcuts import render
import os

from django.http import StreamingHttpResponse, HttpResponseServerError
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .models import OpticFiberConfiguration
from .forms import OpticFiberSubscriptionRequestStep2Form

# Create your views here.

class OpticFiberView(SuccessMessageMixin, UpdateView):
    model = OpticFiberConfiguration
    fields = ['comment']
    success_message = "Configuration enregistrée avec succès !"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(OpticFiberView, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        if settings.MEMBER_CAN_EDIT_VPN_CONF:
            return super(OpticFiberView, self).get_form(form_class)
        return None

    def get_object(self):
        if self.request.user.is_superuser:
            return get_object_or_404(OpticFiberConfiguration, pk=self.kwargs.get("pk"))
        # For normal users, ensure the VPN belongs to them.
        return get_object_or_404(OpticFiberConfiguration, pk=self.kwargs.get("pk"),
                                 offersubscription__member=self.request.user)

