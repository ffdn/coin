# -*- coding: utf-8 -*-

from django.db import migrations, models
import netfields.fields
import coin.validation
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0005_auto_20200717_1733'),
        ('configuration', '0006_auto_20201030_1745'),
        ('offers', '0011_auto_20200717_1733'),
        ('internet_access', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpticFiberConfiguration',
            fields=[
                ('configuration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='configuration.Configuration', on_delete=models.CASCADE)),
                ('address', models.TextField(help_text="Pour les appartements, merci d'indiquer aussi l'\xe9tage et la porte de l'appartement", null=True, verbose_name='adresse postale', blank=True)),
                ('postal_code', models.CharField(blank=True, max_length=5, null=True, verbose_name='code postal', validators=[django.core.validators.RegexValidator(regex='^\\d{5}$', message='Code postal non valide.')])),
                ('city', models.CharField(max_length=200, null=True, verbose_name='commune', blank=True)),
                ('building_reference', models.CharField(help_text='La r\xe9f\xe9rence de votre batiment si vous la connaissez', max_length=50, null=True, verbose_name='R\xe9f\xe9rence batiment', blank=True)),
                ('mac_address', models.CharField(validators=[django.core.validators.RegexValidator(regex='^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$', message='Adresse mac non valide.')], max_length=17, blank=True, help_text="Indiquez l'adresse mac de la box/routeur que vous souhaitez utiliser. Laissez vide si vous souhaitez un routeur en r\xe9emplois fournis par l'association.", null=True, verbose_name='adresse mac du routeur')),
                ('appointment_phone_number', models.CharField(help_text='Num\xe9ro de t\xe9l\xe9phone pour le jour du rendez-vous avec le ou la raccordeuse', max_length=20, verbose_name='telephone')),
                ('appointment_availibility', models.TextField(help_text='Une liste de demi journ\xe9e disponibles en semaine sur les 15 prochains jours pour la prise de rendez-vous', null=True, verbose_name='disponibilit\xe9s', blank=True)),
                ('ipv4_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v4], max_length=39, blank=True, help_text='Adresse IPv4', null=True, verbose_name='IPv4')),
                ('ipv6_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v6], max_length=39, blank=True, help_text='Adresse IPv6 utilis\xe9e', null=True, verbose_name='IPv6')),
            ],
            options={
                'verbose_name': 'Fibre optique',
                'verbose_name_plural': 'Fibres optiques',
            },
            bases=('configuration.configuration', models.Model),
        ),
        migrations.CreateModel(
            name='OpticFiberSubscriptionRequest',
            fields=[
                ('offersubscriptionrequest_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='offers.OfferSubscriptionRequest', on_delete=models.CASCADE)),
                ('address', models.TextField(help_text="Pour les appartements, merci d'indiquer aussi l'\xe9tage et la porte de l'appartement", null=True, verbose_name='adresse postale', blank=True)),
                ('postal_code', models.CharField(blank=True, max_length=5, null=True, verbose_name='code postal', validators=[django.core.validators.RegexValidator(regex='^\\d{5}$', message='Code postal non valide.')])),
                ('city', models.CharField(max_length=200, null=True, verbose_name='commune', blank=True)),
                ('building_reference', models.CharField(help_text='La r\xe9f\xe9rence de votre batiment si vous la connaissez', max_length=50, null=True, verbose_name='R\xe9f\xe9rence batiment', blank=True)),
                ('mac_address', models.CharField(validators=[django.core.validators.RegexValidator(regex='^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$', message='Adresse mac non valide.')], max_length=17, blank=True, help_text="Indiquez l'adresse mac de la box/routeur que vous souhaitez utiliser. Laissez vide si vous souhaitez un routeur en r\xe9emplois fournis par l'association.", null=True, verbose_name='adresse mac du routeur')),
                ('appointment_phone_number', models.CharField(help_text='Num\xe9ro de t\xe9l\xe9phone pour le jour du rendez-vous avec le ou la raccordeuse', max_length=20, verbose_name='telephone')),
                ('appointment_availibility', models.TextField(help_text='Une liste de demi journ\xe9e disponibles en semaine sur les 15 prochains jours pour la prise de rendez-vous', null=True, verbose_name='disponibilit\xe9s', blank=True)),
            ],
            options={
                'verbose_name': "demande d'acc\xe8s internet fibre optique",
                'verbose_name_plural': "demandes d'acc\xe8s internet fibre optique",
            },
            bases=('offers.offersubscriptionrequest', models.Model),
        ),
        migrations.RemoveField(
            model_name='simpledsl',
            name='configuration_ptr',
        ),
        migrations.DeleteModel(
            name='SimpleDSL',
        ),
    ]
