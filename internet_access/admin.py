# -*- coding: utf-8 -*-

from django.contrib import admin

from coin.utils import delete_selected

from coin.configuration.admin import ChildConfigurationAdmin
from coin.offers.admin import ChildOfferSubscriptionRequestAdmin
from internet_access.models import OpticFiberConfiguration, OpticFiberSubscriptionRequest


class OpticFiberInline(admin.StackedInline):
    model = OpticFiberConfiguration


class OpticFiberAdmin(ChildConfigurationAdmin):
    base_model = OpticFiberConfiguration
    list_display = ('__str__', 'offersubscription', 'get_state_icon_display',
                    'mac_address', 'comment', 'city')
    search_fields = ('comment',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    actions = (delete_selected,)
    # Used for form inclusion (when browsing a subscription object in the
    def get_fieldsets(self, request, obj=None):
        fieldsets = super(OpticFiberAdmin, self).get_fieldsets(request, obj)
        fieldsets.insert(1, ("Informations pour le provisionnement", {"fields": ("address", "postal_code", "city", "building_reference", "mac_address", "appointment_phone_number", "appointment_availibility")}))
        return fieldsets
    # admin, SimpleDSLInline will be displayed)
    inline = OpticFiberInline
    # Since we don't provide a view, don't display a "view on site" link
    # in the admin.
    view_on_site = False


class OpticFiberSubscriptionRequestAdmin(ChildOfferSubscriptionRequestAdmin):

    base_model = OpticFiberSubscriptionRequest

    def get_fieldsets(self, request, obj=None):
        fieldsets = super(OpticFiberSubscriptionRequestAdmin, self).get_fieldsets(request, obj)
        fieldsets.insert(1, ("Informations pour le provisionnement", {"fields": ("address", "postal_code", "city", "building_reference", "mac_address", "appointment_phone_number", "appointment_availibility")}))
        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(OpticFiberSubscriptionRequestAdmin, self).get_readonly_fields(request, obj)
        if obj and obj.state != "pending":
            readonly_fields += ["address", "postal_code", "city", "building_reference", "mac_address", "appointment_phone_number", "appointment_availibility"]
        return readonly_fields

admin.site.register(OpticFiberConfiguration, OpticFiberAdmin)
