# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import VPSView

app_name = "vps"

urlpatterns = [
    #'',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', VPSView.as_view(template_name="vps/vps.html"), name="details"),
]
