# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vps', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vpsconfiguration',
            name='console',
            field=models.OneToOneField(null=True, blank=True, to='vps.Console', verbose_name='console', on_delete=models.CASCADE),
        ),
    ]
