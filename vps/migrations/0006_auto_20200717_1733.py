# -*- coding: utf-8 -*-

from django.db import migrations, models
import netfields.fields
import coin.validation
import vps.models


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0011_auto_20200717_1733'),
        ('vps', '0005_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='VPSOperatingSystem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reference', models.CharField(max_length=64, verbose_name='Nom technique (utilis\xe9 par les scripts)')),
                ('display_name', models.CharField(max_length=128, verbose_name='Nom affich\xe9 aux \xeatres humains')),
            ],
            options={
                'verbose_name': "Syst\xe8me d'exploitation",
                'verbose_name_plural': "Syst\xe8mes d'exploitation",
            },
        ),
        migrations.CreateModel(
            name='VPSSubscriptionRequest',
            fields=[
                ('offersubscriptionrequest_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='offers.OfferSubscriptionRequest', on_delete=models.CASCADE)),
                ('ssh_key', models.TextField(blank=True, max_length=1024, null=True, verbose_name='Clef SSH', validators=[vps.models.PublicSSHKeyValidator])),
                ('operating_system', models.ForeignKey(blank=True, to='vps.VPSOperatingSystem', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'demande de VPS',
                'verbose_name_plural': 'demandes de VPS',
            },
            bases=('offers.offersubscriptionrequest', models.Model),
        ),
        migrations.RemoveField(
            model_name='vpsconfiguration',
            name='activated',
        ),
        migrations.AddField(
            model_name='vpsconfiguration',
            name='ssh_key',
            field=models.TextField(blank=True, max_length=1024, null=True, verbose_name='Clef SSH', validators=[vps.models.PublicSSHKeyValidator]),
        ),
        migrations.AlterField(
            model_name='vpsconfiguration',
            name='ipv4_endpoint',
            field=netfields.fields.InetAddressField(validators=[coin.validation.validate_v4], max_length=39, blank=True, help_text='Adresse IPv4 utilis\xe9e comme endpoint', null=True, verbose_name='IPv4'),
        ),
        migrations.AlterField(
            model_name='vpsconfiguration',
            name='ipv6_endpoint',
            field=netfields.fields.InetAddressField(validators=[coin.validation.validate_v6], max_length=39, blank=True, help_text='Adresse IPv6 utilis\xe9e comme endpoint', null=True, verbose_name='IPv6'),
        ),
        migrations.AddField(
            model_name='vpsconfiguration',
            name='operating_system',
            field=models.ForeignKey(blank=True, to='vps.VPSOperatingSystem', null=True, on_delete=models.CASCADE),
        ),
    ]
