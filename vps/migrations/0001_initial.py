# -*- coding: utf-8 -*-

from django.db import migrations, models
import netfields.fields
import coin.validation


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('configuration', '0004_auto_20161015_1837'),
    ]

    operations = [
        migrations.CreateModel(
            name='Console',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('protocol', models.CharField(max_length=256, verbose_name='protocole', choices=[('VNC', 'VNC')])),
                ('domain', models.CharField(max_length=256, null=True, verbose_name='nom de domaine', blank=True)),
                ('port', models.IntegerField(null=True, verbose_name='port')),
                ('password_link', models.URLField(help_text='Lien \xe0 usage unique (d\xe9truit apr\xe8s ouverture)', null=True, verbose_name='Mot de passe', blank=True)),
            ],
            options={
                'verbose_name': 'Console',
            },
        ),
        migrations.CreateModel(
            name='FingerPrint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('algo', models.CharField(max_length=256, verbose_name='algo', choices=[('ED25519', 'ED25519'), ('RSA', 'RSA'), ('ECDSA', 'ECDSA')])),
                ('fingerprint', models.CharField(max_length=256, verbose_name='empreinte')),
                ('length', models.IntegerField(null=True, verbose_name='longueur de la cl\xe9')),
                ('polymorphic_ctype', models.ForeignKey(related_name='polymorphic_vps.fingerprint_set+', editable=False, to='contenttypes.ContentType', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Empreinte',
            },
        ),
        migrations.CreateModel(
            name='VPSConfiguration',
            fields=[
                ('configuration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='configuration.Configuration', on_delete=models.CASCADE)),
                ('activated', models.BooleanField(default=False, verbose_name='activ\xe9')),
                ('ipv4_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v4], max_length=39, blank=True, help_text='Adresse IPv4 utilis\xe9e par d\xe9faut sur le VPS', null=True, verbose_name='IPv4')),
                ('ipv6_endpoint', netfields.fields.InetAddressField(validators=[coin.validation.validate_v6], max_length=39, blank=True, help_text='Adresse IPv6 utilis\xe9e par d\xe9faut sur le VPS', null=True, verbose_name='IPv6')),
                ('console', models.ForeignKey(verbose_name='console', blank=True, to='vps.Console', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'VPS',
            },
            bases=('configuration.configuration',),
        ),
        migrations.AddField(
            model_name='fingerprint',
            name='vps',
            field=models.ForeignKey(verbose_name='vps', to='vps.VPSConfiguration', on_delete=models.CASCADE),
        ),
    ]
