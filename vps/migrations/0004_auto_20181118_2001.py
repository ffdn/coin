# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vps', '0003_auto_20170803_0411'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vpsconfiguration',
            name='activated',
            field=models.BooleanField(default=True, verbose_name='activ\xe9'),
        ),
    ]
