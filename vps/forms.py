# -*- coding: utf-8 -*-

from django import forms

from coin.offers.forms import OfferSubscriptionRequestStep2Form
from .models import VPSOperatingSystem, VPSSubscriptionRequest, PublicSSHKeyValidator


class VPSSubscriptionRequestStep2Form(OfferSubscriptionRequestStep2Form):

    offer_type = "VPS"
    subscriptionrequest_class = VPSSubscriptionRequest

    operating_system = forms.ModelChoiceField(queryset=VPSOperatingSystem.objects.all(), label="Système d'exploitation", required=True)
    ssh_key = forms.CharField(label="Clef SSH", max_length=1024, widget=forms.Textarea(attrs={'style': 'height: 10em;'}), required=True, validators=[PublicSSHKeyValidator])

    def create_offersubscriptionrequest(self, request):
        subscriptionrequest = super().create_offersubscriptionrequest(request)
        subscriptionrequest.operating_system = self.cleaned_data["operating_system"]
        subscriptionrequest.ssh_key = self.cleaned_data["ssh_key"]
        subscriptionrequest.save()
        return subscriptionrequest
