# -*- coding: utf-8 -*-

from django.db import models
from polymorphic.models import PolymorphicModel
from django.core.exceptions import ValidationError
from django.urls import reverse
from netfields import InetAddressField, NetManager

from coin.configuration.models import IPConfiguration
from coin.offers.models import OfferSubscriptionRequest


FINGERPRINT_TYPES = (
    ('ED25519', 'ED25519'),
    ('RSA', 'RSA'),
    ('ECDSA', 'ECDSA')
)

PROTOCOLE_TYPES = (
    ('VNC', 'VNC'),
)

import struct
import base64
from django.core.exceptions import ValidationError
def PublicSSHKeyValidator(ssh_pubkey):
    if not isinstance(ssh_pubkey, str):
        raise ValidationError("Une clef SSH devrait être une chaine de caractères?")
    try:
        # From https://stackoverflow.com/questions/2494450/ssh-rsa-public-key-validation-using-a-regular-expression
        type_, key_string, comment = ssh_pubkey.strip().split(None, 2)
        data = base64.decodestring(key_string)
        int_len = 4
        str_len = struct.unpack('>I', data[:int_len])[0] # this should return 7
        data[int_len:int_len+str_len] == type_
    except:
        raise ValidationError("Il semblerait qu'il y ai une erreur de syntaxe dans cette clef SSH")


class VPSOperatingSystem(models.Model):

    reference = models.CharField(max_length=64,
                                 blank=False, null=False,
                                 verbose_name="Nom technique (utilisé par les scripts)")

    display_name = models.CharField(max_length=128, verbose_name="Nom affiché aux êtres humains",
                                    blank=False, null=False)

    def __str__(self):
        return self.display_name

    class Meta:
        verbose_name = 'Système d\'exploitation'
        verbose_name_plural = 'Systèmes d\'exploitation'
        app_label = "vps"


class VPSProvisioningParameters(models.Model):

    operating_system = models.ForeignKey(VPSOperatingSystem, blank=True, null=True, on_delete=models.CASCADE)

    ssh_key = models.TextField(max_length=1024, verbose_name="Clef SSH",
                               blank=True, null=True,
                               validators=[PublicSSHKeyValidator])

    class Meta:
        abstract = True
        app_label = "vps"

class VPSConfiguration(IPConfiguration, VPSProvisioningParameters):

    url_namespace = "vps"

    # Subclasses can fill this list with stuff like "operating_system" or "ssh_key" or "password"
    provisioning_infos = ["operating_system", "ssh_key"]

    def get_absolute_url(self):
        return reverse('vps:details', args=[str(self.pk)])

    def __str__(self):
        return 'VPS ' + str(self.offersubscription.member.username) + ' ' + self.offersubscription.member.last_name

    class Meta:
        verbose_name = 'VPS'
        app_label = "vps"

class FingerPrint(PolymorphicModel):
    vps = models.ForeignKey(VPSConfiguration, verbose_name="vps", on_delete=models.CASCADE)
    algo = models.CharField(max_length=256, verbose_name="algo",
                            choices=FINGERPRINT_TYPES)
    fingerprint = models.CharField(max_length=256, verbose_name="empreinte")
    length = models.IntegerField(verbose_name="longueur de la clé", null=True)

    class Meta:
        verbose_name = 'Empreinte'
        app_label = "vps"


class Console(models.Model):
    vps = models.OneToOneField(VPSConfiguration, verbose_name="vps", on_delete=models.CASCADE)
    protocol = models.CharField(max_length=256, verbose_name="protocole",
                            choices=PROTOCOLE_TYPES)
    domain = models.CharField(max_length=256, verbose_name="nom de domaine",
                                blank=True, null=True)
    port = models.IntegerField(verbose_name="port", null=True)
    password_link = models.URLField(verbose_name="Mot de passe", blank=True,
                           null=True, help_text="Lien à usage unique (détruit après ouverture)")
    class Meta:
        verbose_name = 'Console'
        app_label = "vps"


class VPSSubscriptionRequest(OfferSubscriptionRequest, VPSProvisioningParameters):

    configuration_class = VPSConfiguration

    class Meta:
        verbose_name = 'demande de VPS'
        verbose_name_plural = 'demandes de VPS'
        app_label = "vps"
