# -*- coding: utf-8 -*-

from django.contrib import admin

from coin.configuration.admin import ChildConfigurationAdmin, ChildConfigurationAdminInline
from coin.offers.admin import ChildOfferSubscriptionRequestAdmin
from coin.utils import delete_selected

from .models import VPSConfiguration, FingerPrint, Console, VPSSubscriptionRequest, VPSOperatingSystem


class ConsoleInline(admin.TabularInline):
    model = Console
    extra = 0


class FingerPrintInline(admin.TabularInline):
    model = FingerPrint
    extra = 0


class VPSConfigurationInline(ChildConfigurationAdminInline):
    model = VPSConfiguration


class VPSConfigurationAdmin(ChildConfigurationAdmin):
    base_model = VPSConfiguration
    list_display = ('offersubscription', 'get_state_icon_display',
                    'ipv4_endpoint', 'comment')
    search_fields = ('comment',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    actions = (delete_selected,)
    inline = VPSConfigurationInline

VPSConfigurationAdmin.inlines = tuple(VPSConfigurationAdmin.inlines) + (FingerPrintInline, ConsoleInline)


class VPSOperatingSystemAdmin(admin.ModelAdmin):

    base_model = VPSOperatingSystem
    list_display = ('display_name',)


class VPSSubscriptionRequestAdmin(ChildOfferSubscriptionRequestAdmin):

    base_model = VPSSubscriptionRequest

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj)
        fieldsets.insert(1, ("Informations pour le provisionnement", {"fields": ("operating_system", "ssh_key")}))
        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj and obj.state != "pending":
            readonly_fields += ["operating_system", "ssh_key"]
        return readonly_fields


admin.site.register(VPSConfiguration, VPSConfigurationAdmin)
admin.site.register(VPSOperatingSystem, VPSOperatingSystemAdmin)
admin.site.register(VPSSubscriptionRequest, VPSSubscriptionRequestAdmin)
