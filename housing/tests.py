# -*- coding: utf-8 -*-

import unittest
from unittest import skipUnless

from django.test import TestCase
from django.conf import settings

from coin.offers.models import Offer, OfferSubscription
from coin.resources.models import IPPool, IPSubnet
from coin.members.models import Member
from coin.members.tests import MemberTestsUtils

from .models import HousingConfiguration

USING_POSTGRES = (settings.DATABASES['default']['ENGINE']
                  ==
                  'django.db.backends.postgresql_psycopg2')


""" BIG FAT WARNING

Ce code requiert une sévère factorisation avec vpn/tests.py et vps/tests.py

"""


@unittest.skip("To my knowledge, vps module tests have always been failing")
class HousingTestCase(TestCase):
    fixtures = ['example_pools.json', 'offers.json']

    def setUp(self):
        self.v6_pool = IPPool.objects.get(default_subnetsize=56)
        self.v4_pool = IPPool.objects.get(default_subnetsize=32)
        self.offer = Offer.objects.filter(configuration_type="HousingConfiguration")[0]

        # Create a member.
        cn = MemberTestsUtils.get_random_username()
        self.member = Member.objects.create(first_name="Toto",
                                            last_name="L'artichaut",
                                            username=cn)

        # Create a new Housing with subnets.
        # We need Django to call clean() so that magic happens.
        abo = OfferSubscription(offer=self.offer, member=self.member)
        abo.full_clean()
        abo.save()
        housing = HousingConfiguration(offersubscription=abo)
        housing.full_clean()
        housing.save()
        v6 = IPSubnet(ip_pool=self.v6_pool, configuration=housing)
        v6.full_clean()
        v6.save()
        v4 = IPSubnet(ip_pool=self.v4_pool, configuration=housing)
        v4.full_clean()
        v4.save()

        # Create additional Housing, they should automatically be attributed a
        # new login.
        for i in range(5):
            abo = OfferSubscription(offer=self.offer, member=self.member)
            abo.full_clean()
            abo.save()
            housing = HousingConfiguration(offersubscription=abo)
            housing.full_clean()
            housing.save()

    def tearDown(self):
        """Properly clean up objects, so that they don't stay in LDAP"""
        for housing in HousingConfiguration.objects.all():
            housing.delete()
        Member.objects.get().delete()

    @skipUnless(USING_POSTGRES, "Using a postgresql-only field")
    def test_has_ipv4_endpoint(self):
        housing = HousingConfiguration.objects.all()[0]
        self.assertIsNotNone(housing.ipv4_endpoint)

    @skipUnless(USING_POSTGRES, "Using a postgresql-only field")
    def test_has_correct_ipv4_endpoint(self):
        """If there is not endpoint, we consider it to be correct."""
        housing = HousingConfiguration.objects.all()[0]
        if housing.ipv4_endpoint is not None:
            subnet = housing.ip_subnet.get(ip_pool=self.v4_pool)
            self.assertIn(housing.ipv4_endpoint, subnet.inet)

    @skipUnless(USING_POSTGRES, "Using a postgresql-only field")
    def test_has_ipv6_endpoint(self):
        housing = HousingConfiguration.objects.all()[0]
        self.assertIsNotNone(housing.ipv6_endpoint)

    @skipUnless(USING_POSTGRES, "Using a postgresql-only field")
    def test_has_correct_ipv6_endpoint(self):
        """If there is not endpoint, we consider it to be correct."""
        housing = HousingConfiguration.objects.all()[0]
        if housing.ipv6_endpoint is not None:
            subnet = housing.ip_subnet.get(ip_pool=self.v6_pool)
            self.assertIn(housing.ipv6_endpoint, subnet.inet)

    @skipUnless(USING_POSTGRES, "Using a postgresql-only field")
    def test_change_v4subnet_is_housing_endpoint_correct(self):
        housing = HousingConfiguration.objects.all()[0]
        subnet = housing.ip_subnet.get(ip_pool=self.v4_pool)
        subnet.inet = "192.168.42.42/31"
        subnet.full_clean()
        subnet.save()
        self.test_has_correct_ipv4_endpoint()

    @skipUnless(USING_POSTGRES, "Using a postgresql-only field")
    def test_change_v6subnet_is_housing_endpoint_correct(self):
        housing = HousingConfiguration.objects.all()[0]
        subnet = housing.ip_subnet.get(ip_pool=self.v6_pool)
        subnet.inet = "2001:db8:4242:4200::/56"
        subnet.full_clean()
        subnet.save()
        self.test_has_correct_ipv6_endpoint()

    def test_automatic_login(self):
        housing = HousingConfiguration.objects.all()[0]
        expected_login = housing.offersubscription.member.username + "-housing1"
        self.assertEqual(housing.login, expected_login)

    def test_has_multiple_housing(self):
        housings = HousingConfiguration.objects.all()
        self.assertEqual(len(housings), 6)
