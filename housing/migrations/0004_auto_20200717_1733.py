# -*- coding: utf-8 -*-

from django.db import migrations, models
import netfields.fields
import coin.validation


class Migration(migrations.Migration):

    dependencies = [
        ('housing', '0003_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='housingconfiguration',
            name='activated',
        ),
        migrations.AlterField(
            model_name='housingconfiguration',
            name='ipv4_endpoint',
            field=netfields.fields.InetAddressField(validators=[coin.validation.validate_v4], max_length=39, blank=True, help_text='Adresse IPv4 utilis\xe9e comme endpoint', null=True, verbose_name='IPv4'),
        ),
        migrations.AlterField(
            model_name='housingconfiguration',
            name='ipv6_endpoint',
            field=netfields.fields.InetAddressField(validators=[coin.validation.validate_v6], max_length=39, blank=True, help_text='Adresse IPv6 utilis\xe9e comme endpoint', null=True, verbose_name='IPv6'),
        ),
        migrations.AlterField(
            model_name='housingconfiguration',
            name='vlan',
            field=models.IntegerField(null=True, verbose_name='vlan id'),
        ),
    ]
