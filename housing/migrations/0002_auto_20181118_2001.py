# -*- coding: utf-8 -*-

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('housing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='housingconfiguration',
            name='activated',
            field=models.BooleanField(default=True, verbose_name='activ\xe9'),
        ),
        migrations.AlterField(
            model_name='housingconfiguration',
            name='vlan',
            field=models.IntegerField(null=True, verbose_name='vlan id', blank=True),
        ),
    ]
