# -*- coding: utf-8 -*-

from django.contrib import admin

from coin.configuration.admin import ChildConfigurationAdmin, ChildConfigurationAdminInline
from coin.utils import delete_selected

from .models import HousingConfiguration



class HousingConfigurationInline(ChildConfigurationAdminInline):
    model = HousingConfiguration
    readonly_fields = ['configuration_ptr']
    specific_fields = ["vlan"]

class HousingConfigurationAdmin(ChildConfigurationAdmin):
    base_model = HousingConfiguration
    list_display = ('offersubscription',
                    'ipv4_endpoint', 'vlan', 'comment')
    search_fields = ('comment', 'vlan', 'ipv4_endpoint',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    specific_fields = ["vlan"]
    actions = (delete_selected,)
    inline = HousingConfigurationInline

admin.site.register(HousingConfiguration, HousingConfigurationAdmin)
