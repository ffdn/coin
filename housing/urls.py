# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import HousingView

app_name = "housing"

urlpatterns = [
    #'',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', HousingView.as_view(template_name="housing/housing.html"), name="details"),
]
