# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404
from django.views.generic.edit import UpdateView
from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from coin.members.models import Member

from .models import HousingConfiguration


class HousingView(SuccessMessageMixin, UpdateView):
    model = HousingConfiguration
    fields = ['ipv4_endpoint', 'ipv6_endpoint', 'comment']
    success_message = "Configuration enregistrée avec succès !"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        return None

    def get_object(self):
        if self.request.user.is_superuser:
            return get_object_or_404(HousingConfiguration, pk=self.kwargs.get("pk"))
        # For normal users, ensure the Housing belongs to them.
        return get_object_or_404(HousingConfiguration, pk=self.kwargs.get("pk"),
                                 offersubscription__member=self.request.user)

