# -*- coding: utf-8 -*-

from django.apps import AppConfig
import coin.apps

#from . import urls


class HousingConfig(AppConfig, coin.apps.AppURLs):
    name = 'housing'
    verbose_name = "Gestion d'accès Housing"

    exported_urlpatterns = [('housing', 'housing.urls')]

    admin_menu_addons = {
        'configs': [
            ("Housing", "housing/housingconfiguration"),
        ]
    }
