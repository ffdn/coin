# -*- coding: utf-8 -*-

from django.db import models
from polymorphic.models import PolymorphicModel
from django.core.exceptions import ValidationError
from django.urls import reverse
from netfields import InetAddressField, NetManager

from coin.configuration.models import IPConfiguration
# from coin.offers.backends import ValidateBackendType
from coin import validation


class HousingConfiguration(IPConfiguration):
    url_namespace = "housing"
    vlan = models.IntegerField(verbose_name="vlan id", default=0,null=True)

    def get_absolute_url(self):
        return reverse('housing:details', args=[str(self.pk)])

    def __str__(self):
        return 'Housing ' + str(self.offersubscription.member.username) + ' ' + self.offersubscription.member.last_name

    class Meta:
        verbose_name = 'Housing'
        app_label = "housing"

