# -*- coding: utf-8 -*-
import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django import forms
from django.forms import formset_factory
from django.shortcuts import render, redirect

from .models import MaillingList, MaillingListSubscription, SyncCommandError
from coin.members.models import Member

logger = logging.getLogger(__name__)

class SubscriptionForm(forms.Form):
    subscribed = forms.BooleanField(widget=forms.Select(
        choices=(
            (True, 'Abonné·e'),
            (False, 'Non abonné·e'),
        ),
        attrs={'class': 'form-control'},
    ), required=False)
    maillinglist = forms.ModelChoiceField(
        queryset=MaillingList.objects.all(),
        widget=forms.HiddenInput,
    )


SubscriptionFormSet = formset_factory(SubscriptionForm, extra=0)


class MemberSubscriptionsForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = []


@login_required
def lists_list(request):
    if request.method == 'POST':
        formset = SubscriptionFormSet(request.POST, request.FILES)

        if formset.is_valid():
            # we do proper add/remove instead of clearing and setting to take
            # care of acurate signal sending (cf signal receivers in models.py).
            new_subscriptions = set(
                i['maillinglist']
                for i in formset.cleaned_data
                if i['subscribed']
            )
            old_subscriptions = set(request.user.subscribed_maillinglists.all())
            try:
                # add
                for mail_list in new_subscriptions - old_subscriptions:
                    MaillingListSubscription.objects.create(member=request.user, maillinglist=mail_list)
                # remove
                to_remove = old_subscriptions - new_subscriptions
                MaillingListSubscription.objects.filter(member=request.user, maillinglist__in=to_remove).delete()
            except SyncCommandError as e:
                logger.error(e)
                messages.error(
                    request,
                    "Impossible de sauvegarder tes abonnements. "
                    "Contacte les administrateur·ice·s système pour "
                    "qu'iels voient ce qui se passe… \n"
                    "Tes abonnements n'ont **pas** été mis à jour.")
            else:

                if old_subscriptions != new_subscriptions:
                    messages.success(
                        request,
                        'Tes (dés)abonnements aux listes mail ont été '
                        'pris en compte.')
                else:
                    messages.warning(
                        request,
                        "Vous n'avez modifié aucun abonnement.")

            return redirect('maillists:lists-list')

    else:
        user_subscriptions = request.user.subscribed_maillinglists.all()
        formset = SubscriptionFormSet(initial=[
            {'subscribed': l in user_subscriptions, 'maillinglist': l}
            for l in MaillingList.objects.all()
        ])

    return render(request, 'maillists/maillinglist_list.html', {
        'formset': formset,
        'all_lists': MaillingList.objects.all(),
        'my_lists': request.user.subscribed_maillinglists.all(),
    })
