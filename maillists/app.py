# -*- coding: utf-8 -*-

from django.apps import AppConfig
import coin.apps


class MailListsConfig(AppConfig, coin.apps.AppURLs):
    name = 'maillists'
    verbose_name = "Listes mail"
    exported_urlpatterns = [('maillists', 'maillists.urls')]

    admin_menu_entry = {
        "id": "maillists",
        "icon": "envelope",
        "title": "Listes mail",
        "models": [
            ("Listes de discussion mail", "maillists/maillinglist"),
        ]
    }
