# -*- coding: utf-8 -*-

import sys

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from coin.members.models import Member
from maillists.models import (
    MaillingList,
    MaillingListSubscription,
    skip_maillist_sync,
)

"""Import a text file of email addresses into mailling list subscription"

Create a new mailling-list subscribing the provided addresses. The script will
try to map email addresses to members, and stop if some addresses do not belong
to any member.

This command takes care to avoid triggering a sync per single subscription.
"""


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser):
        parser.add_argument(
            'subscribers_file',
            help="The text file with the subscribed email addresses, one per line",
        )
        parser.add_argument(
            '--email',
            help='Mail address of the list',
            required=True,
        )
        parser.add_argument(
            '--verbose-name',
            help='The full human-targeted name of the list',
            required=True,
        )

        parser.add_argument(
            '--force',
            help='Import email adresses skipping those who do not belong to any member',
            action='store_true',
            default=False
        )

        parser.add_argument(
            '--dry-run',
            help='Do not write anything to database, just parse the file and show unknown addresses',
            action='store_true',
            default=False
        )

    @staticmethod
    def _iter_emails(filename):
        with open(filename) as f:
            for l in f.readlines():
                email = l.strip()
                if len(email) > 0:
                    yield l.strip()

    @staticmethod
    def _get_unknown_email(emails):
        for email in emails:
            try:
                Member.objects.get(email=email)
            except Member.DoesNotExist:
                yield email

    @transaction.atomic
    def handle(self, subscribers_file, email, verbose_name, force, dry_run, *args, **kwargs):
        ml = MaillingList.objects.create(
            short_name=email.split('@')[0],
            email=email,
            description='À RENSEIGNER',
            verbose_name=verbose_name,
        )
        unknown_emails = []
        with skip_maillist_sync():
            for email in self._iter_emails(subscribers_file):
                try:
                    member = Member.objects.get(email=email)
                except Member.DoesNotExist:
                    unknown_emails.append(email)
                else:
                    mls_exists = MaillingListSubscription.objects.filter(
                        member=member,
                        maillinglist=ml,
                    ).exists()

                    # Not using get_or_create because we want to set skip_sync
                    # before saving
                    if not mls_exists:
                        mls = MaillingListSubscription(
                            member=member,
                            maillinglist=ml,
                        )
                        mls.skip_sync = True
                        mls.save()


        # Do it once… (db will be rollback if it fails)
        sys.stdout.write('Pousse la liste sur le serveur… ',)
        ml.sync_to_list_server()
        print('OK')

        if (len(unknown_emails) > 0) and not force:
            print('ERREUR : Ces adresses ne correspondent à aucun membre')
            for email in unknown_emails:
                print(email)

            raise CommandError(
                "Rien n'a été créé en base, utiliser --force au besoin.")

        elif force or len(unknown_emails) == 0:
            if dry_run:
                # exception triggers rollback
                raise CommandError(
                    "--dry-run est utilisée, rien n'a été écrit en base")
