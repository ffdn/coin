# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

app_name = "maillists"

urlpatterns = [
    url(r'^$', views.lists_list, name='lists-list'),
]
